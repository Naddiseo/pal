//
//  Modified by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.xtext;

import org.eclipse.emf.common.util.URI;
import org.eclipse.xtext.parser.IEncodingProvider;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;
import org.eclipse.xtext.service.DispatchingProvider;

import com.google.inject.Binder;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class PALRuntimeModule extends pal.xtext.AbstractPALRuntimeModule {
	public Class<? extends AbstractAntlrParser> bindAbstractAntlrParser() {
		return pal.xtext.parser.antlr.PALParser.class;
	}
	
	public void configureRuntimeEncodingProvider(Binder binder) {
		binder.bind(IEncodingProvider.class)
		.annotatedWith(DispatchingProvider.Runtime.class)
		.to(UTF8EncodingProvider.class);
	}
	
	private static class UTF8EncodingProvider implements IEncodingProvider
	{
		@Override
		public String getEncoding(URI uri) {
			return "UTF-8";
		}
	}
}
