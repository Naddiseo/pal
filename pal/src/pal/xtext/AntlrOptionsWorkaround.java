//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.xtext;

import org.eclipse.xtext.generator.parser.antlr.AntlrOptions;

public class AntlrOptionsWorkaround extends AntlrOptions {
	public void setKAsString(String k) {
		this.setK(Integer.parseInt(k));
	}
}
