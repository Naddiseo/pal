//
//  Modified by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.xtext;

import pal.xtext.PALStandaloneSetupGenerated;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class PALStandaloneSetup extends PALStandaloneSetupGenerated
{
	public static void doSetup() {
		new PALStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

