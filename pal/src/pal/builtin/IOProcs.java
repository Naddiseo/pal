package pal.builtin;

import pal.codegen.Attr;
import pal.codegen.DType;
import pal.codegen.TNative;

public enum IOProcs
{
	/* This semicolon is, sadly enough, required */;
	
	private static abstract class ReadBase extends BFunc.Variadic
	{
		protected ReadBase(String name) {
			super(name, true);
		}
		
		@Override
		protected boolean typecheck() {
			boolean typechecks = true;
			
			int i = this.variadicArgIndex - 1;
			for (Attr arg: this.compiledArgs) {
				i += 1;
				
				if (arg.isBottom()) continue;
				if (arg.isNumber()) continue;
				if (arg.type == TNative.Char) continue;
				if (arg.type.isString()) continue;
				
				this.errors.add(this.args.get(i), "argument #%d to '%s' must be integer, real, char or string; given '%s'", i + 1, this.name, arg.type.name);
				typechecks = false;
			}
			
			return typechecks;
		}
		
		@Override
		protected Attr compile() {
			for (Attr arg: this.compiledArgs) {
				if (arg.isBottom()) continue;
				
				DType type = arg.type;
				
				if (type.isInt()) {
					builder.op("readi");
					builder.iferr("runtime_error_no_number");
					builder.popi();
				} else if (type.isReal()) {
					builder.op("readr");
					builder.iferr("runtime_error_no_number");
					builder.popi();
				} else if (type.isChar()) {
					builder.op("readc");
					builder.iferr("runtime_error_read_at_eof");
					builder.popi();
				} else if (type.isString()) {
					builder.konst(type.size());
					builder.call(0, "std_readstr");
					builder.adjust(-2);
				}
			}
			
			return Attr.Void;
		}
	}
	
	private static abstract class WriteBase extends BFunc.Variadic
	{
		protected WriteBase(String name) {
			super(name, false);
		}

		@Override
		protected boolean typecheck() {
			boolean typechecks = true;
			
			int i = this.variadicArgIndex - 1;
			for (Attr arg: this.compiledArgs) {
				i += 1;
				
				if (arg.isBottom()) continue;
				if (arg.isNumber()) continue;
				if (arg.type == TNative.Char) continue;
				if (arg.type.isString()) continue;
				
				this.errors.add(this.args.get(i), "argument #%d to '%s' must be integer, real, char or string; given '%s'", i + 1, this.name, arg.type.name);
				typechecks = false;
			}
			
			return typechecks;
		}
		
		@Override
		protected Attr compile() {
			for (Attr arg: this.compiledArgs) {
				if (arg.isBottom()) continue;
				
				DType type = arg.type;
				
				if (type.isInt()) {
					builder.op("writei");
				} else if (type.isReal()) {
					builder.op("writer");
				} else if (type.isChar()) {
					builder.op("writec");
				} else if (type.isString()) {
					int size = type.size();
					
					builder.konst(size);
					builder.call(0, "std_writestr");
					builder.adjust(-(size + 1));
				}
			}
			
			return Attr.Void;
		}
	}
	
	public final static DBuiltIn Read = new ReadBase("read") {};
	
	public final static DBuiltIn ReadLn = new ReadBase("readln") {
		@Override
		protected void variadicEnd() {			
			builder.call(0, "std_read2eol");
		}
	};
	
	public final static DBuiltIn Write = new WriteBase("write") {};
	
	public final static DBuiltIn WriteLn = new WriteBase("writeln") {
		@Override
		protected void variadicEnd() {
			builder.konst(10);
			builder.op("writec");
		}
	};
}
