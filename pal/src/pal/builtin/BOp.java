package pal.builtin;

import pal.codegen.Attr;
import pal.codegen.DType;
import pal.codegen.TNative;

abstract class BOp extends DBuiltIn
{
	private final int numArgs;
	protected final DType returnType;
	
	protected BOp(String name, int numArgs, DType returnType) {
		super(name, true);
		this.numArgs = numArgs;
		this.returnType = returnType;
	}
	
	protected int expectedNumArgs() {
		return this.numArgs;
	}
	
	@Override
	protected Attr errorAttr() {
		return new Attr(this.returnType, false);
	}
	
	public static abstract class Binary extends BOp
	{
		protected Binary(String name, DType returnType) {
			super(name, 2, returnType);
		}
		
		public static abstract class Logic extends Binary
		{
			protected Logic(String name) {
				super(name, TNative.Bool);
			}
			
			@Override
			protected boolean typecheck(Attr a, Attr b) {
				boolean ta = a.type.isBool();
				boolean tb = b.type.isBool();
				
				if (!ta) {
					errors.add(this.args.get(0), "argument #1 to '%s' must be boolean", this.name);
				}
				
				if (!tb) {
					errors.add(this.args.get(0), "argument #2 to '%s' must be boolean", this.name);
				}
				
				return ta && tb;
			}
		}

		@Override
		protected boolean typecheck() {
			return this.typecheck(this.compiledArgs.get(0), this.compiledArgs.get(1));
		}

		@Override
		protected Attr fold() {
			return this.fold(this.compiledArgs.get(0), this.compiledArgs.get(1));
		}

		@Override
		protected Attr compile() {
			return this.compile(this.compiledArgs.get(0), this.compiledArgs.get(1));
		}
		
		protected abstract boolean typecheck(Attr a, Attr b);
		protected abstract Attr fold(Attr a, Attr b);
		protected abstract Attr compile(Attr a, Attr b);
	}
}
