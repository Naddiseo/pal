package pal.builtin;

import pal.codegen.Attr;
import pal.codegen.DType;
import pal.codegen.TEnum;
import pal.codegen.TNative;

public enum DiscreteFuncs
{
	/* This semicolon is, sadly enough, required */;
	
	public final static DBuiltIn Odd = new BFunc.Unary("odd") {
		@Override
		protected Attr foldArg(Attr arg) {
			return new Attr(arg.vint() % 2 != 0);
		}

		@Override
		protected boolean typecheckArg(DType type) {
			if (type != TNative.Int) {
				this.errors.add(this.args.get(0), "argument #1 to 'odd' is not an integer");
				return false;
			}
			return true;
		}
		
		@Override
		protected Attr compileArg(Attr arg) {
			this.builder.konst(2);
			this.builder.op("mod");
			return Attr.Bool;
		}
		
		@Override
		protected Attr errorAttr() {
			return Attr.Bool;
		}
	};
	
	public final static DBuiltIn Ord = new BFunc.Unary("ord") {
		@Override
		protected boolean typecheckArg(DType arg) {
			if (!(arg instanceof TEnum)) {
				this.errors.add(this.args.get(0), "argument #1 of 'ord' must be of ordinal type");
				return false;
			}
			
			return true;
		}
		
		@Override
		protected Attr errorAttr() {
			return Attr.Int;
		}
		
		@Override
		protected Attr foldArg(Attr arg) {
			return new Attr(arg.vint());
		}

		@Override
		protected Attr compileArg(Attr arg) {
			// No-op
			return Attr.Int;
		}
	};
	
	public final static DBuiltIn Chr = new BFunc.Unary("chr") {
		@Override
		protected boolean typecheckArg(DType arg) {
			if (arg != TNative.Int) {
				this.errors.add(this.args.get(0), "argument #1 of 'chr' must be integer");
				return false;
			}
			
			return true;
		}
		
		@Override
		protected Attr errorAttr() {
			return Attr.Char;
		}
		
		@Override
		protected Attr foldArg(Attr arg) {
			int i = arg.vint();
			
			if (i < 0 || i > 255) {
				this.errors.add(this.args.get(0), "argument #1 of 'chr' must be in 0 .. 255 range");
				return Attr.Char;
			} else {
				return new Attr((char)i);
			}
		}

		@Override
		protected Attr compileArg(Attr arg) {
			// No-op
			return Attr.Char;
		}
	};
	
	public final static DBuiltIn Succ = new BFunc.Unary("succ") {
		@Override
		protected boolean typecheckArg(DType arg) {
			if (!(arg instanceof TEnum)) {
				errors.add(this.args.get(0), "argument #1 to 'succ' must be of ordinal type");
				return false;
			}
			
			return true;
		}

		@Override
		protected Attr foldArg(Attr arg) {
			TEnum t = (TEnum)arg.type;
			int i = arg.vint();
			
			if (i == t.max) {
				errors.add(this.args.get(0), "cannot take 'succ' of maximum value");
				return Attr.Bottom;
			} else {
				return new Attr(t, i + 1);
			}
		}

		@Override
		protected Attr compileArg(Attr arg) {
			TEnum type = (TEnum)arg.type;
			
			builder.dup();
			builder.konst(type.max);
			builder.lt(TNative.Int);
			builder.ifz("runtime_error_succ_overflow");
			builder.konst(1);
			builder.op("addi");
			
			return new Attr(type, false);
		}
	};
	
	public final static DBuiltIn Pred = new BFunc.Unary("pred") {
		@Override
		protected boolean typecheckArg(DType arg) {
			if (!(arg instanceof TEnum)) {
				errors.add(this.args.get(0), "argument #1 to 'pred' must be of ordinal type");
				return false;
			}
			
			return true;
		}

		@Override
		protected Attr foldArg(Attr arg) {
			TEnum t = (TEnum)arg.type;
			int i = arg.vint();
			
			if (i == t.min) {
				errors.add(this.args.get(0), "cannot take 'pred' of minimum value");
				return Attr.Bottom;
			} else {
				return new Attr(t, i - 1);
			}
		}

		@Override
		protected Attr compileArg(Attr arg) {
			TEnum type = (TEnum)arg.type;
			
			builder.dup();
			builder.konst(type.min);
			builder.gt(TNative.Int);
			builder.ifz("runtime_error_pred_underflow");
			builder.konst(1);
			builder.op("subi");
			
			return new Attr(type, false);
		}
	};
}
