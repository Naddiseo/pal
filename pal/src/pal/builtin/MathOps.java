package pal.builtin;

import pal.codegen.Attr;
import pal.codegen.TNative;

// Ops that take int, real, or both
public enum MathOps
{
	/* Mandatory enum semicolon */;
	
	private static abstract class CastingMathOp extends BOp.Binary
	{
		protected CastingMathOp(String name) {
			super(name, TNative.Int);
		}

		@Override
		protected boolean typecheck(Attr a, Attr b) {
			boolean ta = a.isNumber();
			boolean tb = b.isNumber();
			
			if (!ta) {
				errors.add(this.args.get(0), "argument #1 to operator '%s' must be a number", this.name);
			}
			
			if (!tb) {
				errors.add(this.args.get(1), "argument #2 to operator '%s' must be a number", this.name);
			}
			
			return ta && tb;
		}

		@Override
		protected Attr fold(Attr a, Attr b) {
			if (a.type == TNative.Int && b.type == TNative.Int) {
				return new Attr(this.foldi(a.vint(), b.vint()));
			} else {
				return new Attr(this.foldr(a.vreal(), b.vreal()));
			}
		}

		@Override
		protected Attr compile(Attr a, Attr b) {
			if (a.type == TNative.Int && b.type == TNative.Int) {
				this.compilei(a, b);
				return Attr.Int;
			} else {
				if (a.type == TNative.Int) {
					builder.adjust(-1);
					builder.op("itor");
					builder.adjust(1);
					
					a = new Attr(TNative.Real, false);
				}
				
				if (b.type == TNative.Int) {
					builder.op("itor");
					
					b = new Attr(TNative.Real, false);
				}
				
				this.compiler(a, b);
				return Attr.Real;
			}
		}
		
		protected abstract int foldi(int a, int b);
		protected abstract float foldr(float a, float b);
		protected abstract void compilei(Attr a, Attr b);
		protected abstract void compiler(Attr a, Attr b);
	}
	
	public final static DBuiltIn Add = new CastingMathOp("+")
	{
		@Override
		protected int foldi(int a, int b) {
			return a + b;
		}

		@Override
		protected float foldr(float a, float b) {
			return a + b;
		}

		@Override
		protected void compilei(Attr a, Attr b) {
			builder.op("addi");
			builder.iferr("rt_err_add_overflow");
		}

		@Override
		protected void compiler(Attr a, Attr b) {
			builder.op("addr");
		}
	};
	
	public final static DBuiltIn Sub = new CastingMathOp("-")
	{
		@Override
		protected int foldi(int a, int b) {
			return a - b;
		}

		@Override
		protected float foldr(float a, float b) {
			return a - b;
		}

		@Override
		protected void compilei(Attr a, Attr b) {
			builder.op("subi");
			builder.iferr("rt_err_sub_overflow");
		}

		@Override
		protected void compiler(Attr a, Attr b) {
			builder.op("subr");
		}
	};
	
	public final static DBuiltIn Mult = new CastingMathOp("*")
	{
		@Override
		protected int foldi(int a, int b) {
			return a * b;
		}

		@Override
		protected float foldr(float a, float b) {
			return a * b;
		}

		@Override
		protected void compilei(Attr a, Attr b) {
			builder.op("muli");
			builder.iferr("rt_err_mul_overflow");
		}

		@Override
		protected void compiler(Attr a, Attr b) {
			builder.op("mulr");
		}
	};
	
	public final static DBuiltIn DivI = new BOp.Binary("div", TNative.Int)
	{
		@Override
		protected boolean typecheck(Attr a, Attr b) {
			boolean ta = a.type.isInt();
			boolean tb = b.type.isInt();
			
			if (!ta) {
				errors.add(this.args.get(0), "argument #1 to operator '%s' must be an integer", this.name);
			}
			
			if (!tb) {
				errors.add(this.args.get(1), "argument #2 to operator '%s' must be an integer", this.name);
			}
			
			return ta && tb;
		}

		@Override
		protected Attr fold(Attr a, Attr b) {
			int d = b.vint();
			
			if (d == 0) {
				errors.add(this.args.get(1), "division by 0");
				return Attr.Int;
			} else {
				return new Attr(a.vint() / d);
			}
		}

		@Override
		protected Attr compile(Attr a, Attr b) {
			if (b.value != null && b.vint() == 0) {
				errors.add(this.args.get(1), "division by 0");
			} else {
				builder.op("divi");
				builder.iferr("rt_err_div_by_zero");
			}
			
			return Attr.Int;
		}
	};
	
	public final static DBuiltIn DivR = new BOp.Binary("/", TNative.Real)
	{
		@Override
		protected boolean typecheck(Attr a, Attr b) {
			boolean ta = a.isNumber();
			boolean tb = b.isNumber();
			
			if (!ta) {
				errors.add(this.args.get(0), "argument #1 to operator '%s' must be a number", this.name);
			}
			
			if (!tb) {
				errors.add(this.args.get(1), "argument #2 to operator '%s' must be a number", this.name);
			}
			
			return ta && tb;
		}

		@Override
		protected Attr fold(Attr a, Attr b) {
			float d = b.vreal();
			
			if (d == 0.0) {
				errors.add(this.args.get(1), "division by 0");
				return Attr.Real;
			} else {
				return new Attr(a.vreal() / d);
			}
		}

		@Override
		protected Attr compile(Attr a, Attr b) {
			if (b.value != null && b.vreal() == 0.0f) {
				errors.add(this.args.get(1), "division by 0");
			} else {
				if (a.type == TNative.Int) {
					builder.adjust(-1);
					builder.op("itor");
					builder.adjust(1);
				}
				
				if (b.type == TNative.Int) {
					builder.op("itor");
				}
				
				builder.op("divr");
				builder.iferr("rt_err_div_by_zero");
			}
			
			return Attr.Real;
		}
	};
	
	public final static DBuiltIn Mod = new BOp.Binary("mod", TNative.Int)
	{
		@Override
		protected boolean typecheck(Attr a, Attr b) {
			boolean ta = a.type.isInt();
			boolean tb = b.type.isInt();
			
			if (!ta) {
				errors.add(this.args.get(0), "argument #1 to operator '%s' must be an integer", this.name);
			}
			
			if (!tb) {
				errors.add(this.args.get(1), "argument #2 to operator '%s' must be an integer", this.name);
			}
			
			return ta && tb;
		}

		@Override
		protected Attr fold(Attr a, Attr b) {
			int d = b.vint();
			
			if (d == 0) {
				errors.add(this.args.get(1), "modulus by 0");
				return Attr.Int;
			} else {
				return new Attr(a.vint() % d);
			}
		}

		@Override
		protected Attr compile(Attr a, Attr b) {
			if (b.value != null && b.vint() == 0) {
				errors.add(this.args.get(1), "modulus by 0");
			} else {
				builder.op("mod");
				builder.iferr("rt_err_mod_by_zero");
			}
			
			return Attr.Int;
		}
	};
	
	public final static DBuiltIn Neg = new BOp("-", 1, TNative.Int)
	{
		@Override
		protected boolean typecheck() {
			if (!this.compiledArgs.get(0).isNumber()) {
				errors.add(this.args.get(0), "argument #1 to operator '%s' must be a number", this.name);
				return false;
			}
			
			return true;
		}

		@Override
		protected Attr fold() {
			Attr arg = this.compiledArgs.get(0);
			
			if (arg.type == TNative.Int) {
				return new Attr(-arg.vint());
			} else {
				return new Attr(-arg.vreal());
			}
		}

		@Override
		protected Attr compile() {
			Attr arg = this.compiledArgs.get(0);
			
			if (arg.type == TNative.Int) {
				builder.konst(-1);
				builder.op("muli");
				return Attr.Int;
			} else {
				builder.konst(-1.0f);
				builder.op("mulr");
				return Attr.Real;
			}
		}
	};
	
	public final static DBuiltIn Pos = new BOp("+", 1, TNative.Int)
	{
		@Override
		protected boolean typecheck() {
			if (!this.compiledArgs.get(0).isNumber()) {
				errors.add(this.args.get(0), "argument #1 to operator '%s' must be a number", this.name);
				return false;
			}
			
			return true;
		}

		@Override
		protected Attr fold() {
			return this.compiledArgs.get(0);
		}

		@Override
		protected Attr compile() {
			return this.compiledArgs.get(0);
		}
	};
}
