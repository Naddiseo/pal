package pal.builtin;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import pal.codegen.Attr;
import pal.codegen.CompilerExpr;
import pal.codegen.Declared;
import pal.codegen.TNative;
import pal.codegen.Compiler.EvalContext;
import pal.codegen.TempASCBuilder;
import pal.common.ErrorReport;
import pal.xtext.pal.Expr;

public abstract class DBuiltIn extends Declared
{
	protected final boolean isFoldable;
	protected final boolean argsByRef;
	protected final boolean isVariadic;
	
	// Hack.
	protected int variadicArgIndex;
	
	// Current state flattened, for convenience
	protected CompilerExpr compiler;
	protected TempASCBuilder builder;
	protected ErrorReport errors;
	protected EObject source;
	protected List<Expr> args;
	protected List<Attr> compiledArgs;
	
	private State currentState;
	
	protected DBuiltIn(String name, boolean isFoldable) {
		super(name);
		this.isFoldable = isFoldable;
		this.argsByRef = false;
		this.isVariadic = false;
		this.currentState = null;
	}
	
	protected DBuiltIn(String name, boolean argsByRef, boolean isVariadic) {
		super(name);
		this.isFoldable = false;
		this.argsByRef = argsByRef;
		this.isVariadic = isVariadic;
		this.currentState = null;
	}
	
	public Attr compile(CompilerExpr compiler, EObject obj, List<Expr> args) {
		this.initializeState(compiler, obj, args);
		
		this.builder.push();
		
		Attr ret = this.compileOrFold(false);
		
		if (ret.value != null) {
			this.builder.clear();
			this.builder.konst(ret.value);
		} else {
			this.builder.flush();
		}
		
		this.disposeState();
		return ret;
	}
	
	public Attr fold(CompilerExpr compiler, EObject obj, List<Expr> args) {
		this.initializeState(compiler, obj, args);
		Attr ret = this.compileOrFold(true);
		this.disposeState();
		return ret;
	}
	
	private void initializeState(CompilerExpr compiler, EObject obj, List<Expr> args) {
		State newState = new State(this.currentState);
		
		newState.compiler = compiler;
		newState.source = obj;
		newState.args = args;
		newState.compiledArgs = null;
		
		this.compiler = newState.compiler;
		this.builder = newState.compiler.builder;
		this.errors = newState.compiler.errors;
		this.source = newState.source;
		this.args = newState.args;
		this.compiledArgs = newState.compiledArgs;
		
		this.currentState = newState;
	}
	
	private void disposeState() {
		if (this.currentState == null) return;
		
		State oldState = this.currentState.parent;
		
		if (oldState == null) {
			this.compiler = null;
			this.builder = null;
			this.errors = null;
			this.source = null;
			this.args = null;
			this.compiledArgs = null;
		} else {
			this.compiler = oldState.compiler;
			this.builder = oldState.compiler.builder;
			this.errors = oldState.compiler.errors;
			this.source = oldState.source;
			this.args = oldState.args;
			this.compiledArgs = oldState.compiledArgs;
		}
		
		this.currentState = oldState;
	}
	
	private Attr compileOrFold(boolean forceFolding) {
		EvalContext context = (this.argsByRef ? EvalContext.LVal : EvalContext.RVal);
		
		if (this.isVariadic) {
			this.variadicArgIndex = 0;
			
			for (Expr arg: this.args) {
				this.compiledArgs = new ArrayList<Attr>();
				this.currentState.compiledArgs = this.compiledArgs;
				this.compiledArgs.add(this.compiler.compile(arg, context));
				this.postArgCompilationStep(forceFolding);
				
				this.variadicArgIndex += 1;
			}
			
			this.variadicEnd();
			
			return Attr.Void;
		} else {
			if (this.expectedNumArgs() != this.args.size()) {
				this.errors.add(this.source, "'%s' expects %d arguments, given %d", this.name, this.expectedNumArgs(), this.args.size());
				return this.errorAttr();
			}
			
			this.compiledArgs = new ArrayList<Attr>();
			this.currentState.compiledArgs = this.compiledArgs;
			
			for (Expr arg: this.args) {
				Attr attr = (forceFolding ?
					this.compiler.fold(arg) :
					this.compiler.compile(arg, context)
				);
				
				this.compiledArgs.add(attr);
			}
			
			return this.postArgCompilationStep(forceFolding);
		}
	}
	
	private Attr postArgCompilationStep(boolean forceFolding) {
		boolean typechecks = this.typecheck();
		boolean constantArgs = this.isFoldable;
		boolean hasBottomArgs = false;
		
		for (Attr attr: this.compiledArgs) {
			if (attr.type == TNative.Bottom) {
				hasBottomArgs = true;
				constantArgs = false;
			} else if (attr.value == null) {
				constantArgs = false;
			}
		}
		
		if (hasBottomArgs || !typechecks) {
			return this.errorAttr();
		}
		
		if (constantArgs) {
			return this.fold();
		} else {
			if (forceFolding) {
				return this.errorAttr();
			} else {
				return this.compile();
			}
		}
	}
	
	protected abstract int expectedNumArgs();
	protected abstract boolean typecheck();
	protected abstract Attr fold();
	protected abstract Attr compile();
	
	protected void variadicEnd() {
		// Nothing by default.
	}
	
	protected Attr errorAttr() {
		return Attr.Bottom;
	}
	
	public String kind() {
		return "built-in";
	}
	
	// This is unfortunately needed.
	// (all instances of DBuiltIn are singletons)
	private static class State
	{
		public CompilerExpr compiler;
		public EObject source;
		public List<Expr> args;
		public List<Attr> compiledArgs;
		
		public final State parent;
		
		public State(State parent) {
			this.parent = parent;
		}
	}
	
}
