package pal.builtin;

import pal.codegen.Attr;
import pal.codegen.TNative;

public enum LogicOps
{
	/* Mandatory enum semicolon */;
	
	public final static DBuiltIn And = new BOp.Binary.Logic("and")
	{
		@Override
		protected Attr fold(Attr a, Attr b) {
			return new Attr(a.vbool() && b.vbool());
		}

		@Override
		protected Attr compile(Attr a, Attr b) {
			builder.op("and");
			return Attr.Bool;
		}
	};
	
	public final static DBuiltIn Or = new BOp.Binary.Logic("or")
	{
		@Override
		protected Attr fold(Attr a, Attr b) {
			return new Attr(a.vbool() || b.vbool());
		}

		@Override
		protected Attr compile(Attr a, Attr b) {
			builder.op("or");
			return new Attr(this.returnType, false);
		}
	};
	
	public final static DBuiltIn Not = new BOp("not", 1, TNative.Bool)
	{
		@Override
		protected boolean typecheck() {
			boolean tc = this.compiledArgs.get(0).type.isBool();
			
			if (!tc) {
				errors.add(this.args.get(0), "argument #1 to '%s' must be boolean", this.name);
			}
			
			return tc;
		}
		
		@Override
		protected Attr fold() {
			return new Attr(!this.compiledArgs.get(0).vbool());
		}

		@Override
		protected Attr compile() {
			builder.op("not");
			return new Attr(this.returnType, false);
		}
	};
}
