package pal.builtin;

import pal.codegen.Attr;
import pal.codegen.DType;
import pal.codegen.TNative;

abstract class BFunc extends DBuiltIn
{
	protected BFunc(String name, boolean isFoldable) {
		super(name, isFoldable);
	}
	
	public static abstract class Variadic extends DBuiltIn
	{
		protected Variadic(String name, boolean argsByRef) {
			super(name, argsByRef, true);
		}
		
		@Override
		protected Attr errorAttr() {
			return Attr.Void;
		}
		
		@Override
		protected int expectedNumArgs() {
			return 1;
		}
		
		@Override
		protected Attr fold() {
			return Attr.Void;
		}
	}
	
	public static abstract class Unary extends BFunc
	{
		protected Unary(String name) {
			super(name, true);
		}
		
		@Override
		protected boolean typecheck() {
			Attr arg = this.compiledArgs.get(0);
			
			if (arg.type != TNative.Bottom) {
				return this.typecheckArg(arg.type);
			} else {
				return false;
			}
		}
		
		protected abstract boolean typecheckArg(DType arg);
		
		@Override
		protected Attr fold() {
			return this.foldArg(this.compiledArgs.get(0));
		}
		
		@Override
		protected Attr compile() {
			return this.compileArg(this.compiledArgs.get(0));
		}
		
		protected abstract Attr foldArg(Attr arg);
		protected abstract Attr compileArg(Attr arg);
		
		@Override
		protected int expectedNumArgs() {
			return 1;
		}
	}
}