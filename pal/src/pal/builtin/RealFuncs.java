package pal.builtin;

import pal.codegen.Attr;
import pal.codegen.DType;
import pal.codegen.TNative;

public enum RealFuncs {
	/* This semicolon is, sadly enough, required */;
	
	public final static DBuiltIn Trunc = new BFunc.Unary("trunc") {
		@Override
		protected boolean typecheckArg(DType arg) {
			if (!arg.isNumber()) {
				this.errors.add(this.args.get(0),
						"argument #1 to 'trunc' must be number");
				return false;
			}
			
			return true;
		}
		
		@Override
		protected Attr errorAttr() {
			return Attr.Int;
		}
		
		@Override
		protected Attr foldArg(Attr arg) {
			if (arg.type == TNative.Real) {
				return new Attr((int) Math.floor(arg.vreal()));
			}
			else {
				return arg;
			}
		}
		
		@Override
		protected Attr compileArg(Attr arg) {
			if (arg.type == TNative.Real) {
				this.builder.op("rtoi");
			}
			
			return Attr.Int;
		}
	};
	
	public final static DBuiltIn Round = new BFunc.Unary("round") {
		@Override
		protected boolean typecheckArg(DType arg) {
			if (!arg.isNumber()) {
				this.errors.add(this.args.get(0),
						"argument #1 to 'round' must be number");
				return false;
			}
			
			return true;
		}
		
		@Override
		protected Attr errorAttr() {
			return Attr.Int;
		}
		
		@Override
		protected Attr foldArg(Attr arg) {
			if (arg.type == TNative.Real) {
				return new Attr(Math.round(arg.vreal()));
			}
			else {
				return arg;
			}
		}
		
		@Override
		protected Attr compileArg(Attr arg) {
			if (arg.type == TNative.Real) {
				this.builder.konst(0.5f);
				this.builder.op("addr");
				this.builder.op("rtoi");
			}
			
			return Attr.Int;
		}
	};
	
	public final static DBuiltIn Abs = new BFunc.Unary("abs") {
		@Override
		protected Attr errorAttr() {
			return Attr.Int;
		}
		
		@Override
		protected boolean typecheckArg(DType arg) {
			if (!arg.isNumber()) {
				this.errors.add(this.args.get(0),
						"argument #1 to 'abs' must be a number");
				return false;
			}
			
			return true;
		}
		
		@Override
		protected Attr foldArg(Attr arg) {
			if (arg.type == TNative.Real) {
				return new Attr(Math.abs(arg.vreal()));
			}
			else {
				return new Attr(Math.abs(arg.vint()));
			}
		}
		
		@Override
		protected Attr compileArg(Attr arg) {
			if (arg.type == TNative.Real) {
				this.builder.call(0, "std_absr");
			}
			else {
				this.builder.call(0, "std_absi");
			}
			
			return new Attr(arg.type, false);
		}
	};
	
	public final static DBuiltIn Sqr = new BFunc.Unary("sqr") {
		@Override
		protected Attr errorAttr() {
			return Attr.Int;
		}
		
		@Override
		protected boolean typecheckArg(DType arg) {
			if (!arg.isNumber()) {
				this.errors.add(this.args.get(0),
						"argument #1 to 'sqr' must be a number");
				return false;
			}
			
			return true;
		}
		
		@Override
		protected Attr foldArg(Attr arg) {
			if (arg.type == TNative.Real) {
				return new Attr(arg.vreal() * arg.vreal());
			}
			else {
				return new Attr(arg.vint() * arg.vint());
			}
		}
		
		@Override
		protected Attr compileArg(Attr arg) {
			this.builder.dup();
			
			if (arg.type == TNative.Real) {
				this.builder.op("mulr");
			}
			else {
				this.builder.op("muli");
			}
			
			return new Attr(arg.type, false);
		}
	};
	
	public final static DBuiltIn Sqrt = new BFunc.Unary("sqrt") {
		@Override
		protected Attr errorAttr() {
			return Attr.Real;
		}
		
		@Override
		protected boolean typecheckArg(DType arg) {
			if (!arg.isNumber()) {
				this.errors.add(this.args.get(0),
						"argument #1 to 'sqrt' must be a number");
				return false;
			}
			
			return true;
		}
		
		@Override
		protected Attr foldArg(Attr arg) {
			float f = arg.vreal();
			
			if (f < 0) {
				errors.add(this.args.get(0), "argument #1 to 'sqrt' must be non-negative");
				return Attr.Real;
			} else {
				return new Attr((float)Math.sqrt(f));
			}
		}
		
		@Override
		protected Attr compileArg(Attr arg) {
			if (arg.type == TNative.Int) {
				this.builder.op("itor");
			}
			
			this.builder.call(0, "std_sqrt");
			
			return Attr.Real;
		}
	};
	
	public final static DBuiltIn Sin = new BFunc.Unary("sin") {
		@Override
		protected Attr errorAttr() {
			return Attr.Real;
		}
		
		@Override
		protected boolean typecheckArg(DType arg) {
			if (!arg.isNumber()) {
				this.errors.add(this.args.get(0),
						"argument #1 to 'sin' must be a number");
				return false;
			}
			
			return true;
		}
		
		@Override
		protected Attr foldArg(Attr arg) {
			float f = arg.vreal();
			return new Attr((float) Math.sin(f));
		}
		
		@Override
		protected Attr compileArg(Attr arg) {
			if (arg.type == TNative.Int) {
				this.builder.op("itor");
			}
			
			this.builder.call(0, "std_sin");
			
			return Attr.Real;
		}
	};
	
	public final static DBuiltIn Exp = new BFunc.Unary("exp") {
		@Override
		protected Attr errorAttr() {
			return Attr.Real;
		}
		
		@Override
		protected boolean typecheckArg(DType arg) {
			if (!arg.isNumber()) {
				this.errors.add(this.args.get(0),
						"argument #1 to 'exp' must be a number");
				return false;
			}
			
			return true;
		}
		
		@Override
		protected Attr foldArg(Attr arg) {
			float f = arg.vreal();
			return new Attr((float) Math.exp(f));
		}
		
		@Override
		protected Attr compileArg(Attr arg) {
			if (arg.type == TNative.Int) {
				this.builder.op("itor");
			}
			
			this.builder.call(0, "std_exp");
			
			return Attr.Real;
		}
	};
	
	public final static DBuiltIn Ln = new BFunc.Unary("ln") {
		@Override
		protected Attr errorAttr() {
			return Attr.Real;
		}
		
		@Override
		protected boolean typecheckArg(DType arg) {
			if (!arg.isNumber()) {
				this.errors.add(this.args.get(0),
						"argument #1 to 'ln' must be a number");
				return false;
			}
			
			return true;
		}
		
		@Override
		protected Attr foldArg(Attr arg) {
			float f = arg.vreal();
			
			if (f <= 0) {
				errors.add(this.args.get(0), "argument #1 to 'ln' must be positive");
				return Attr.Real;
			} else {
				return new Attr((float)Math.log(f));
			}
		}
		
		@Override
		protected Attr compileArg(Attr arg) {
			if (arg.type == TNative.Int) {
				this.builder.op("itor");
			}
			
			this.builder.call(0, "std_ln");
			
			return Attr.Real;
		}
	};
}
