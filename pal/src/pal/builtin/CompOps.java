package pal.builtin;

import pal.codegen.Attr;
import pal.codegen.DType;
import pal.codegen.TArray;
import pal.codegen.TEnum;
import pal.codegen.TNative;

public enum CompOps
{
	/* Mandatory enum semicolon */;
	
	public static DBuiltIn Equal = new CompBase("=") {
		@Override
		protected boolean foldi(int a, int b) {
			return a == b;
		}

		@Override
		protected boolean foldr(float a, float b) {
			return a == b;
		}

		@Override
		protected boolean folds(String a, String b) {
			return a.equals(b);
		}

		@Override
		protected void compilei() {
			builder.op("eqi");
		}

		@Override
		protected void compiler() {
			builder.op("eqr");
		}

		@Override
		protected void compiles(int length) {
			builder.konst(length);
			builder.call(0, "std_compstr");
			builder.adjust(-(2*length));
			builder.konst(0);
			builder.eq(TNative.Int);
		}
	};
	
	public static DBuiltIn NotEqual = new CompBase("<>") {
		@Override
		protected boolean foldi(int a, int b) {
			return a != b;
		}

		@Override
		protected boolean foldr(float a, float b) {
			return a != b;
		}

		@Override
		protected boolean folds(String a, String b) {
			return !a.equals(b);
		}

		@Override
		protected void compilei() {
			builder.op("eqi");
			builder.op("not");
		}

		@Override
		protected void compiler() {
			builder.op("eqr");
			builder.op("not");
		}

		@Override
		protected void compiles(int length) {
			builder.konst(length);
			builder.call(0, "std_compstr");
			builder.adjust(-(2*length));
			builder.konst(0);
			builder.eq(TNative.Int);
			builder.op("not");
		}
	};
	
	public static DBuiltIn LessEqual = new CompBase("<=") {
		@Override
		protected boolean foldi(int a, int b) {
			return a <= b;
		}

		@Override
		protected boolean foldr(float a, float b) {
			return a <= b;
		}

		@Override
		protected boolean folds(String a, String b) {
			return a.compareTo(b) <= 0;
		}

		@Override
		protected void compilei() {
			builder.op("gti");
			builder.op("not");
		}

		@Override
		protected void compiler() {
			builder.op("gtr");
			builder.op("not");
		}

		@Override
		protected void compiles(int length) {
			builder.konst(length);
			builder.call(0, "std_compstr");
			builder.adjust(-(2*length));
			builder.konst(0);
			builder.op("gti");
			builder.op("not");
		}
	};
	
	public static DBuiltIn GreaterEqual = new CompBase(">=") {
		@Override
		protected boolean foldi(int a, int b) {
			return a >= b;
		}

		@Override
		protected boolean foldr(float a, float b) {
			return a >= b;
		}

		@Override
		protected boolean folds(String a, String b) {
			return a.compareTo(b) >= 0;
		}

		@Override
		protected void compilei() {
			builder.op("lti");
			builder.op("not");
		}

		@Override
		protected void compiler() {
			builder.op("ltr");
			builder.op("not");
		}

		@Override
		protected void compiles(int length) {
			builder.konst(length);
			builder.call(0, "std_compstr");
			builder.adjust(-(2*length));
			builder.konst(0);
			builder.op("lti");
			builder.op("not");
		}
	};
	
	public static DBuiltIn LessThan = new CompBase("<") {
		@Override
		protected boolean foldi(int a, int b) {
			return a < b;
		}

		@Override
		protected boolean foldr(float a, float b) {
			return a < b;
		}

		@Override
		protected boolean folds(String a, String b) {
			return a.compareTo(b) < 0;
		}

		@Override
		protected void compilei() {
			builder.op("lti");
		}

		@Override
		protected void compiler() {
			builder.op("ltr");
		}

		@Override
		protected void compiles(int length) {
			builder.konst(length);
			builder.call(0, "std_compstr");
			builder.adjust(-(2*length));
			builder.konst(0);
			builder.op("lti");
		}
	};
	
	public static DBuiltIn GreaterThan = new CompBase(">") {
		@Override
		protected boolean foldi(int a, int b) {
			return a > b;
		}

		@Override
		protected boolean foldr(float a, float b) {
			return a > b;
		}

		@Override
		protected boolean folds(String a, String b) {
			return a.compareTo(b) > 0;
		}

		@Override
		protected void compilei() {
			builder.op("gti");
		}

		@Override
		protected void compiler() {
			builder.op("gtr");
		}

		@Override
		protected void compiles(int length) {
			builder.konst(length);
			builder.call(0, "std_compstr");
			builder.adjust(-(2*length));
			builder.konst(0);
			builder.op("gti");
		}
	};
	
	private static abstract class CompBase extends BOp.Binary
	{
		protected CompBase(String name) {
			super(name, TNative.Bool);
		}
		
		private boolean isComparableType(DType type) {
			return type.isNumber() || type instanceof TEnum || type.isString();
		}
		
		// 1: two ints
		// 2: at least one real, maybe an int
		// 3: two strings
		// 4: two equal enums
		// 5: error: unequal types
		// 6: error: equal, invalid types
		private int categorizeArgs(Attr a, Attr b) {
			if (a.isNumber() && b.isNumber()) {
				if (a.type.isInt()) {
					return b.type.isInt() ? 1 : 2;
				} else {
					return 2;
				}
			} else if (a.type.isString() && b.type.isString()) {
				return 3;
			} else if (a.type instanceof TEnum && b.type == a.type) {
				return 4;
			} else if (!a.type.equals(b.type)) {
				return 5;
			} else {
				return 6;
			}
		}
		
		@Override
		protected boolean typecheck(Attr a, Attr b) {
			int C = this.categorizeArgs(a, b);
			
			if (C < 3) {
				return true;
			} else if (C == 3) {
				if (a.isBottom() || b.isBottom()) return true;
				
				if (!a.type.equals(b.type)) {
					errors.add(this.source, "strings must have same length to be comparable");
					return false;
				}
				
				return true;
			} else if (C == 4) {
				return true;
			} else if (C == 5) {
				errors.add(this.source, "arguments to operator '%s' must be of compatible types", this.name);
				return false;
			} else {
				boolean ta = this.isComparableType(a.type);
				boolean tb = this.isComparableType(b.type);
				
				if (!ta) {
					errors.add(this.args.get(0), "argument #1 to operator '%s' must be integer, real, char, boolean, string, or enum", this.name);
				}
				
				if (!tb) {
					errors.add(this.args.get(1), "argument #2 to operator '%s' must be integer, real, char, boolean, string, or enum", this.name);
				}
				
				// By the previous checks, one of ta or tb will be false.
				return false;
			}
		}

		@Override
		protected Attr fold(Attr a, Attr b) {
			int C = this.categorizeArgs(a, b);
			
			boolean res;
			
			if (C == 1) {
				res = this.foldi(a.vint(), b.vint());
			} else if (C == 2) {
				res = this.foldr(a.vreal(), b.vreal());
			} else if (C == 3) {
				res = this.folds(a.vstring(), b.vstring());
			} else if (C == 4) {
				res = this.foldi(a.vint(), b.vint());
			} else {
				// Should never get here.
				return Attr.Bool;
			}
			
			return new Attr(res);
		}
		
		protected abstract boolean foldi(int a, int b);
		protected abstract boolean foldr(float a, float b);
		protected abstract boolean folds(String a, String b);
		
		@Override
		protected Attr compile(Attr a, Attr b) {
			int C = this.categorizeArgs(a, b);
			
			if (C == 1) {
				this.compilei();
			} else if (C == 2) {
				if (a.type == TNative.Int) {
					builder.adjust(-1);
					builder.op("itor");
					builder.adjust(1);
				}
				
				if (b.type == TNative.Int) {
					builder.op("itor");
				}
				
				this.compiler();
			} else if (C == 3) {
				this.compiles(((TArray)a.type).size());
			} else if (C == 4) {
				this.compilei();
			} else {
				// Should never get here.
				return Attr.Bool;
			}
			
			return Attr.Bool;
		}
		
		protected abstract void compilei();
		protected abstract void compiler();
		protected abstract void compiles(int length);
	}
}
