//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.bins;

import java.io.File;
import java.io.FileNotFoundException;

import pal.common.ErrorReport;
import pal.common.Parser;
import pal.common.Error;
import pal.codegen.Compiler;
import pal.codegen.ASCBuilder;
import pal.codegen.FileASCBuilder;

public class OddEven {
	public static void main(String[] args) throws FileNotFoundException {
		File input = new File("files/oe.pal");
		
		Parser.Result result = Parser.Instance.parse(input);
		
		if (result.report.getErrors().isEmpty()) {
			ASCBuilder builder = new FileASCBuilder("files/oe.asc");
			Compiler compiler = new Compiler(builder, true);
			
			ErrorReport errors = compiler.compile(result.program);
			
			for (Error error: errors.getErrors()) {
				System.err.println(error);
			}
		} else {
			for (Error error: result.report.getErrors()) {
				System.err.println(error);
			}
		}
	}
}
