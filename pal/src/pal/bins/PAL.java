//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.bins;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

import pal.codegen.ASCBuilder;
import pal.codegen.Compiler;
import pal.codegen.NullASCBuilder;
import pal.common.ErrorListing;
import pal.common.Parser;
import pal.optimizer.BasicBlockBuilder;

/*
 * The main PAL compiler.
 * 
 * Usage: java pal.bins.PAL [options] <file>.pal
 * 
 * Does:
 * 1. Produces a listing of errors into <file>.lst
 * 2. Compiles the file into <file>.asc
 * 3. Runs the ASC interpreter on <file>.asc
 * 
 * Options is zero or more of:
 * 	-S   suppress removal of <file>.asc after interpretation
 *  -n   do not produce the program listing
 *  -a   do not generate run-time array subscript checking
 *  -c   same as -S, and suppress interpretation of <file>.asc
 * 
 */
public class PAL {
	private final static String DefaultASC = "~c415/ASC/bin/asc";
	
	public static void main(String[] args)
	{
		// Default values mandated by the project spec.
		boolean produceListing = true;
		boolean runASC = true;
		boolean arraySubscriptChecking = true;
		boolean keepASC = false;
		String ascExe = PAL.DefaultASC;
		
		// Parse args
		if (args.length < 1) {
			PrintUsage();
			return;
		}
		
		int last = args.length - 1;
		
		String infile = args[last];
		
		if (!infile.endsWith(".pal")) {
			System.err.println("Error: input file must end with \".pal\"");
			return;
		}
		
		infile = infile.substring(0, infile.length() - 4);
		
		for (int i = 0; i < last; ++i) {
			String arg = args[i];
			
			if ("-S".equals(arg)) {
				keepASC = true;
			} else if ("-n".equals(arg)) {
				produceListing = false;
			} else if ("-a".equals(arg)) {
				arraySubscriptChecking = false;
			} else if ("-c".equals(arg)) {
				keepASC = true;
				runASC = false;
			} else if ("-asc".equals(arg)) {
				if (i < last - 1) {
					ascExe = args[i + 1];
					i += 1;
				}
			} else {
				System.err.println("Error: invalid option \"" + arg + "\"");
				return;
			}
		}
		
		PAL.Compile(infile, produceListing, runASC, arraySubscriptChecking, keepASC, ascExe);
	}
	
	private static void PrintUsage() {
		PrintStream out = System.out;
		
		out.println("Usage:");
		out.println("\tpal [options] <file>.pal\n");
		out.println("Options are:");
		out.println("\t-S  suppress removal of <file>.asc after interpretation");
		out.println("\t-n  do not produce the program listing");
		out.println("\t-a  do not generate run-time array subscript checking");
		out.println("\t-c  same as -S, and suppress interpretation of <file>.asc");
	}
	
	private static void Compile(
		String file,
		boolean produceListing, boolean runASC,
		boolean arraySubscriptChecking, boolean keepASC,
		String ascExe
	) {
		File fpal = new File(file + ".pal");
		File fasc = new File(file + ".asc");
		File flst = new File(file + ".lst");
		
		try	{
			System.out.println("Compiling '" + file + ".pal'...");
			
			ErrorListing listing = PAL.Codegen(fpal, fasc, arraySubscriptChecking);
			
			if (produceListing) {
				listing.write(flst, false);
			}
			
			listing.write(System.err, true);
			
			if (listing.isEmpty()) {
				System.out.println("No errors.");
				
				if (runASC) {
					System.out.println("Running ASC...");
					
					try {
						PAL.Interpret(ascExe, fasc);
					} catch (Exception ex) {
						System.err.println(ex.getMessage());
					}
				}
				
				if (!keepASC && fasc.exists()) {
					fasc.delete();
				}
			} else {
				System.out.println(listing.size() + " errors");
			}
			
			System.out.println("Done.");
		} catch (FileNotFoundException ex) {
			System.err.println("Error: file \"" + fpal.getName() + "\" does not exist");
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		}
	}
	
	private static void Interpret(String ascExe, File fasc) {
		try {
			// run the Unix "ps -ef" command
			// using the Runtime exec method:
			Process p = Runtime.getRuntime().exec(ascExe, new String[] { fasc.toString() });
			
			BufferedReader stdInput = new BufferedReader(
				new InputStreamReader(p.getInputStream())
			);
			
			BufferedReader stdError = new BufferedReader(
				new InputStreamReader(p.getErrorStream())
			);
			
			BufferedReader stdOutput = new BufferedReader(
				new InputStreamReader(System.in)
			);
			
			String s;
			
			//while ((s = stdOutput.readLine()) != null) {
			//	p.getOutputStream().write(s.getBytes());
			//}
			p.getOutputStream().close();
			
			while ((s = stdError.readLine()) != null) {
			    System.err.println(s);
			}
			
			while ((s = stdInput.readLine()) != null) {
			    System.out.println(s);
			}
    } catch (IOException e) {
			System.err.println(String.format(
				"Error while executing '%s' on '%s':",
				ascExe, fasc.toString()
			));
			
			e.printStackTrace();
    }
	}
	
	private static ErrorListing Codegen(File fpal, File fasc, boolean arraySubscriptChecking)
		throws IOException
	{	
		Parser.Result result = Parser.Instance.parse(fpal);
		ErrorListing return_listing = new ErrorListing(fpal, result.report);
		ASCBuilder builder;
		
		if (result.report.getErrors().isEmpty()) {
			builder = new BasicBlockBuilder(fasc);
		} else {
			builder = NullASCBuilder.Instance;
		}
		
		Compiler compiler = new Compiler(builder, arraySubscriptChecking);
		return_listing.add(compiler.compile(result.program));
		
		return return_listing;
	}
}
