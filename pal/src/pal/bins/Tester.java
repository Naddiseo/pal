//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.bins;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import pal.common.Error;
import pal.common.ErrorReport;
import pal.common.Test;
import pal.common.Test.Phase;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class Tester
{
	static Logger logger = Logger.getLogger(Tester.class.getName());
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		// set logger to OFF to prevent unwanted output
		logger.setLevel(Level.OFF);
		
		ArrayList<Test> testList = new ArrayList<Test>();
		
		//
		// Syntactic Tests
		//
		// ASCBuilder is run on each them as well to test how well it deals with incomplete parse trees
		//
		for (int i = 0; i <= 9; i++) {
			String filename = "files/parser/" + i + ".pal";
			testList.add(new Test(filename, 0, Phase.SemanticBuilder, false));
		}
		
		for (int i = 1; i <= 24; i++) {
			String filename = String.format("files/tests/cp1/%02d.pal", i);
			testList.add(new Test(filename, 0, Phase.SemanticBuilder, false));			
		}
		
		for (int i = 1; i <= 50; i++) {
			String filename = String.format("files/tests/test%02d.pal", i);
			testList.add(new Test(filename, 0, Phase.SemanticBuilder, false));			
		}
		
		//
		// Semantic Tests
		//
		testList.add(new Test("files/nt01.pal", 0, Phase.SemanticBuilder, true));
		
		for (int i = 0; i <= 9; i++) {
			String filename = String.format("files/semantic/%d.pal", i);
			testList.add(new Test(filename, 0, Phase.SemanticBuilder, false));
		}


		
		int i = 0;
		List<Integer> failures = new ArrayList<Integer>();
		for (Test test : testList) {
			i++;
			try {
				test.run();
				
				System.out.println("TEST " + i + ": " +  test.getFilename());
				if (test.getSuccess()) {
					System.out.println("PASS");
				} else {
					System.out.println("FAIL");
				}
							
				String noViableAlt = "no viable alternative at input";
				String noViableAltAtChar = "no viable alternative at character ";
				ErrorReport parserReport = test.getParserReport();			
				System.out.println("PARSER ERRORS: " + parserReport.getErrorCount());
				for (Error error : parserReport.getErrors()) {			
					
					String msg = error.getMessage();
					if (msg.length() > noViableAlt.length()) {
						if (msg.substring(0, noViableAlt.length()).equals(noViableAlt)) {
							msg = "unexpected character " + msg.substring(noViableAlt.length());
						}
					}
					if (msg.length() > noViableAltAtChar.length()) {
						if (msg.substring(0, noViableAltAtChar.length()).equals(noViableAltAtChar)) {
							msg = "illegal character " + msg.substring(noViableAltAtChar.length()); 
						}
					}
					System.out.println("" + error.getLine() + "\t" + msg);
				}
				
				System.out.println("PARSER WARNINGS: " + parserReport.getWarningCount());
				for (Error error : parserReport.getWarnings()) {				
					System.out.println("" + error.getLine() + "\t" + error.getMessage());
				}
				
				
				// don't go any further if we're just testing the parser
				if (test.getPhase() == Test.Phase.Parser) {
					System.out.println();
					continue;
				}
							
				if (parserReport.getErrorCount() != 0 && test.getHaltOnError()) {
					System.out.println("TEST INCOMPLETE, HALT ON ERROR");
					System.out.println();
					continue;
				}
				
				ErrorReport semanticReport = test.getSemanticReport();			
				System.out.println("SEMANTIC ERRORS: " + semanticReport.getErrorCount());
				for (Error error : semanticReport.getErrors()) {
					int lineNumber = error.getLine();

					if (lineNumber > 0) {
						String line = test.sourceCode.get(lineNumber-1);
						System.out.println("" + lineNumber + ":\t" + line);
						char chars[] = line.toCharArray();
						
						for (int i2 = 0; i2 < chars.length; i2++) {
							if (!Character.isWhitespace(chars[i2])) {
								chars[i2] = ' ';
							}
						}
						
						int length = error.getLength();
						int start = error.getStartOffset();
						
						chars[start] = '^';
						line = new String(chars);
						line = line.substring(0, start+1);
						
						if (length > 0) {
							for (int i2 = 0; i2 < length - 1; i2++) {
								line += "^";
							}
						}
						System.out.println("\t" + line + "   " + error.getMessage());
						//System.out.println(error.getEndOffset() - error.getStartOffset());
					} else {
						System.out.println("\t" + error.getMessage());
					}
					System.out.println();
				}
			} catch (Exception e) {
				System.out.println("TEST FAILED");
				
				for (StackTraceElement se : e.getStackTrace()) {
					System.out.println(se.toString());
				}
				failures.add(i);
			} 
			
			System.out.println();
		}
		
		if (failures.size() == 0) {
			System.out.println("ALL TESTS PASSED!");
		} else {
			System.out.println("FAILED TESTS");
			for (i = 0; i < failures.size(); i++) {
				System.out.print(failures.get(i).toString() + " ");
			}
		}
				
		// pretty printing of listings
		//System.out.println();
		//System.out.println("LISTING");
		
		// builds the semantic tree, not necessary for checkpoint 1		
		//Parser parser = Parser.Instance;
		//Parser.Result result = parser.parse("files/semantic/sem01.pal");
		
		//pal.sem.Builder builder = new Builder();
		//builder.get(result.program);
			
		//ErrorReport report = builder.getErrorReport();

		/*try {
			ErrorListing listing = new ErrorListing(new File("files/semantic/sem01.pal"), result.report);
			listing.write(System.out);
		} catch (IOException e) {
			System.out.println("couldn't read file: files/semantic/sem01.pal");
		}*/
	}
}

