//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.bins;

import java.io.File;
import java.io.FileNotFoundException;

import pal.common.Error;
import pal.common.ErrorReport;
import pal.common.Parser;
import pal.codegen.ASCBuilder;
import pal.codegen.Compiler;
import pal.optimizer.BasicBlockBuilder;

/*
 * A PAL compiler useful for debugging.
 * 
 * Usage: java pal.bins.PALTest [pal file]
 * 
 * Prints the lexical, syntactic, and semantic errors in the file.
 */
public class PALTest
{
	public static void main(String[] args) throws FileNotFoundException {
		if (args.length < 1) {
			print("Usage: java PALTest file.pal");
			return;
		}
		
		Parser parser = Parser.Instance;
		Parser.Result result = parser.parse(args[0]);
		printErrors(result.report);
		
		int nerrors = result.report.size();
		
		print(nerrors + " syntax errors");
		
		ASCBuilder builder = new BasicBlockBuilder(new File("files/out.asc"));
		Compiler compiler = new Compiler(builder, false);
		ErrorReport report = compiler.compile(result.program);
		
		nerrors = report.size();
		printErrors(report);
		
		print(nerrors + " semantic errors");
	}
	
	private static void print(String str) {
		System.out.println(str);
		System.err.flush();
		System.out.flush();
	}
	
	private static void printErrors(ErrorReport report) {
		for (Error err: report.getErrors()) {
			System.err.println(err.toString());
			System.out.flush();
			System.err.flush();
		}
	}
}
