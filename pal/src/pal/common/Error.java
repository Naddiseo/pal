//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.common;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.nodemodel.ILeafNode;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;

public class Error {
	public enum Level { Error, Warning };
	
	private final String message;
	private final int line;
	private final int startOffset;
	private final int endOffset;
	private final int length;
	private final Object[] args;
	public final Level level;
	
	public Error(INode node) {
		this.message = Error.PreprocMessage(node.getSyntaxErrorMessage().getMessage().replace("%", "%%"));
		this.line = node.getStartLine();
		this.startOffset = Error.GetOffsetOnLine(node);
		this.endOffset = this.startOffset + node.getText().length() - 1;
		this.length = node.getText().length();
		this.args = new Object[] {};
		this.level = Level.Error;
	}
	
	public Error(EObject obj, String message, Object... args) {
		if (obj == null) {
			this.line = 1;
			this.startOffset = 0;
			this.endOffset = 0;
			this.length = 0;
		} else {
			INode node = NodeModelUtils.getNode(obj);
			this.length = node.getText().length();
			this.line = node.getStartLine();
			this.startOffset = Error.GetOffsetOnLine(node);
			this.endOffset = this.startOffset + node.getText().length() - 1;
		}
		
		this.message = Error.PreprocMessage(message);
		this.args = args;
		this.level = Level.Error;
	}
	
	public Error(EObject obj, Level level, String message, Object... args) {
		if (obj == null) {
			this.line = 0;
			this.startOffset = 0;
			this.endOffset = 0;
			this.length = 0;
		} else {
			INode node = NodeModelUtils.getNode(obj);
			this.length = node.getText().length();
			this.line = node.getStartLine();
			this.startOffset = Error.GetOffsetOnLine(node);
			this.endOffset = this.startOffset + node.getText().length() - 1;
		}
		
		this.message = Error.PreprocMessage(message);
		this.args = args;
		this.level = level;
	}
	
	private static int GetOffsetOnLine(INode node) {
		INode root = node.getRootNode();
		int fromOffset = node.getOffset();
		
		if (fromOffset == 0) return 0;

		List<ILeafNode> r = new ArrayList<ILeafNode>();
		
		for(ILeafNode l: root.getLeafNodes()) {
			if (l.getOffset() >= fromOffset) break;
			r.add(l);
		}
		
		int offset = 0;
		
		Pattern p = Pattern.compile("^.*\\r?\\n", Pattern.DOTALL | Pattern.MULTILINE);
		for (int i = r.size() - 1; i >= 0; --i) {
			String t = r.get(i).getText();
			Matcher m = p.matcher(t);
			
			offset += t.length();
			
			if (m.find()) {
				offset -= m.end();
				break;
			}
		}
		
		return offset;
	}
	
	public int getLine() {
		return line;
	}
	
	public int getLength() {
		return length;
	}
	
	public String getMessage() {
		return String.format(this.message, this.args);
	}
	
	@Override
	public String toString() {
		return "Error on line " + this.line + ", char " + (this.startOffset + 1) + ": " + this.getMessage();
	}
	
	public int getStartOffset() {
		return this.startOffset;
	}
	
	public int getEndOffset() {
		return this.endOffset;
	}
	
	public Level getLevel() {
		return this.level;
	}
	
	// Makes the error messages more "friendly".
	private static String PreprocMessage(String message) {
		message = message.replaceAll("RULE_ID", "identifier");
		message = message.replaceAll("RULE_L_INT", "integer");
		message = message.replaceAll("RULE_L_REAL", "real");
		message = message.replaceAll("RULE_L_STR", "string");
		
		Pattern p;
		Matcher m;
		
		p = Pattern.compile("^no viable alternative at (character|input) '(.+)'$", Pattern.CASE_INSENSITIVE);
		m = p.matcher(message);
		
		if (m.matches()) {
			message = "unexpected " + m.group(1) + " '" + m.group(2) + "'";
			return message;
		}
		
		p = Pattern.compile("^(mismatched|extraneous) input '(.+)' expecting '(.+)'$", Pattern.CASE_INSENSITIVE);
		m = p.matcher(message);
		
		if (m.matches()) {
			message = "expecting '" + m.group(3) + "' before '" + m.group(2) + "'";
			return message;
		}
		
		return message;
	}
}