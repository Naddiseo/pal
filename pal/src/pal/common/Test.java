//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.common;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pal.codegen.ASCBuilder;
import pal.codegen.Compiler;
import pal.codegen.NullASCBuilder;
import pal.common.Parser;

public class Test
{
	public enum Phase {
		Parser, SemanticBuilder, IRBuilder, CodeGen
	};
	
	private String filename;
	private int expectedErrors;
	private Phase phase;
	private boolean success;
	private boolean haltOnError;
	private ErrorReport parserReport;
	private ErrorReport semanticReport;
	public final List<String> sourceCode;
	
	public Test(String filename, int expectedErrors, Phase phase, boolean haltOnErrors) {
		this.filename = filename;
		this.expectedErrors = expectedErrors;
		this.phase = phase;	
		this.success = false;
		this.haltOnError = haltOnErrors;
		this.parserReport = new ErrorReport();
		this.semanticReport = new ErrorReport();
		this.sourceCode = new ArrayList<String>();
	}
	
	public void run() throws FileNotFoundException, IOException {
		
		BufferedReader br = new BufferedReader(new FileReader(this.filename));
		String line;
		while ((line = br.readLine()) != null) {
			sourceCode.add(line);
		}
		br.close();
		
		Parser parser = Parser.Instance;
		Parser.Result parserResult = parser.parse(this.filename);			
		this.parserReport = parserResult.report;
		
		// don't build semantic tree
		if (this.phase == Phase.Parser) {			
			if (this.parserReport.getErrorCount() == this.expectedErrors) {
				this.success = true;
			}		
			
			return;
		}
		
		
				
		// exit early if we have parser errors and haltOnErrors is set
		if (this.parserReport.getErrorCount() != 0 && haltOnError) {
			this.success = false;
			return;
		}
		
		
		ASCBuilder builder = NullASCBuilder.Instance;
		Compiler compiler = new Compiler(builder, true);
		this.semanticReport = compiler.compile(parserResult.program);
		
		
		// don't build IR representation
		if (this.phase == Phase.SemanticBuilder) {
			if (this.semanticReport.getErrorCount() == this.expectedErrors) {
				this.success = true;
			}
			return;
		}
		
		// exit early if we have semantic errors and haltOnErrors is set
		if (this.semanticReport.getErrorCount() != 0 && haltOnError) {
			this.success = false;
			return;
		}
		
		if (this.phase == Phase.IRBuilder || this.phase == Phase.CodeGen) {

			// don't perform code generation
			if (this.phase == Phase.IRBuilder) {
				return;
			}
		}
		
	}
	
	public String getFilename() { 
		return this.filename;
	}
	
	public ErrorReport getParserReport() {
		return this.parserReport;
	}
	
	public ErrorReport getSemanticReport() {
		return this.semanticReport;
	}
	
	public boolean getSuccess() {
		return this.success;
	}
	
	public Phase getPhase() {
		return this.phase;
	}
	
	public boolean getHaltOnError() {
		return this.haltOnError;
	}
}
