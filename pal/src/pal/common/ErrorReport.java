//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;


public class ErrorReport {
	private List<Error> errorList;
	private List<Error> warningList; 
	private Map<Integer, List<Error>> errorMap;  // maps line numbers to lists of errors on that line 

	public ErrorReport() {
		this.errorList = new ArrayList<Error>();		
		this.errorMap = new HashMap<Integer,List<Error>>();
		this.warningList = new ArrayList<Error>();	
	}
	
	public void addError(Error error) {
		switch (error.level) {
		case Error: this.errorList.add(error); break;
		case Warning: this.warningList.add(error); break;
		}
		
		int line = error.getLine();
		
		List<Error> list = errorMap.get(line);
		if (list == null) {
			list = new ArrayList<Error>();
			errorMap.put(line, list);
		}
		
		list.add(error);
	}
	
	public void add(EObject obj, String message, Object... args) {
		this.addError(new Error(obj, message, args));	
	}
	
	public List<Error> getErrorsForLine(int line) {
		return errorMap.get(line);
	}
	
	public int size() {
		return this.errorList.size();
	}
	
	public List<Error> getErrors() {
		return this.errorList;
	}
	
	public List<Error> getWarnings() {
		return this.warningList;
	}
	
	public int getErrorCount() {
		return this.errorList.size();
	}
	
	public int getWarningCount() {
		return this.warningList.size();
	}
}
