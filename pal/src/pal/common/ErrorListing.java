//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

public class ErrorListing {
	private final File infile;
	private final ErrorReport report;
	
	public ErrorListing(File infile, ErrorReport report) {
		this.infile = infile;
		this.report = report;
	}
	
	public void add(ErrorReport other) {
		for (Error err : other.getErrors()) {
			report.addError(err);
		}
	}
	
	public void write(OutputStream outs, boolean errorsOnly) throws IOException {
		FileInputStream in = new FileInputStream(this.infile);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String lineStr;
		
		PrintStream out = new PrintStream(outs);
		
		int i = 0;
		while ((lineStr = br.readLine()) != null) {
			i++;		
			
			List<Error> errorList = report.getErrorsForLine(i);
			
			if (!errorsOnly) {
				lineStr = lineStr.replace("\t", " ");
				lineStr = lineStr.replace("\r", "");
				lineStr = lineStr.replace("\n", "");
				out.println(String.format("% 4d %s", i, lineStr));
				out.flush();
			}

			if (errorList == null) continue;
			
			String markerLine = "";
			
			for (Error e: errorList) {
				int offset = e.getStartOffset();
				int endOffset = e.getEndOffset();
				
				while (markerLine.length() < offset) {
					markerLine += " ";
				}
				
				while (markerLine.length() <= endOffset) {
					markerLine += "^";
				}
			}
			
			if (!errorsOnly) {
				out.println("     " + markerLine);
			}
			
			for (Error e: errorList) {
				out.println(e);
				out.flush();
			}
		}
	}
	
	public void write(File out, boolean errorsOnly) throws IOException {
		this.write(new FileOutputStream(out), errorsOnly);
	}
	
	public boolean isEmpty() {
		return this.report.getErrors().isEmpty();
	}
	
	public int size() {
		return this.report.getErrors().size();
	}
}
