package pal.common;

public class Addr
{
	public final int offset;
	public final int reg;

	public Addr(int offset, int reg) {
		this.offset = offset;
		this.reg = reg;
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Addr)) return false;
		Addr a = (Addr)other;
		return a.offset == this.offset && a.reg == this.reg;
	}
	
	@Override
	public String toString() {
		return String.format("%d[%d]", this.offset, this.reg);
	}
}
