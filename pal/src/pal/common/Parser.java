//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import pal.common.Error.Level;
import pal.xtext.PALStandaloneSetup;
import pal.xtext.pal.Expr;
import pal.xtext.pal.LitInt;
import pal.xtext.pal.LitReal;
import pal.xtext.pal.LitStr;
import pal.xtext.pal.Program;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.parser.IParser;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;

import com.google.inject.Injector;

public enum Parser {
	Instance;
	
	private final IParser impl;
	
	private Parser() {
		Injector guiceInjector = new PALStandaloneSetup().createInjectorAndDoEMFRegistration();
		this.impl = guiceInjector.getInstance(AbstractAntlrParser.class);
	}
	
	public Result parse(Reader r) {
		IParseResult result = this.impl.parse(r);
		Iterable<INode> errors = result.getSyntaxErrors();
		
		Program root = (Program)result.getRootASTElement();
		
		List<Error> validatorErrors = this.validatorErrors(root);
		
		return new Result(errors, validatorErrors, root);
	}
	
	public Result parse(File file) throws FileNotFoundException {
		Reader r = new InputStreamReader(
			new FileInputStream(file)
		);
		
		return parse(r);
	}
	
	private List<Error> validatorErrors(EObject obj) {
		List<Error> list = new ArrayList<Error>();
		
		if (obj != null) {
			this.validatorErrorsAux(list, obj);
		}
		
		return list;
	}
	
	private EObject validatorErrorsAux(List<Error> list, EObject obj) {
		if (obj == null) return null;
		
		if (obj instanceof LitStr) {
			this.fixString(list, (LitStr)obj);
		} else if (obj instanceof LitReal) {
			this.fixReal(list, (LitReal)obj);
		} else if (obj instanceof LitInt) {
			this.fixInt(list, (LitInt)obj);
		}
		
		if (obj instanceof Expr) {
			boolean exprValid = true;
			
			for (EObject child: obj.eContents()) {
				if (this.validatorErrorsAux(list, child) == null) {
					exprValid = false;
				}
			}
			
			return (exprValid ? obj : null);
		} else {
			for (EObject child: obj.eContents()) {
				this.validatorErrorsAux(list, child);
			}
		}
		
		return obj;
	}
	
	private void fixReal(List<Error> list, LitReal obj) {
		if (obj.getNum() == null) {
			obj.setNum((obj.getB() == null ? "0" : obj.getB()) + "." + (obj.getC() == null ? "0" : obj.getC()));
		}
		
		String num = obj.getNum();
		
		boolean err = false;
		
		try {
			BigDecimal bd = new BigDecimal(num);
			Float f = bd.floatValue();
			
			err = f.isInfinite() || f.isNaN() ||
					(bd.compareTo(BigDecimal.ZERO) != 0 && f == 0.0f);
			
			if (err) {				
				String clampedNum = "0.0";
				
				if (bd.compareTo(new BigDecimal(Float.MAX_VALUE)) > 0) {
					clampedNum = ((Float)Float.MAX_VALUE).toString();
				} 
				if (bd.compareTo(new BigDecimal(Float.MIN_VALUE)) < 0) {
					clampedNum = ((Float)Float.MIN_VALUE).toString();
				}

				list.add(new Error(obj, Level.Error, "'%s' under/overflows when represented as 32 bit float; value will be clamped to '%s'", num, clampedNum));
				
				num = clampedNum;
			} else {
				// NOTE: what BigDecimal accepts as parsable and what Float accepts as parsable do not neccessarily coincide
				try {
					f = Float.parseFloat(num);
				} catch (NumberFormatException nfe) {
					list.add(new Error(obj, Level.Error, "could not parse real number"));
					num = "0.0";
				}
			}
		} catch (NumberFormatException ex) {
			list.add(new Error(obj, Level.Error, "could not parse real number"));
			num = "0.0";
		}
		
		obj.setNum(num);
	}
	
	private void fixInt(List<Error> list, LitInt obj) {
		String num = obj.getNum();
		
		try {
			Integer.parseInt(num);
		} catch (NumberFormatException ex) {
			BigInteger bi = new BigInteger(num);
			String clampedInt = "0";
			
			String maxInt = Integer.MAX_VALUE + "";
			String minInt = -Integer.MAX_VALUE + "";
			if (bi.compareTo(new BigInteger(maxInt)) > 0) {
				clampedInt = maxInt;
			}
			if (bi.compareTo(new BigInteger(minInt)) < 0) {
				clampedInt = minInt;
			}
			
			list.add(new Error(obj, "'%s' under/overflows when represented as a 32 bit int; value will be clamped to '%s'", num, clampedInt));
			num = clampedInt;
		}
		
		obj.setNum(num);
	}
	
	private void fixString(List<Error> list, LitStr obj) {
		String str = obj.getString();
		String cleanStr = "";
		
		int l = str.length();
		
		boolean inEscape = false;
		boolean terminated = false;
		
		for (int i = 1; i < l; ++i) {
			char c = str.charAt(i);
			
			if (inEscape) {
				switch (c) {
				case '\'':
					cleanStr += '\'';
					break;
				case 'n':
					cleanStr += '\n';
					break;
				case 't':
					cleanStr += '\t';
					break;
				case '\\':
					cleanStr += '\\';
					break;
				default:
					cleanStr += ' ';
					list.add(new Error(obj, "invalid escape sequence '\\%c'", c));
					break;
				}
				inEscape = false;
			} else {
				if (c == '\\') {
					inEscape = true;
				} else if (c == '\'') {
					terminated = true;
				} else {
					cleanStr += c;
				}
			}
		}
		
		if (inEscape) {
			if (cleanStr.isEmpty()) {
				cleanStr = " ";
			}
		}
		
		if (!terminated) {
			list.add(new Error(obj, "unterminated string"));
		}
		
		if (cleanStr.isEmpty()) {
			list.add(new Error(obj, "strings may not be empty"));
		}
		
		obj.setString(cleanStr);
	}
	
	public Result parse(String file) throws FileNotFoundException {
		return this.parse(new File(file));
	}
	
	public static class Result {
		public final ErrorReport report;
		public final Program program;
		
		public Result(Iterable<INode> errors, List<Error> validatorErrors, Program program) {
			this.report = new ErrorReport();
			
			for (INode node: errors) {
				if (node.getSyntaxErrorMessage() == null) continue;
				
				Error error = new Error(node);
				this.report.addError(error);
			}
			
			for (Error error: validatorErrors) {
				this.report.addError(error);
			}
			
			this.program = program;
		}
	}
}
