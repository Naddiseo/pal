//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

import pal.common.Addr;

public class StdLib {
	private StdLib() {}

	// TODO: split it up into separate methods
	// TODO: add read/readln methods
	public static void EmitCode(ASCBuilder b) {
		EmitRuntimeErrorOutOfBounds(b);
		EmitRuntimeErrorPredUnderflow(b);
		EmitRuntimeErrorSuccOverflow(b);
		EmitRuntimeErrorLnNegative(b);
		EmitRuntimeErrorLnSubnormal(b);
		EmitRuntimeErrorSqrtNegative(b);
		EmitRuntimeErrorNoNumber(b);
		EmitRuntimeErrorReadAtEOF(b);
		
		EmitRuntimeErrorAddOverflow(b);
		EmitRuntimeErrorSubUnderflow(b);
		EmitRuntimeErrorMulOverflow(b);
		EmitRuntimeErrorDivideByZero(b);
		EmitRuntimeErrorModulusByZero(b);
		
		EmitReadStr(b);
		EmitReadToEOL(b);
		EmitWriteStr(b);
		EmitCompStr(b);
		EmitDeref(b);
		EmitCopyInto(b);

		// MATH
		EmitInvSqrt(b);
		EmitSqrt(b);
		EmitPowi(b);
		EmitExp(b);
		EmitSin(b);
		EmitLn(b);
		EmitAbsI(b);
		EmitAbsR(b);

		b.label("test");
		b.jump("test_end");
		b.jump("test_end");
		b.konst(1);
		b.label("test_end");
	}

	// Runtime errors are only ever printed once, right before
	// the program is aborted. It's not worth saving these messages
	// on the stack.
	private static void EmitInlineMessage(ASCBuilder b, String s) {
		b.comment("\"" + s + "\"");
		for (int i = 0; i < s.length(); ++i) {
			b.konst(s.charAt(i), ((Character)s.charAt(i)).toString());
			b.op("writec");
		}
	}
	
	private static void EmitRuntimeErrorNoNumber(ASCBuilder b) {
		b.label("runtime_error_no_number");
		EmitInlineMessage(b, "Run-time error: no number to read from input.\n");
		b.stop();
	}
	
	private static void EmitRuntimeErrorReadAtEOF(ASCBuilder b) {
		b.label("runtime_error_read_at_eof");
		EmitInlineMessage(b, "Run-time error: unable to read at EOF.\n");
		b.stop();
	}

	private static void EmitRuntimeErrorOutOfBounds(ASCBuilder b) {
		b.label("runtime_error_out_of_bounds");
		EmitInlineMessage(b, "Run-time error: array index out of bounds.\n");
		b.stop();
	}
	
	private static void EmitRuntimeErrorPredUnderflow(ASCBuilder b) {
		b.label("runtime_error_pred_underflow");
		EmitInlineMessage(b, "Run-time error: 'pred' of ordinal out of bounds.\n");
		b.stop();
	}
	
	private static void EmitRuntimeErrorSuccOverflow(ASCBuilder b) {
		b.label("runtime_error_succ_overflow");
		EmitInlineMessage(b, "Run-time error: 'succ' of ordinal out of bounds.\n");
		b.stop();
	}
	
	private static void EmitRuntimeErrorAddOverflow(ASCBuilder b) {
		b.label("rt_err_add_overflow");
		EmitInlineMessage(b, "Run-time error: integer overflow on addition.\n");
		b.stop();
	}
	
	private static void EmitRuntimeErrorSubUnderflow(ASCBuilder b) {
		b.label("rt_err_sub_overflow");
		EmitInlineMessage(b, "Run-time error: integer overflow on subtraction.\n");
		b.stop();
	}
	
	private static void EmitRuntimeErrorMulOverflow(ASCBuilder b) {
		b.label("rt_err_mul_overflow");
		EmitInlineMessage(b, "Run-time error: integer overflow on multiplication.\n");
		b.stop();
	}
	
	private static void EmitRuntimeErrorDivideByZero(ASCBuilder b) {
		b.label("rt_err_div_by_zero");
		EmitInlineMessage(b, "Run-time error: divide by zero.\n");
		b.stop();
	}
	
	private static void EmitRuntimeErrorModulusByZero(ASCBuilder b) {
		b.label("rt_err_mod_by_zero");
		EmitInlineMessage(b, "Run-time error: modulus by zero.\n");
		b.stop();
	}
	
	private static void EmitRuntimeErrorLnNegative(ASCBuilder b) {
		b.label("runtime_error_ln_negative");
		EmitInlineMessage(b, "Run-time error: non-positive argument given to 'ln'.\n");
		b.stop();
	}
	
	private static void EmitRuntimeErrorLnSubnormal(ASCBuilder b) {
		b.label("rt_err_ln_subnormal");
		EmitInlineMessage(b, "Run-time error: subnormal (< 10^38) argument given to 'ln'.\n");
		b.stop();
	}
	
	private static void EmitRuntimeErrorSqrtNegative(ASCBuilder b) {
		b.label("runtime_error_sqrt_negative");
		EmitInlineMessage(b, "Run-time error: negative argument given to 'sqrt'.\n");
		b.stop();
	}
	
	// NOTE: does not use the standard calling convention
	private static void EmitWriteStr(ASCBuilder b) {
		Addr ptr = A(0, 0);
		Addr len = A(-3, 0);

		b.label("std_writestr");

		b.adjust(1);
		b.pusha(len);
		b.push(len);
		b.op("subi");
		b.pop(ptr);

		b.push(ptr);
		b.push(len);
		b.op("addi");
		b.pop(len);

		b.label("std_writestr_loop");

		b.push(ptr);
		b.push(len);
		b.op("lti");
		b.ifz("std_writestr_end");

		b.push(ptr);
		b.pushi();
		b.op("writec");

		b.push(ptr);
		b.konst(1);
		b.op("addi");
		b.pop(ptr);

		b.jump("std_writestr_loop");

		b.label("std_writestr_end");
		b.adjust(-1);
		b.ret(0);

	}
	
	private static void EmitReadToEOL(ASCBuilder b) {
		b.label("std_read2eol");
		b.op("readc");
		b.iferr("std_read2eol_end");
		b.konst(10);
		b.op("eqi");
		b.ifnz("std_read2eol_end");
		b.jump("std_read2eol");
		b.label("std_read2eol_end");
		b.ret(0);
	}
	
	private static void EmitReadStr(ASCBuilder b) {
		// TODO: implement
		Addr ptr = A(0,0);	
		Addr size = A(1,0);
		Addr i = A(2,0);
		
		b.label("std_readstr");
		b.adjust(3);
		
		b.push(A(-3,0));
		b.pop(size);

		b.push(A(-4,0));
		b.pop(ptr);
		
		b.konst(0);
		b.pop(i);
		
		b.label("std_readstr_loop");
		b.push(size);
		b.push(i);
		b.op("eqi");
		b.ifnz("std_readstr_end");
		
		b.push(ptr);
		b.push(i);
		b.op("addi");
		b.op("readc");
		b.iferr("runtime_error_read_at_eof");
		b.popi();
		
		b.push(i);
		b.konst(1);
		b.op("addi");
		b.pop(i);
		
		b.jump("std_readstr_loop");
		
		b.label("std_readstr_end");
		b.adjust(-3);
		b.ret(0);
	}
	
	// Calling convention:
	// [ str1 1..n ][ str2 1..2 ][n]
	// [r] - return value overwrites first char of bottom string
	private static void EmitCompStr(ASCBuilder b) {
		Addr i = A(0,0);
		Addr str1 = A(1,0);
		Addr str2 = A(2,0);
		Addr result = A(3,0);
		
		Addr n = A(-3,0);
		
		b.label("std_compstr");
		b.adjust(4);
		
		b.konst(0);
		b.pop(i);
		
		b.pusha(n);
		b.push(n);
		b.op("subi");
		b.pop(str2);
		
		b.push(str2);
		b.push(n);
		b.op("subi");
		b.pop(str1);
		
		b.konst(0); // assume that the strings are equal
		b.pop(result);
		
		b.label("std_compstr_loop");
		b.push(i);
		b.push(n);
		b.op("lti");
		b.ifz("std_compstr_end");
		
		b.push(str1);
		b.push(i);
		b.op("addi");
		b.pushi();	// get str[i];
		
		b.push(str2);
		b.push(i);
		b.op("addi");
		b.pushi();	// get str[i];
		
		b.op("lti");
		b.ifnz("std_compstr_lti");
		
		b.push(str1);
		b.push(i);
		b.op("addi");
		b.pushi();	// get str[i];
		
		b.push(str2);
		b.push(i);
		b.op("addi");
		b.pushi();	// get str[i];
		
		b.op("gti");
		b.ifnz("std_compstr_gti");
		
		b.push(i);
		b.konst(1);
		b.op("addi");
		b.pop(i);
		b.jump("std_compstr_loop");

		b.label("std_compstr_lti");
		b.konst(-1);
		b.pop(result);
		b.jump("std_compstr_end");
		
		b.label("std_compstr_gti");
		b.konst(1);
		b.pop(result);
		b.jump("std_compstr_end");
		
		b.label("std_compstr_end");
		
		b.push(str1);
		b.push(result);
		b.popi();
		
		b.adjust(-4);
		b.ret(0);
	}
	
	// Calling convention:
	// [addr][space 1 .. n - 2][n], n >= 2
	// [dereferenced data at addr] <= replaces everything
	private static void EmitDeref(ASCBuilder b) {
		final int T = 3;
		
		b.label("std_deref");
		b.adjust(T);
		
		Addr n = new Addr(-3, 0);
		Addr src = new Addr(0, 0);
		Addr dest = new Addr(1, 0);
		Addr end = new Addr(2, 0);

		// dest = &n - (n - 1)
		b.pusha(n);
		b.push(n);
		b.konst(1);
		b.op("subi");
		b.op("subi");
		b.pop(dest);
		
		// src = *dest;
		b.push(dest);
		b.pushi();
		b.pop(src);
		
		// end = &n + 1;
		b.pusha(n);
		b.konst(1);
		b.op("addi");
		b.pop(end);
		
		b.label("std_deref_loop");
		
		// *dest = *src
		b.push(dest);
		b.push(src);
		b.pushi();
		b.popi();
		
		// dest++;
		b.push(dest);
		b.konst(1);
		b.op("addi");
		b.pop(dest);
		
		// src++;
		b.push(src);
		b.konst(1);
		b.op("addi");
		b.pop(src);
		
		// while (dest < end);
		b.push(dest);
		b.push(end);
		b.op("lti");
		b.ifnz("std_deref_loop");
		
		b.label("std_deref_end");
		b.adjust(-T);
		b.ret(0);
	}
	
	// Calling convention:
	// [addr][data 1 .. n][n]
	// no return
	private static void EmitCopyInto(ASCBuilder b) {
		final int T = 2;
		
		b.label("std_copyinto");
		b.adjust(T);
		
		Addr n = new Addr(-3, 0);
		Addr srcAddr = new Addr(0, 0);
		Addr destAddr = new Addr(1, 0);
		
		b.pusha(n);
		b.push(n);
		b.op("subi");
		b.dup();
		b.pop(srcAddr);
		b.konst(1);
		b.op("subi");
		b.pushi();
		b.pop(destAddr);
		
		b.label("std_copyinto_loop");
		
		// *dest = *src;
		b.push(destAddr);
		b.push(srcAddr);
		b.pushi();
		b.popi();
		
		// dest++;
		b.push(destAddr);
		b.konst(1);
		b.op("addi");
		b.pop(destAddr);
		
		// src++;
		b.push(srcAddr);
		b.konst(1);
		b.op("addi");
		b.pop(srcAddr);
		
		// while (src < end);
		b.push(srcAddr);
		b.pusha(n);
		b.op("lti");
		b.ifnz("std_copyinto_loop");
		
		b.label("std_copyinto_end");
		b.adjust(-T);
		b.ret(0);
	}
	
	private static void EmitInvSqrt(ASCBuilder b) {
		Addr arg = A(0, 0);
		Addr local = A(1, 0);
		Addr ret = A(-3, 0);

		b.label("std_invsqrt");
		b.adjust(2);

		b.push(arg);
		b.pop(local);

		b.push(arg);
		b.konst(0.5);
		b.op("mulr");
		b.pop(local);

		b.konst(1597463007); // magic number
		b.push(arg);
		b.konst(2);
		b.op("divi");
		b.op("subi");

		for (int i = 0; i < 3; i++) {
			b.push(local);
			b.konst(1.5);
			b.push(local);
			b.push(arg);
			b.push(arg);
			b.op("mulr");
			b.op("mulr");
			b.op("subr");
			b.op("mulr");
			b.pop(arg);
		}

		b.push(arg);
		b.pop(ret);

		b.adjust(-2);
		b.ret(0);
	}

	private static void EmitSqrt(ASCBuilder b) {
		Addr local = A(0, 0);
		Addr copy = A(1, 0);
		
		// Same
		Addr ret = A(-3, 0);
		Addr arg = A(-3, 0);

		b.label("std_sqrt");
		b.adjust(2);

		b.push(arg);
		b.konst(0.0f);
		b.lt(TNative.Real);
		b.ifnz("runtime_error_sqrt_negative");

		b.push(arg);
		b.pop(local);
		b.push(arg);
		b.pop(copy);		
		
		b.push(local);
		b.konst(0.5);
		b.op("mulr");
		b.pop(local);

		b.konst(1597463007);
		b.push(arg);
		b.konst(2);
		b.op("divi");
		b.op("subi");
		b.pop(arg);

		for (int i = 0; i < 2; i++) {
			b.push(arg);
			b.konst(1.5);
			b.push(local);
			b.push(arg);
			b.push(arg);
			b.op("mulr");
			b.op("mulr");
			b.op("subr");
			b.op("mulr");
			b.pop(arg);
		}

		b.push(arg);
		b.push(copy);
		b.op("mulr");
		b.pop(ret);

		b.adjust(-2);
		b.ret(0);
	}

	private static void EmitSin(ASCBuilder b) {
		Addr result = A(0, 0);
		Addr i = A(1, 0);
		Addr sign = A(2, 0);
		Addr d = A(3,0);
		
		Addr ret = A(-3, 0);

		b.label("std_sin");
		b.adjust(4);

		b.label("std_sin_ub");
		b.push(ret);
		b.konst(2*Math.PI + 0.0001);
		b.op("ltr");
		b.ifnz("std_sin_lb");
		b.push(ret);
		b.konst(2*Math.PI);
		b.op("subr");
		b.pop(ret);
		b.jump("std_sin_ub");
		
		b.label("std_sin_lb");
		b.push(ret);
		b.konst(-2*Math.PI - 0.0001);
		b.op("gtr");
		b.ifnz("std_sin_begin");
		b.push(ret);
		b.konst(2*Math.PI);
		b.op("addr");
		b.pop(ret);
		b.jump("std_sin_lb");
		
		b.label("std_sin_begin");
		b.push(ret);
		b.pop(result);
		b.konst(3);
		b.pop(i);
		b.konst(-1.0);
		b.pop(sign);

		b.label("std_sin_loop");
		b.konst(30); // max number of iterations
		b.push(i);
		b.lt(TNative.Int);
		b.ifnz("std_sin_end");

		b.push(ret);
		b.push(i);
		b.call(0, "std_powi");
		b.adjust(-1);

		b.push(i);
		b.pop(d);
		
		b.label("std_sin_factl");
		b.push(d);
		b.ifz("std_sin_facte");
		b.push(d);
		b.op("itor");
		b.op("divr");
		b.push(d);
		b.konst(1);
		b.op("subi");
		b.pop(d);
		b.jump("std_sin_factl");
		
		b.label("std_sin_facte");
		b.push(sign);
		b.op("mulr");
		b.push(result);
		b.op("addr");
		b.pop(result); // result = result + sign*x^n/n!

		b.push(i);
		b.konst(2);
		b.op("addi");
		b.pop(i); // i = i + 2

		b.push(sign);
		b.konst(-1.0);
		b.op("mulr");
		b.pop(sign); // sign = -1.0*sign

		b.jump("std_sin_loop");

		b.label("std_sin_end");
		b.push(result);
		b.pop(ret);
		b.adjust(-4);
		b.ret(0);
	}

	private static void EmitExp(ASCBuilder b) {
		Addr result = A(0, 0);
		Addr i = A(1, 0);
		Addr d = A(2, 0);
		Addr ret = A(-3, 0);

		b.label("std_exp");
		b.adjust(3);

		b.konst(1.0);
		b.pop(result);

		b.push(ret);
		b.push(result);
		b.op("addr");
		b.pop(result);

		b.konst(2);
		b.pop(i);

		b.label("std_exp_loop");
		b.konst(20); // number of iterations
		b.push(i);
		b.eq(TNative.Int);
		b.ifnz("std_exp_end");

		b.push(ret);
		b.push(i);
		b.call(0, "std_powi");
		b.adjust(-1);
		
		b.push(i);
		b.pop(d);
		
		b.label("std_exp_factl");
		b.push(d);
		b.ifz("std_exp_facte");
		b.push(d);
		b.op("itor");
		b.op("divr");
		b.push(d);
		b.konst(1);
		b.op("subi");
		b.pop(d);
		b.jump("std_exp_factl");
		
		b.label("std_exp_facte");

		b.push(result);
		b.op("addr");
		b.pop(result);

		b.push(i);
		b.konst(1);
		b.op("addi");
		b.pop(i);

		b.jump("std_exp_loop");

		b.label("std_exp_end");
		b.push(result);
		b.pop(ret);
		b.adjust(-3);
		b.ret(0);
	}

	private static void EmitPowi(ASCBuilder b) {
		Addr result = A(-4, 0);
		Addr x = A(0, 0);
		Addr n = A(-3, 0);

		b.label("std_powi");
		b.adjust(1);

		b.push(result);
		b.pop(x);
		b.push(n);
		b.ifnz("std_powi_loop");

		b.konst(1.0);
		b.jump("std_powi_end");

		b.label("std_powi_loop");
		b.push(n);
		b.konst(1);
		b.op("subi");
		b.pop(n);
		
		b.push(n);
		b.ifz("std_powi_end");
		
		b.push(result);
		b.push(x);
		b.op("mulr");
		b.pop(result);
		b.jump("std_powi_loop");

		b.label("std_powi_end");
		b.adjust(-1);
		b.ret(0);
	}
	
	private static void EmitLn(ASCBuilder b) {
		Addr arg = A(-3, 0);
		
		b.label("std_ln");
		
		b.push(arg);
		b.konst(0.0f);
		b.gt(TNative.Real);
		b.ifz("runtime_error_ln_negative");
		
		final int T = 4;
		b.adjust(T);
		
		Addr t0 = A(0, 0);
		Addr t1 = A(1, 0);
		Addr t2 = A(2, 0);
		Addr t3 = A(3, 0);
		
		final int MaxFrac = 1 << 23;
		
		// Compute the raw fraction
		b.push(arg);
		b.konst(MaxFrac);
		b.op("mod");
		b.pop(t0);
		
		// Compute the exponent
		b.push(arg);
		b.konst(MaxFrac);
		b.op("divi");
		b.dup();
		b.pop(t1);
		
		b.ifz("rt_err_ln_subnormal");
		
		// Do the -127 adjustment to exponent
		b.push(t1);
		b.konst(127);
		b.op("subi");
		b.pop(t1);
		
		// convert t0 into a float
		b.push(t0);
		b.konst(127 << 23);
		b.op("addi");
		b.pop(t0);
		
		// todo: convert t0 into a float
		
		// Decompose real into a * 2^p, 1 <= a < 2
		// $ret = p ln 2 - T[a]
		// T[x] = sum (1 - x)^i / i
		
		// $ret = p ln 2
		b.konst((float)Math.log(2));
		b.push(t1);
		b.op("itor");
		b.op("mulr");
		b.pop(arg);
		
		// t3 = 1 - x
		// t1 = (1 - x)^i
		b.konst(1.0f);
		b.push(t0);
		b.op("subr");
		b.dup();
		b.pop(t3);
		b.pop(t1);
		
		// t2 = i = 1
		b.konst(1.0f);
		b.pop(t2);
		
		b.label("std_ln_loop");
		
		// $ret -= (1 - x)^i / i
		b.push(arg);
		b.push(t1);
		b.push(t2);
		b.op("divr");
		b.op("subr");
		b.pop(arg);

		// t1 *= t3
		b.push(t1);
		b.push(t3);
		b.op("mulr");
		b.pop(t1);
		
		// i += 1
		b.push(t2);
		b.konst(1.0f);
		b.op("addr");
		b.dup();
		b.pop(t2);
		b.konst(12.0f);
		b.op("ltr");
		b.ifnz("std_ln_loop");
		
		b.label("std_ln_end");
		b.adjust(-T);
		b.ret(0);
	}
	
	private static void EmitAbsI(ASCBuilder b) {
		Addr arg = A(-3, 0);
		
		b.label("std_absi");
		
		b.push(arg);
		b.konst(0);
		b.lt(TNative.Int);
		b.ifz("std_absi_end");
		
		b.pusha(arg);
		b.konst(0);
		b.push(arg);
		b.op("subi");
		b.popi();
		
		b.label("std_absi_end");
		b.ret(0);
	}
	
	private static void EmitAbsR(ASCBuilder b) {
		Addr arg = A(-3, 0);
		
		b.label("std_absr");
		
		b.push(arg);
		b.konst(0.0f);
		b.lt(TNative.Real);
		b.ifz("std_absr_end");
		
		b.pusha(arg);
		b.konst(0.0f);
		b.push(arg);
		b.op("subr");
		b.popi();
		
		b.label("std_absr_end");
		b.ret(0);
	}
	
	private static Addr A(int offset, int reg) {
		return new Addr(offset, reg);
	}
}
