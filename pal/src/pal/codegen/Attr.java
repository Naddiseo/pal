//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

/*
 * Some semantics of this class (for later use):
 * 1) type != Void (ever)
 * 2) value != null => type != Bottom
 * 
 */

public class Attr {
	public final DType type;
	public final boolean isRef;
	public final Object value;
	
	public Attr(DType type, boolean isRef) {
		this.type = type;
		this.isRef = isRef;
		this.value = null;
	}
	
	public Attr(DType type) {
		this.type = type;
		this.isRef = false;
		this.value = null;
	}
	
	public Attr(float data) {
		this.value = data;
		this.isRef = false;
		this.type = TNative.Real;
	}
	
	public Attr(int data) {
		this.value = data;
		this.isRef = false;
		this.type = TNative.Int;
	}
	
	public Attr(char data) {
		this.value = (int)data;
		this.isRef = false;
		this.type = TNative.Char;
	}
	
	public Attr(String data) {
		this.isRef = false;
		
		if (data.length() == 1) {
			this.value = (int)data.charAt(0);
			this.type = TNative.Char;
		} else {
			this.value = data;
			this.type = TArray.String(data.length());
		}
	}
	
	public Attr(boolean data) {
		this.type = TNative.Bool;
		this.isRef = false;
		this.value = (data ? 1 : 0);
	}
	
	public Attr(TEnum type, int data) {
		this.type = type;
		this.isRef = false;
		this.value = data;
	}
	
	public boolean isNumber() {
		return this.type.isNumber();
	}
	
	public boolean isBottom() {
		return this.type.isBottom();
	}
	
	public int vint() {
		return (Integer)this.value;
	}
	
	public float vreal() {
		if (this.value instanceof Integer) {
			return (float)(Integer)this.value;
		} else {
			return (Float)this.value;
		}
	}
	
	public boolean vbool() {
		return (Integer)this.value != 0;
	}
	
	public String vstring() {
		return (String)this.value;
	}
	
	public Attr deref() {
		if (!this.isRef) return this;
		return new Attr(this.type, false, this.value);
	}
	
	public Attr(DType type, boolean isRef, Object value) {
		this.type = type;
		this.isRef = isRef;
		this.value = value;
	}
	
	public final static Attr Int = new Attr(TNative.Int);
	public final static Attr Char = new Attr(TNative.Char);
	public final static Attr Void = new Attr(TNative.Void);
	public final static Attr Real = new Attr(TNative.Real);
	public final static Attr Bottom = new Attr(TNative.Bottom);
	public final static Attr Bool = new Attr(TNative.Bool);
}
