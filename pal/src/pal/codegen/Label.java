//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

public class Label {
	private Label() {}
	
	public final static String Begin = "prog_begin";
	public final static String End = "prog_end";
	
	public static String ProcBegin(DProc proc) {
		return String.format("proc_%s_begin", proc.id);
	}
	
	public static String ProcEnd(DProc proc) {
		return String.format("proc_%s_end", proc.id);
	}
	
	public static String WhileTop(String base) {
		return String.format(base, "top");
	}
	
	public static String WhileBottom(String base) {
		return String.format(base, "bottom");
	}
	
	public static String IfElse(String base) {
		return String.format(base, "else");
	}
	
	public static String IfEnd(String base) {
		return String.format(base, "end");
	}
}
