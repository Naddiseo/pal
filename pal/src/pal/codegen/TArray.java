//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

public class TArray extends DType
{
	public final DType valueType;
	public final DType indexType;
	public final int min;
	public final int max;
	
	public static TArray String(int length) {
		return new TArray(null, TNative.Char, TNative.Int, 1, length);
	}
	
	public TArray(String name, DType valueType, DType indexType, int min, int max) {
		super(name == null ? String.format("array[%d .. %d] of %s", min, max, valueType.name) : name);
		
		this.valueType = valueType;
		this.indexType = indexType;
		this.min = min;
		this.max = max;
	}

	@Override
	public int size() {
		return (this.max - this.min + 1) * this.valueType.size(); 
	}
	
	public boolean isString() {
		return valueType == TNative.Char && min == 1 && indexType == TNative.Int;
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == this) return true;
		if (!(other instanceof TArray)) return false;
		
		TArray arr = (TArray)other;
		if (!this.isString()) return false;		
		if (!arr.isString()) return false;
		
		return this.max == arr.max;
	}
}
