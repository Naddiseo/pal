package pal.codegen;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

// For storing things that might not get output because
// of constant folding
// Example: (1 + 2) + 4
// 1 2 <clear> 3 4 <clear> 7
//
// It is implemented hackily but it works.
public class TempASCBuilder extends ASCBuilderBase
{
	public final ASCBuilder dest;
	
	private final Stack<List<Object[]>> temp;
	
	public TempASCBuilder(ASCBuilder dest) {
		this.dest = dest;
		this.temp = new Stack<List<Object[]>>();
	}
	
	public void push() {
		this.temp.push(new ArrayList<Object[]>());
	}
	
	public void clear() {
		if (temp.isEmpty()) return;
		this.temp.pop();
	}
	
	public void flush() {
		if (temp.isEmpty()) return;
		
		List<Object[]> top = this.temp.pop();
		
		for (Object[] obj: top) {
			this.output(obj);
		}
	}
	
	private void output(Object[] obj) {
		if (this.temp.isEmpty()) {
			if (obj[0] == INST) {
				this.dest.inst((String)obj[1], (Object[])obj[2]);
			} else if (obj[0] == LBL) {
				this.dest.label((String)obj[1]);
			}
		} else {
			this.temp.peek().add(obj);
		}
	}
	
	@Override
	public void label(String label) {
		Object[] obj = new Object[] { LBL, label };
		this.output(obj);
	}

	@Override
	public void inst(String inst, Object... args) {
		Object[] obj = new Object[] { INST, inst, args };
		this.output(obj);
	}
	
	@Override
	public void comment(String str) {
		this.dest.comment(str);
	}
	
	private final static Integer LBL = 1;
	private final static Integer INST = 2;
}
