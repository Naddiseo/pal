//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

public class DError extends Declared {
	public DError(String name) {
		super(name);
	}
	
	@Override
	public String kind() {
		return "error";
	}
}
