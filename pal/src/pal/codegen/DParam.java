//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

public class DParam extends Declared {
	public final DType type;
	public final int display;
	public int offset;
	public final boolean isRef;
	
	public DParam(String name, DType type, int display, int offset, boolean isRef) {
		super(name);
		
		this.type = type;
		this.display = display;
		this.offset = offset;
		this.isRef = isRef;
	}

	
	@Override
	public String kind() {
		return "param";
	}
}
