//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import pal.builtin.CompOps;
import pal.builtin.DBuiltIn;
import pal.builtin.LogicOps;
import pal.builtin.MathOps;

public class Operator {
	private static Map<String, DBuiltIn> UnaryOps = null;
	private static Map<String, DBuiltIn> BinaryOps = null;
	
	private Operator() {}
	
	public static DBuiltIn Unary(String name) {
		return Operator.UnaryOps.get(name);
	}
	
	public static DBuiltIn Binary(String name) {
		return Operator.BinaryOps.get(name);
	}
	
	static {
		List<DBuiltIn> unaryList = Arrays.asList(
			LogicOps.Not, MathOps.Neg, MathOps.Pos
		);
		
		List<DBuiltIn> binaryList = Arrays.asList(
			LogicOps.And, LogicOps.Or,
			MathOps.Add, MathOps.Sub, MathOps.Mult, MathOps.DivI, MathOps.DivR, MathOps.Mod,
			CompOps.Equal, CompOps.NotEqual, CompOps.LessEqual, CompOps.GreaterEqual, CompOps.LessThan, CompOps.GreaterThan
		);
		
		UnaryOps = new HashMap<String, DBuiltIn>();
		BinaryOps = new HashMap<String, DBuiltIn>();
		
		for (DBuiltIn op: unaryList) {
			UnaryOps.put(op.name, op);
		}
		
		for (DBuiltIn op: binaryList) {
			BinaryOps.put(op.name, op);
		}
	}
}
