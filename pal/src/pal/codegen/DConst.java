//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

public class DConst extends Declared {
	public final DType type;
	public final Object value;
	
	public DConst(String name, DType type, Object value) {
		super(name);
		
		this.type = type;
		this.value = value;
	}
	
	@Override
	public String kind() {
		return "const";
	}
}
