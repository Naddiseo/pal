//
// Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac
// CMPUT 415, Fall 2011
//
package pal.codegen;

//
// An ASCBuilder that doesn't do anything with the code.
// For testing.
//
public class NullASCBuilder extends ASCBuilderBase {
	public final static ASCBuilder Instance = new NullASCBuilder();

	private NullASCBuilder() {
	}

	@Override
	public void label(String label) {
		// Do nothing.
	}

	@Override
	public void inst(String inst, Object... args) {
		// Do nothing.
	}
	
	@Override
	public void comment(String c) {
	}
}
