//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

import pal.common.Addr;

public interface ASCBuilder {
	//
	// STACK INSTRUCTIONS
	//
	public void push(Addr addr);

	public void push(int offset, int reg);

	public void pushi(int reg);

	public void pushi();

	public void pusha(Addr addr);

	public void pop(Addr addr);

	public void popi(int reg);

	public void popi();

	public void dup();

	public void adjust(int n);

	public void konst(Object value);

	public void konst(Object value, String comment);

	//
	// HEAP INSTRUCTIONS
	//
	public void alloc(int n);

	public void free();

	public void salloc();

	//
	// FLOW CONTROL INSTRUCTIONS
	//
	public void ifz(String label);

	public void ifnz(String label);

	public void iferr(String label);

	public void jump(String label);

	public void call(int reg, String label);

	public void ret(int reg);

	public void stop();

	public void label(String label);

	public void op(String op);

	public void comment(String c);

	//
	// COMPARISON OPERATORS

	public void lt(DType type);

	public void le(DType type);

	public void gt(DType type);

	public void ge(DType type);

	public void eq(DType type);

	public void ne(DType type);
	
	// Moved here from AscBuilderBase
	
	public void inst(String inst, Object... args);
}
