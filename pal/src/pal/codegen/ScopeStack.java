//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.apache.log4j.Logger;

import pal.bins.Tester;
import pal.common.ErrorReport;
import pal.xtext.pal.NameRef;

public class ScopeStack {
	static final Logger logger = Logger.getLogger(Tester.class.getName());
	
	private Scope topScope;
	private final ErrorReport errors;
	public final Stack<String> loops;

	public ScopeStack(ErrorReport errors) {
		this.topScope = new Scope(errors, null);
		this.errors = errors;
		this.loops = new Stack<String>();
	}

	public Declared get(NameRef name) {
		if (name == null) {
			return new DError("<error>");
		}
		
		Declared ret = this.topScope.get(name);
		
		if (ret == null) {
			this.errors.add(name, "'%s' is not defined in this scope", name.getName());
			ret = new DError(name.getName());
			this.topScope.declare(name, ret);
		}
		
		return ret;
	}
	
	public DType getType(NameRef n) {
		Declared decl = this.get(n);
		
		if (decl instanceof DError) {
			return TNative.Bottom;
		}
		
		if (decl instanceof DType) {
			return (DType)decl;
		} else {
			this.errors.add(n, "'%s' is a %s, not a type", n.getName(), decl.kind());
			return TNative.Bottom;
		}
	}

	public DProc getCurrentProc() {
		return this.topScope.proc;
	}
	
	public int display() {
		return this.topScope.display;
	}
	
	public void declare(NameRef name, Declared item) {
		this.topScope.declare(name, item);
	}
	
	public void addBuiltIn(String name, Declared item) {
		this.topScope.addBuiltIn(name, item);
	}
	
	public void push(DProc proc) {
		Scope newTop = new Scope(this.topScope, proc);
		this.topScope = newTop;
	}

	public void pop() {
		this.topScope = this.topScope.parent;
	}
	
	public int getCurrentVarOffset(DType type) {
		int offset = this.topScope.offset;
		this.topScope.offset += type.size();
		return offset;
	}
	
	private static class Scope {
		private final Scope parent;
		private final Map<String, Declared> items = new HashMap<String, Declared>();
		private final ErrorReport errors;
		
		public final DProc proc;
		public final int display;
		public int offset = 0;
		
		public Scope(ErrorReport errors, DProc proc) {
			this.errors = errors;
			this.parent = null;
			this.display = -1;
			this.proc = proc;
		}
		
		public Scope(Scope parent, DProc proc) {
			this.errors = parent.errors;
			this.parent = parent;
			this.display = parent.display + 1;
			this.proc = proc;
		}

		public Declared get(NameRef name) {
			if (name == null) {
				return null;
			}
			
			if (this.items.containsKey(name.getName())) {
				return this.items.get(name.getName());
			} else if (this.parent != null) {
				Declared decl = this.parent.get(name);
				
				if (decl != null) {
					this.items.put(name.getName(), decl);
				}
				
				return decl;
			} else {
				return null;
			}
		}
		
		public void addBuiltIn(String name, Declared decl) {
			this.items.put(name, decl);
		}
		
		public void declare(NameRef name, Declared decl) {
			if (this.items.containsKey(name.getName())) {
				Declared d = this.items.get(name.getName());
				this.errors.add(name, "'%s' is already declared in this scope as a %s", name.getName(), d.kind());
				return;
			}

			this.items.put(name.getName(), decl);
		}
	}
}