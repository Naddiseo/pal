//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

// Used for A, B, C in "program A(B, C);"
public class DJunk extends Declared {
	private final String kind;
	
	public DJunk(String name, String kind) {
		super(name);
		this.kind = kind;
	}

	@Override
	public String kind() {
		return this.kind;
	}
}
