//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

public abstract class DType extends Declared {
	public DType(String name) {
		super(name);
	}
	
	public abstract int size();
	
	public boolean isNumber() {
		return this == TNative.Int || this == TNative.Real || this == TNative.Bottom;
	}
	
	public boolean isInt() {
		return this == TNative.Int || this == TNative.Bottom;
	}
	
	public boolean isReal() {
		return this == TNative.Real || this == TNative.Bottom;
	}
	
	public boolean isArray() {
		return this instanceof TArray || this == TNative.Bottom;
	}
	
	public boolean isRec() {
		return this instanceof TRec || this == TNative.Bottom;
	}
	
	public boolean isChar() {
		return this == TNative.Char || this == TNative.Bottom;
	}
	
	public boolean isBool() {
		return this == TNative.Bool || this == TNative.Bottom;
	}
	
	public boolean isNative() {
		return this == TNative.Int || this == TNative.Real || this == TNative.Char || this == TNative.Bool || this == TNative.Bottom;
	}
	
	public boolean isString() {
		if (this instanceof TArray) {
			return ((TArray)this).isString();
		} else {
			return this == TNative.Bottom;
		}
	}
	
	public boolean isBottom() {
		return this == TNative.Bottom;
	}
	
	// Checks if they're compatible without a cast.
	// Important: returns FALSE for int and real.
	public static boolean AreCompatible(DType src, DType dest) {
		if (src == dest) return true;
		if (dest == TNative.Bottom) return true;
		if (src == TNative.Bottom) return true; 
		
		if (src instanceof TArray && dest instanceof TArray) {
			TArray asrc = (TArray)src;
			TArray adest = (TArray)dest;
			
			if (asrc.isString() && adest.isString()) {
				return asrc.max == adest.max;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public String kind() {
		return "type";
	}
}
