//
// Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac
// CMPUT 415, Fall 2011
//
package pal.codegen;

import java.util.Arrays;
import java.util.List;

import pal.builtin.DBuiltIn;
import pal.codegen.Compiler.EvalContext;
import pal.common.Addr;
import pal.common.ErrorReport;
import pal.xtext.pal.Expr;
import pal.xtext.pal.ExprBin;
import pal.xtext.pal.ExprBracket;
import pal.xtext.pal.ExprCall;
import pal.xtext.pal.ExprIndex;
import pal.xtext.pal.ExprProp;
import pal.xtext.pal.ExprUnary;
import pal.xtext.pal.LitInt;
import pal.xtext.pal.LitReal;
import pal.xtext.pal.LitStr;
import pal.xtext.pal.NameRef;

public class CompilerExpr {
	public final ErrorReport errors;
	private final ScopeStack scope;
	private final boolean arraySubscriptChecking;

	public final TempASCBuilder builder;
	
	public CompilerExpr(ASCBuilder builder, ErrorReport  errors, ScopeStack scope, boolean arraySubscriptChecking) {
		this.errors = errors;
		this.builder = new TempASCBuilder(builder);
		this.scope = scope;
		this.arraySubscriptChecking = arraySubscriptChecking;
	}
	
	private Attr handleExprBin(ExprBin e, EvalContext context) {
		DBuiltIn op = Operator.Binary(e.getOp());
		Attr retAttr = op.compile(this, e, Arrays.asList(e.getLeft(), e.getRight()));
 		return retAttr;
	}
	
	public Attr handleArrayIndex(Attr attr, Expr obj, List<Expr> indexList) {
		DType currentType = attr.type;
		
		for (Expr expr: indexList) {
			DType valueType;
			DType indexType;
			int min;
			int max;
			boolean isArray = (currentType instanceof TArray);
			
			if (isArray) {
				TArray arr = (TArray)currentType;
				valueType = arr.valueType;
				indexType = arr.indexType;
				min = arr.min;
				max = arr.max;
			} else {
				valueType = TNative.Bottom;
				indexType = TNative.Bottom;
				min = 0;
				max = 0;
				
				if (currentType != TNative.Bottom) {
					this.errors.add(obj, "value being indexed is not an array");
				}
			}
			
			Attr indexAttr = this.compile(expr, EvalContext.RVal);
			
			if (DType.AreCompatible(indexAttr.type, indexType)) {
				if (indexType != TNative.Bottom && indexAttr.type != TNative.Bottom) {
					if (indexAttr.value != null) {
						int idx = indexAttr.vint();
						
						if (idx < min || idx > max) {
							this.errors.add(expr, "array subscript out of range");
						}
					}

					int valueSize = valueType.size();
					
					if (this.arraySubscriptChecking) {
						// if index < min jump to runtime_error_out_of_bounds
						builder.dup();
						builder.konst(min);
						builder.op("lti");
						builder.ifnz("runtime_error_out_of_bounds");

						// if index > max jump to runtime_error_out_of_bounds
						builder.dup();
						builder.konst(max);
						builder.op("gti");
						builder.ifnz("runtime_error_out_of_bounds");
					}
					
					// subtract min from the index b/c we need to start our offset at 0
					builder.konst(min);
					builder.op("subi");

					// multiply by the size of the values stored in the array
					builder.konst(valueSize);
					builder.op("muli");
					
					// add the offset to the current base address
					builder.op("addi");
				}
			} else {
				this.errors.add(expr, "array subscript must be of type '%s', given '%s'", indexType.name, indexAttr.type.name);
			}
			
			attr = (valueType == TNative.Bottom ? Attr.Bottom : new Attr(valueType, true));
			currentType = valueType;
			obj = expr;
		}
		
		return attr;
	}

	private Attr handleExprCall(ExprCall e, EvalContext context) {
		NameRef name = e.getProcName();

		Declared decl = this.scope.get(name);

		if (decl instanceof DProc) {
			DProc proc = (DProc) decl;

			int paramCount = proc.params.size();
			int argCount = e.getArgs().size();

			if (paramCount != argCount) {
				this.errors.add(e, "'%s' expects %d argument(s), given %d", proc.name, paramCount, argCount);
			}
			
			// create space on the stack for the return argument
			this.builder.adjust(proc.out.type.size());

			int argSpace = 0;
			for (int i = 0; i < argCount; ++i) {
				Expr arg = e.getArgs().get(i);

				if (paramCount <= i) {
					this.compile(arg, EvalContext.RVal);
					continue;
				}

				DParam param = proc.params.get(i);

				DType paramType = param.type;

				if (param.isRef) {
					Attr argAttr = this.compile(arg, EvalContext.LVal);
					DType argType = argAttr.type;
					
					if (!DType.AreCompatible(argType, paramType)) {
						this.errors.add(e, "argument '%s' expects type '%s', given '%s'", param.name, paramType.name, argType.name);
					}
					
					argSpace += 1;
				}
				else {
					Attr argAttr = this.compile(arg, EvalContext.RVal);

					if (argAttr != Attr.Bottom) {
						DType argType = argAttr.type;
						
						if (!DType.AreCompatible(argType, paramType)) {
							if (paramType == TNative.Real && argType == TNative.Int) {
								this.builder.op("itor");
							} else {
								this.errors.add(e, "argument '%s' expects type '%s', given '%s'", param.name, paramType.name, argType.name);
							}
						}
					}

					argSpace += paramType.size();
				}
			}

			this.builder.call(proc.display, Label.ProcBegin(proc));
			this.builder.adjust(-argSpace);
			
			if (context == EvalContext.Void) {
				if (!proc.isProc) {
					this.errors.add(name, "functions may not be called in void context");
				}
			}
			
			return new Attr(proc.out.type);
		} else if (decl instanceof DBuiltIn) {
			DBuiltIn b = (DBuiltIn)decl;
			return b.compile(this, e, e.getArgs());
		}
		else if (!(decl instanceof DError)) {
			this.errors.add(e, "'%s' is a %s, not a procedure or function",
					name.getName(), decl.kind());
		}

		return Attr.Bottom;
	}
	
	public Attr compile(Expr expr, EvalContext context) {
		if (expr == null) {
			return (context == EvalContext.Void ? Attr.Void : Attr.Bottom);
		}
		
		if (expr instanceof ExprBin) {
			Attr ret = this.handleExprBin((ExprBin) expr, context);
			
			if (context == EvalContext.Void) {
				builder.adjust(-ret.type.size());
				return Attr.Void;
			} else if (context == EvalContext.LVal) {
				errors.add(expr, "can't evaluate binary expression as an lval");
				return Attr.Bottom;
			} else {
				return ret;
			}
		} else if (expr instanceof ExprCall) {
			Attr ret = this.handleExprCall((ExprCall)expr, context);
			
			if (context == EvalContext.Void) {
				builder.adjust(-ret.type.size());
				return Attr.Void;
			} else if (context == EvalContext.LVal) {
				errors.add(expr, "can't evaluate function call as an lval");
				return Attr.Bottom;
			} else {
				return ret;
			}
		} else if (expr instanceof ExprBracket) {
			return this.compile(((ExprBracket)expr).getExpr(), context);
		} else if (expr instanceof ExprUnary) {
			ExprUnary u = (ExprUnary) expr;
			
			DBuiltIn op = Operator.Unary(u.getOp());
			Attr ret = op.compile(this, u, Arrays.asList(u.getExpr()));
			
			if (context == EvalContext.Void) {
				builder.adjust(-ret.type.size());
				return Attr.Void;
			} else if (context == EvalContext.LVal) {
				errors.add(expr, "can't evaluate unary expression as an lval");
				return Attr.Bottom;
			} else {
				return ret;
			}
		}
		else if (expr instanceof ExprProp) {
			ExprProp prop = (ExprProp) expr;
			Expr obj = prop.getObj();

			Attr attr = this.compile(obj, EvalContext.LVal);
			if (attr == Attr.Bottom || prop.getProp() == null) {
				return Attr.Bottom;
			}

			if (attr.type instanceof TRec) {
				TRec type = (TRec) attr.type;
				TRec.Field field = type.fields.get(prop.getProp().getName());

				if (field != null) {
					this.builder.konst(field.offset);
					this.builder.op("addi");
					
					Attr fieldAttr = new Attr(field.type, true);
					
					if (context == EvalContext.RVal) {
						this.doDeref(field.type);
						return fieldAttr.deref();
					}
					else if (context == EvalContext.LVal) {
						return fieldAttr;
					}
					else {
						this.builder.adjust(-1);
						return Attr.Void;
					}
				}
				else {
					this.errors.add(prop.getProp(), "'%s' is not a field", prop.getProp().getName());
				}
			}
			else {
				this.errors.add(obj, "left side of field access is not a record");
			}

			return Attr.Bottom;
		}
		else if (expr instanceof ExprIndex) {
			ExprIndex index = (ExprIndex) expr;
			Expr obj = index.getObj();
			
			Attr attr = this.compile(obj, EvalContext.LVal);
			attr = this.handleArrayIndex(attr, obj, index.getIndexes());
			
			if (context == EvalContext.LVal) {
				return attr;
			} else if (context == EvalContext.Void) {
				this.builder.adjust(-1);
				return Attr.Void;
			} else {
				this.doDeref(attr.type);
				return attr.deref();
			}
		}
		else if (expr instanceof NameRef) {
			NameRef r = (NameRef)expr;

			this.builder.comment("var " + r.getName());
			
			Declared decl = this.scope.get(r);

			if (decl instanceof DVar) {
				if (context == EvalContext.Void) return Attr.Void;
				
				DVar d = (DVar)decl;
				Addr varAddr = new Addr(d.offset, d.display);
				
				if (context == EvalContext.RVal) {
					return this.pushOnStackByValue(d.type, varAddr, false);
				} else {
					this.builder.pusha(varAddr);
					return new Attr(d.type, true);
				}
			} else if (decl instanceof DConst) {
				if (context == EvalContext.Void)
					return Attr.Void;

				DConst d = (DConst) decl;

				if (context == EvalContext.LVal) {
					this.errors.add(r, "'%s' is not an lval", r.getName());
					return Attr.Bottom;
				} else {
					this.builder.konst(d.value);
					return new Attr(d.type, false, d.value);
				}
			} else if (decl instanceof DParam) {
				if (context == EvalContext.Void) return Attr.Void;
				
				DParam d = (DParam)decl;
				Addr paramAddr = new Addr(d.offset, d.display);
				
				if (context == EvalContext.RVal) {
					return this.pushOnStackByValue(d.type, paramAddr, d.isRef);
				} else {
					if (d.isRef) {
						this.builder.push(paramAddr);
					} else {
						this.builder.pusha(paramAddr);
					}
					
					return new Attr(d.type, true);
				}
			}
			else if (!(decl instanceof DError)) {
				this.errors.add(r, "'%s' is a %s, not a var or const", r.getName(), decl.kind());
			}

			return Attr.Bottom;
		}
		else if (context == EvalContext.RVal) {
			if (expr instanceof LitInt) {
				String num = ((LitInt) expr).getNum();
				int parsedInt = Integer.parseInt(num);
				this.builder.konst(parsedInt);
				return new Attr(parsedInt);
			}
			else if (expr instanceof LitStr) {
				String str = ((LitStr) expr).getString();

				if (str.isEmpty()) {
					return Attr.Bottom;
				}
				
				if (str.length() == 1) {
					this.builder.konst(str.charAt(0));
				} else {
					this.builder.konst(str);
				}

				return new Attr(str);
			}
			else if (expr instanceof LitReal) {
				String num = ((LitReal) expr).getNum();
				float parsedFloat = Float.parseFloat(num);
				this.builder.konst(parsedFloat);
				return new Attr(parsedFloat);
			}

			return Attr.Bottom;
		}

		return Attr.Bottom;
	}
	
	private Attr pushOnStackByValue(DType type, Addr addr, boolean isRef) {
		int size = type.size();
		
		if (size == 1) {
			this.builder.push(addr);
			
			if (isRef) {
				this.builder.pushi();
			}
		} else if (type != TNative.Bottom && size >= 2) {
			if (isRef) {
				this.builder.push(addr);
			} else {
				//this.builder.adjust(size - 2);
				this.builder.pusha(addr);
			}
			
			this.doDeref(type);
		}
		
		return new Attr(type);
	}
	
	private void doDeref(DType type) {
		if (type.isBottom()) return;
		
		int size = type.size();
		
		if (size == 1) {
			this.builder.pushi();
		} else if (size >= 2) {
			this.builder.adjust(size - 2);
			this.builder.konst(size);
			this.builder.call(0, "std_deref");
		}
	}
	
	public Attr foldTop(Expr expr) {
		if (expr == null) {
			// Syntax error, don't give a semantic one.
			return Attr.Bottom;
		} else {
			Attr attr = this.fold(expr);
			
			if (attr.value == null) {
				errors.add(expr, "cannot evaluate expression to a constant");
			}
			
			return attr;
		}
	}
	
	public Attr fold(Expr expr) {
		if (expr == null)
			return Attr.Bottom;

		if (expr instanceof ExprCall) {
			ExprCall e = (ExprCall) expr;
			NameRef name = e.getProcName();

			Declared decl = this.scope.get(name);

			if (decl instanceof DBuiltIn) {
				DBuiltIn b = (DBuiltIn) decl;
				return b.fold(this, e, e.getArgs());
			} else {
				return Attr.Bottom;
			}
		}
		else if (expr instanceof ExprUnary) {
			ExprUnary u = (ExprUnary) expr;
			DBuiltIn op = Operator.Unary(u.getOp());
			return op.fold(this, expr, Arrays.asList(u.getExpr()));
		} else if (expr instanceof ExprBracket) {
			Expr inner = ((ExprBracket) expr).getExpr();
			return this.fold(inner);
		} else if (expr instanceof ExprBin) {
			ExprBin bin = (ExprBin) expr;
			DBuiltIn op = Operator.Binary(bin.getOp());
			return op.fold(this, expr, Arrays.asList(bin.getLeft(), bin.getRight()));
		} else if (expr instanceof NameRef) {
			NameRef name = (NameRef)expr;
			Declared decl = this.scope.get(name);
			
			if (decl instanceof DConst) {
				DConst d = (DConst) decl;
				return new Attr(d.type, false, d.value);
			}
			else {
				if (!(decl instanceof DError)) {
					this.errors.add(expr, "'%s' is not a const", name.getName());
				}

				return Attr.Bottom;
			}
		} else if (expr instanceof LitInt) {
			return V(Integer.parseInt(((LitInt)expr).getNum()));
		} else if (expr instanceof LitReal) {
			return V(Float.parseFloat(((LitReal)expr).getNum()));
		} else if (expr instanceof LitStr) {
			return V(((LitStr)expr).getString());
		} else {
			return Attr.Bottom;
		}
	}

	private static Attr V(int data) {
		return new Attr(data);
	}

	private static Attr V(float data) {
		return new Attr(data);
	}
	
	private static Attr V(String data) {
		return new Attr(data);
	}
}
