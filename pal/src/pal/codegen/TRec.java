//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

import java.util.Map;

public class TRec extends DType {
	public final Map<String, Field> fields;
	private final int _size;
	
	public TRec(String name, Map<String, Field> fields) {
		super(name == null ? "<anonymous record type>" : name);
		
		this.fields = fields;
		
		int size = 0;
		
		for (Field f: fields.values()) {
			size += f.type.size();
		}
		
		this._size = size;
	}
	
	public static class Field
	{		
		public final String name;
		public final DType type;
		public final int offset;
		
		public Field(String name, DType type, int offset) {
			this.name = name;
			this.type = type;
			this.offset = offset;
		}
	}

	@Override
	public int size() {
		return this._size;
	}
}
