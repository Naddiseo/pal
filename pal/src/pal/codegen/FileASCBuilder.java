//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

public class FileASCBuilder extends ASCBuilderBase {
	private final PrintStream writer;

	public FileASCBuilder(String file) throws FileNotFoundException {
		this.writer = new PrintStream(new File(file));
	}

	public FileASCBuilder(File file) throws FileNotFoundException {
		this.writer = new PrintStream(file);
	}

	public FileASCBuilder(PrintStream stream) {
		this.writer = stream;
	}

	@Override
	public void label(String label) {
		this.writer.println(label);
	}

	@Override
	public void inst(String inst, Object... args) {
		this.writer.print("\t" + inst.toUpperCase());

		boolean first = true;

		for (Object arg : args) {
			if (first) {
				this.writer.print(" ");
				first = false;
			}
			else {
				this.writer.print(", ");
			}

			this.writer.print(ASCBuilderBase.Stringify(arg));
		}

		this.writer.println();
	}

	@Override
	public void comment(String c) {
		c = c.replaceAll("\n", "\\n");
		c = c.replaceAll("\r", "\\r");
		
		if (c.length() > 30) {
			c = c.substring(0, 30);
		}
		
		this.writer.println("# " + c);
	}

	@Override
	public void push(int offset, int reg) {
		this.writer.println(String.format("PUSH %d[%d]", offset, reg));

	}
}
