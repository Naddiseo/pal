//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

import java.util.Arrays;
import java.util.List;

import pal.builtin.DBuiltIn;

import pal.builtin.IOProcs;
import pal.builtin.RealFuncs;
import pal.builtin.DiscreteFuncs;

enum BuiltIns
{
	/* This semicolon is, sadly enough, required */;
	
	public final static List<DBuiltIn> ALL = Arrays.asList(
		IOProcs.Read, IOProcs.ReadLn,
		IOProcs.Write, IOProcs.WriteLn,
		
		DiscreteFuncs.Ord, DiscreteFuncs.Chr,
		DiscreteFuncs.Succ, DiscreteFuncs.Pred,
		DiscreteFuncs.Odd,
		
		RealFuncs.Trunc, RealFuncs.Round, RealFuncs.Abs,
		RealFuncs.Sqr, RealFuncs.Sqrt,
		RealFuncs.Sin, RealFuncs.Exp, RealFuncs.Ln
	);
}
