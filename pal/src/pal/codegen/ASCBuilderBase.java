//
// Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac
// CMPUT 415, Fall 2011
//
package pal.codegen;

import pal.common.Addr;

public abstract class ASCBuilderBase implements ASCBuilder {
	//
	// STACK INSTRUCTIONS
	//
	@Override
	public void push(Addr addr) {
		this.inst("push", addr);
	}

	@Override
	public void push(int offset, int reg) {
		this.inst("push", ASCBuilderBase.Stringify(new DVar(null, null, reg, offset)));
	}

	@Override
	public void pushi(int reg) {
		this.inst("pushi", reg);
	}

	@Override
	public void pushi() {
		this.inst("pushi");
	}

	@Override
	public void pusha(Addr addr) {
		this.inst("pusha", addr);
	}

	@Override
	public void pop(Addr addr) {
		this.inst("pop", addr);
	}

	@Override
	public void popi(int reg) {
		this.inst("popi", reg);
	}

	@Override
	public void popi() {
		this.inst("popi");
	}

	@Override
	public void dup() {
		this.inst("dup");
	}

	private void typedInst(String inst, DType type) {
		if (type == TNative.Int) {
			this.inst(inst + "i");
		}
		else if (type == TNative.Real) {
			this.inst(inst + "r");
		}
		else {
			throw new IllegalArgumentException("Unhandled DType");
		}
	}

	@Override
	public void lt(DType type) {
		this.typedInst("lt", type);
	}

	@Override
	public void le(DType type) {
		this.typedInst("gt", type);
		this.inst("not");
	}

	@Override
	public void gt(DType type) {
		this.typedInst("gt", type);
	}

	@Override
	public void ge(DType type) {
		this.typedInst("lt", type);
		this.inst("not");
	}

	@Override
	public void eq(DType type) {
		this.typedInst("eq", type);
	}

	@Override
	public void ne(DType type) {
		this.typedInst("eq", type);
		this.inst("not");
	}

	@Override
	public void adjust(int n) {
		if (n != 0) {
			this.inst("adjust", n);
		}
	}

	@Override
	public void konst(Object value, String comment) {
		this.konst(value);
	}

	@Override
	public void konst(Object value) {
		if (value instanceof Integer) {
			this.inst("consti", value);
		}
		else if (value instanceof Character) {
			this.inst("consti", (int)(char)(Character)value);
		}
		else if (value instanceof Byte) {
			this.inst("consti", (int)(byte)(Byte)value);
		}
		else if (value instanceof Float) {
			this.inst("constr", value);
		}
		else if(value instanceof Double) {
			this.inst("constr", value);
		}
		else if (value instanceof String) {
			String str = (String) value;
			this.comment(str);
			for (byte c : str.getBytes()) {
				this.konst((int) c);
			}
		}
		else {
			throw new IllegalArgumentException("Unhandled constant type.");
		}
	}

	//
	// HEAP INSTRUCTIONS
	//
	@Override
	public void alloc(int n) {
		this.inst("alloc", n);
	}

	@Override
	public void free() {
		this.inst("free");
	}

	@Override
	public void salloc() {
		this.inst("salloc");
	}

	//
	// FLOW CONTROL INSTRUCTIONS
	//
	@Override
	public void ifz(String label) {
		this.inst("ifz", label);
	}

	@Override
	public void ifnz(String label) {
		this.inst("ifnz", label);
	}

	@Override
	public void iferr(String label) {
		this.inst("iferr", label);
	}

	@Override
	public void jump(String label) {
		this.inst("goto", label);
	}

	@Override
	public void call(int reg, String label) {
		this.inst("call", reg, label);
	}

	@Override
	public void ret(int reg) {
		this.inst("ret", reg);
	}

	@Override
	public void stop() {
		this.inst("stop");
	}

	@Override
	public void comment(String c) {
		// Ignore comments by default
	}

	@Override
	public abstract void label(String label);

	@Override
	public void op(String op) {
		this.inst(op);
	}
	
	public static String Stringify(Object arg) {
		if (arg instanceof DVar) {
			DVar var = (DVar) arg;
			return String.format("%d[%d]", var.offset, var.display);
		}
		else if (arg instanceof DParam) {
			DParam var = (DParam) arg;
			return String.format("%d[%d]", var.offset, var.display);
		}
		else {
			return arg.toString();
		}
	}
}
