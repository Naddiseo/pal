//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

import java.util.Arrays;
import java.util.List;

public class TNative extends DType {
	private int _size;
	
	private TNative(String name, int size) {
		super(name);
		
		this._size = size;
	}
	
	public final static TEnum Bool = new TEnum("boolean", 0, 1);
	public final static TEnum Char = new TEnum("char", 0, 255);
	public final static TEnum Int = new TEnum("integer", -Integer.MAX_VALUE, Integer.MAX_VALUE);
	
	public final static TNative Real = new TNative("real", 1);
	public final static TNative Bottom = new TNative("<error>", 0);
	public final static TNative Void = new TNative("<void>", 0);
	
	public final static List<DType> ALL = Arrays.asList(
		Real, TNative.Int, TNative.Bool, TNative.Char
	);
	
	@Override
	public int size() {
		return this._size;
	}
}
