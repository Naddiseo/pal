//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

public class DVar extends Declared {
	public final DType type;
	public final int display;
	public final int offset;

	public DVar(String name, DType type, int display, int offset) {
		super(name);

		this.type = type;
		this.display = display;
		this.offset = offset;
	}

	@Override
	public String kind() {
		return "var";
	}

	public boolean equals(DVar other) {
		return display == other.display && offset == other.offset
				&& type.equals(other.type);
	}
}
