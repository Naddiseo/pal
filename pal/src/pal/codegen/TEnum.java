//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

public class TEnum extends DType
{
	public final int min;
	public final int max;
	
	public TEnum(String name, int min, int max) {
		super(name == null ? "<anonymous enum type>" : name);
		this.min = min;
		this.max = max;
	}

	@Override
	public int size() {
		return 1;
	}
}
