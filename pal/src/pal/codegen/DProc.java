//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

import java.util.List;

public class DProc extends Declared {
	public final int display;
	public final List<DParam> params;
	public DVar out;
	public final String id;
	public final boolean isProc;
	public boolean isReturnAssigned = false;
	
	public DProc(String name, int display, List<DParam> args, DVar out, int idx, boolean isProc) {
		super(name);
		
		this.display = display;
		this.params = args;
		this.out = out;
		this.id = name + "_" + idx;
		this.isProc = isProc;
	}

	@Override
	public String kind() {
		return isProc ? "procedure" : "function";
	}
}
