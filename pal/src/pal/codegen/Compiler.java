//
// Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac
// CMPUT 415, Fall 2011
//
package pal.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import pal.bins.Tester;
import pal.common.Addr;
import pal.common.ErrorReport;
import pal.optimizer.BasicBlockBuilder;
import pal.xtext.pal.Constant;
import pal.xtext.pal.Declarations;
import pal.xtext.pal.Expr;
import pal.xtext.pal.Field;
import pal.xtext.pal.NameRef;
import pal.xtext.pal.Param;
import pal.xtext.pal.Proc;
import pal.xtext.pal.Program;
import pal.xtext.pal.Statement;
import pal.xtext.pal.StmtAssign;
import pal.xtext.pal.StmtBlock;
import pal.xtext.pal.StmtContinue;
import pal.xtext.pal.StmtExit;
import pal.xtext.pal.StmtIf;
import pal.xtext.pal.StmtWhile;
import pal.xtext.pal.Type;
import pal.xtext.pal.TypeArray;
import pal.xtext.pal.TypeDecl;
import pal.xtext.pal.TypeEnum;
import pal.xtext.pal.TypeRange;
import pal.xtext.pal.TypeRecord;
import pal.xtext.pal.VarDecl;

public class Compiler {
	static final Logger logger = Logger.getLogger(Tester.class.getName());
	
	private final ErrorReport errors;
	private final ScopeStack scope;
	
	public final ASCBuilder builder;
	public final CompilerExpr exprCompiler;
	
	private int procIndex;
	private int labelIndex;
	private final boolean arraySubscriptChecking;
	
	public Compiler(ASCBuilder builder, boolean arraySubscriptChecking) {
		this.arraySubscriptChecking = arraySubscriptChecking;
		this.errors = new ErrorReport();
		this.scope = new ScopeStack(this.errors);
		this.builder = builder;
		this.procIndex = 0;
		this.exprCompiler = new CompilerExpr(this.builder, this.errors,
				this.scope, this.arraySubscriptChecking);
	}
	
	public ErrorReport compile(Program p) {
		if (p != null) {
			this.declareBuiltIns();
			
			this.scope.push(null);
			this.caseProgram(p);
			this.scope.pop();
		}
		else {
			this.errors.add(null, "no program in file");
		}
		
		if (this.builder instanceof BasicBlockBuilder) {
			((BasicBlockBuilder) this.builder).optimize();
			((BasicBlockBuilder) this.builder).save();
		}
		
		return this.errors;
	}
	
	private void caseProgram(Program p) {
		if (p.getName() != null) {
			this.scope.declare(p.getName(), new DJunk(p.getName().getName(),
					"program"));
		}
		
		if (p.getInput() != null) {
			this.scope.declare(p.getInput(), new DJunk(p.getInput().getName(),
					"input"));
		}
		
		if (p.getOutput() != null) {
			this.scope.declare(p.getOutput(), new DJunk(
					p.getOutput().getName(), "output"));
		}
		
		this.builder.jump(Label.Begin);
		
		StdLib.EmitCode(this.builder);
		
		int varspace = this.handleDeclarations(p.getDeclarations());
		
		this.builder.label(Label.Begin);
		
		if (varspace > 0) {
			this.builder.adjust(varspace);
		}
		
		this.compile(p.getMain());
		
		this.builder.label(Label.End);
		this.builder.stop();
	}
	
	private int handleDeclarations(Declarations d) {
		if (d == null) {
			// return what createVars(d) would return
			// it checks if d == null too
			return this.createVars(d);
		}
		
		for (Constant c : d.getConsts()) {
			this.createConstant(c);
		}
		
		for (TypeDecl t : d.getTypes()) {
			this.createType(t);
		}
		
		int varspace = this.createVars(d);
		
		for (Proc proc : d.getProcs()) {
			this.caseProc(proc);
		}
		
		return varspace;
	}
	
	private void createConstant(Constant c) {
		if (c == null) {
			return;
		}
		Attr obj = this.exprCompiler.foldTop(c.getValue());
		if (obj != null) {
			this.scope.declare(c.getName(), new DConst(c.getName().getName(),
					obj.type, obj.value));
		}
		else {
			// folding was unsuccessful
			this.errors.add(c, "const value is not a compile-time constant");
		}
	}
	
	private DType createType(String name, Type t) {
		if (t instanceof TypeArray) {
			return this.createArrayType(name, (TypeArray) t);
		}
		else if (t instanceof TypeRecord) {
			return this.createRecordType(name, (TypeRecord) t);
		}
		else if (t instanceof TypeEnum) {
			TypeEnum type = (TypeEnum) t;
			
			TEnum ret = new TEnum(name, 0, type.getValues().size() - 1);
			
			int i = 0;
			for (NameRef value : type.getValues()) {
				this.scope.declare(value, new DConst(value.getName(), ret, i));
				i += 1;
			}
			
			return ret;
		}
		else {
			return this.scope.getType((NameRef) t);
		}
	}
	
	private TRec createRecordType(String name, TypeRecord type) {
		Map<String, TRec.Field> fields = new HashMap<String, TRec.Field>();
		
		int offset = 0;
		
		this.scope.push(null);
		
		for (Field field : type.getFields()) {
			if ((field == null) || (field.getName() == null)) {
				continue;
			}
			
			String fieldName = field.getName().getName();
			
			if (fieldName == null) {
				continue;
			}
			
			DType fieldType = this.createType(null, field.getType());
			
			DJunk dfield = new DJunk(fieldName, "record field");
			this.scope.declare(field.getName(), dfield);
			
			fields.put(fieldName, new TRec.Field(fieldName, fieldType, offset));
			offset += fieldType.size();
		}
		
		this.scope.pop();
		
		return new TRec(name, fields);
	}
	
	private TArray createArrayType(String name, TypeArray type) {
		DType valueType = this.createType(null, type.getType());
		DType indexType;
		
		// Make longs incase of overflows in integers
		long min = 0;
		long max = 1;
		
		Type gIndexType = type.getIndexType();
		
		if (gIndexType instanceof TypeRange) {
			TypeRange tr = (TypeRange) gIndexType;
			
			Attr k1 = this.exprCompiler.foldTop(tr.getMin());
			Attr k2 = this.exprCompiler.foldTop(tr.getMax());
			
			if ((k1 == null) && (k2 == null)) {
				indexType = TNative.Bottom;
				min = 0;
				max = 0;
			}
			else if (k1 == null) {
				indexType = k2.type;
				
				if (indexType instanceof TEnum) {
					max = k2.vint();
					min = max - 1;
				}
				else if (indexType != TNative.Bottom) {
					this.errors.add(tr.getMax(), "'%s' is not an ordinal type", indexType.name);
					indexType = TNative.Bottom;
				}
			}
			else if (k2 == null) {
				indexType = k1.type;
				
				if (indexType instanceof TEnum) {
					min = k1.vint();
					max = min + 1;
				}
				else if (indexType != TNative.Bottom) {
					this.errors.add(tr.getMax(), "'%s' is not an ordinal type", indexType.name);
					indexType = TNative.Bottom;
				}
			}
			else {
				DType t1 = k1.type;
				DType t2 = k2.type;
				
				if (t1 != t2) {
					this.errors.add(gIndexType, "incompatible types used for range");
					indexType = TNative.Bottom;
					min = 0;
					max = 1;
				}
				else {
					indexType = t1;
					
					if (t1 instanceof TEnum) {
						min = k1.vint();
						max = k2.vint();
						
						if (min > max) {
							this.errors.add(tr.getMax(), "min value of range may not be larger than max value");
							
							long tmp = min;
							min = max;
							max = tmp;
						} else if (min == 1 && max == 1 && indexType == TNative.Int && valueType == TNative.Char) {
							this.errors.add(tr, "strings must have more than one character");
							max = 2;
						}
						
						if ((((max - min) + 1) * Math.max(1, valueType.size())) > 8196) {
							this.errors.add(gIndexType, "array too large to fit on ASC stack");
						}
					}
					else if (t1 != TNative.Bottom) {
						this.errors.add(tr.getMax(), "'%s' is not an ordinal type", t1.name);
						indexType = TNative.Bottom;
					}
				}
			}
		}
		else {
			indexType = this.resolveType(gIndexType);
			
			if (indexType instanceof TEnum) {
				TEnum e = (TEnum) indexType;
				min = e.min;
				max = e.max;
				
				if ((max - min) > 8192) {
					this.errors.add(gIndexType, "array too large to fit on ASC stack");
				}
			}
			else {
				this.errors.add(gIndexType, "array index must be of ordinal type");
				indexType = TNative.Bottom;
			}
		}
		
		return new TArray(name, valueType, indexType, (int) min, (int) max);
	}
	
	private void createType(TypeDecl t) {
		if (t == null) {
			return;
		}
		DType type = this.createType(t.getName().getName(), t.getType());
		this.scope.declare(t.getName(), type);
	}
	
	private void caseProc(Proc proc) {
		if (proc == null) {
			return;
		}
		
		List<DParam> parameters = new ArrayList<DParam>();
		
		if (proc.isProc() && (proc.getType() != null)) {
			this.errors.add(proc.getType(), "procedure may not return a value");
		}
		else if (!proc.isProc() && (proc.getType() == null)) {
			this.errors.add(proc.getName(), "function missing return value");
		}
		
		DType type;
		
		if (proc.isProc()) {
			type = TNative.Void;
		}
		else {
			type = (
				proc.getType() == null ?
				TNative.Bottom :
				this.scope.getType(proc.getType())
			);
			
			if ((type instanceof TRec) || (type instanceof TArray)) {
				this.errors.add(proc.getType(), "functions may not return a structured type");
				type = TNative.Bottom;
			}
		}
		
		List<Param> params = proc.getParams();
		
		NameRef procName = proc.getName();
		
		if (this.scope.display() >= 15) {
			this.errors.add(procName == null ? proc : procName,
				"%s exceeds the maximum nesting depth",
				procName == null ? "<unknown>" : procName.getName()
			);
		}
		
		if (procName == null) {
			// No error; this would only happen on a syntax error, in which case
			// an error would have already been reported.
			// this.errors.add(proc, "invalid procedure name");
		}
		
		DProc dproc = new DProc(
			(procName == null ? null : procName.getName()),
			this.scope.display() + 1, parameters, null, this.nextProc(),
			proc.isProc()
		);
		
		if (procName != null) {
			this.scope.declare(procName, dproc);
		}
		
		this.scope.push(dproc);

		int offset = -2;
		
		for (Param p : params) {
			DType paramType = (
				p.getType() == null ? TNative.Bottom : this.scope.getType(p.getType())
			);
			
			if (p.getName() != null) {
				offset -= (p.isRef() ? 1 : paramType.size());
				
				DParam dparam = new DParam(
					p.getName().getName(), paramType,
					this.scope.display(), 0, p.isRef()
				);
				
				this.scope.declare(p.getName(), dparam);
				parameters.add(dparam);
			}
		}
		
		DVar out = new DVar("$ret", type, this.scope.display(), offset - type.size());
		dproc.out = out;
		
		for (DParam p: parameters) {
			p.offset = offset;
			offset += (p.isRef ? 1 : p.type.size());
		}
		
		// Do this so it gets added in the current scope as well.
		if (!proc.isProc() && (procName != null)) {
			this.scope.get(procName);
		}
		
		{
			int varSpace = this.handleDeclarations(proc.getDeclarations());
			
			this.builder.label(Label.ProcBegin(dproc));
			
			this.builder.adjust(varSpace);
			this.compile(proc.getBody());
			
			if (!dproc.isProc && !dproc.isReturnAssigned) {
				this.errors.add(
					procName == null ? proc : procName,
					"function '%s' never sets its return value",
					dproc.name
				);
			}
			
			this.builder.label(Label.ProcEnd(dproc));
			this.builder.adjust(-varSpace);
			this.builder.ret(dproc.display);
		}
		
		this.scope.pop();
	}
	
	private void compile(Statement stmt) {
		if (stmt instanceof StmtBlock) {
			StmtBlock s = (StmtBlock) stmt;
			
			for (Statement ss : s.getStatements()) {
				this.compile(ss);
			}
		}
		else if (stmt instanceof StmtAssign) {
			StmtAssign s = (StmtAssign) stmt;
			
			boolean lhsError = false;
			boolean lhsHandled = false;
			
			Attr lhsAttr = Attr.Bottom;
			
			// Check for the special case of assigning to the current proc
			if (s.getLeft() instanceof NameRef) {
				NameRef nameRef = (NameRef) s.getLeft();
				String name = nameRef.getName();
				
				DProc currentProc = this.scope.getCurrentProc();
				
				if (currentProc != null) {
					if (currentProc.name != null && currentProc.name.equals(name)) {
						if (this.scope.get(nameRef) instanceof DProc) {
							lhsHandled = true;
							
							if (currentProc.isProc) {
								this.errors.add(s.getLeft(), "procedures cannot return a value");
								lhsError = true;
								lhsAttr = Attr.Bottom;
							}
							else {
								this.builder.pusha(new Addr(
									currentProc.out.offset,
									currentProc.out.display
								));
								lhsAttr = new Attr(currentProc.out.type, true);
								currentProc.isReturnAssigned = true;
							}
						}
					}
				}
			}
			
			if (!lhsHandled) {
				lhsAttr = this.exprCompiler.compile(
					s.getLeft(),
					EvalContext.LVal
				);
			}
			
			if (lhsError || lhsAttr == Attr.Bottom) {
				this.exprCompiler.compile(s.getRight(), EvalContext.RVal);
			}
			else {
				Attr rhsAttr = this.exprCompiler.compile(
					s.getRight(),
					EvalContext.RVal
				);
				
				if (!DType.AreCompatible(rhsAttr.type, lhsAttr.type)) {
					if (lhsAttr.type == TNative.Real && rhsAttr.type == TNative.Int) {
						this.builder.op("itor");
					}
					else {
						this.errors.add(
							stmt, "expect value of type '%s', got '%s'",
							lhsAttr.type.name, rhsAttr.type.name
						);
						return;
					}
				}
				
				int size = rhsAttr.type.size();
				
				if (size == 1) {
					this.builder.popi();
				} else if (size >= 2) {
					this.builder.konst(size);
					this.builder.call(0, "std_copyinto");
					this.builder.adjust(-size - 2);
				}
			}
		}
		else if (stmt instanceof StmtIf) {
			StmtIf s = (StmtIf) stmt;
			
			String labelBase = this.nextLabel();
			
			String lblEnd = Label.IfEnd(labelBase);
			
			Attr attr = this.exprCompiler.compile(s.getCond(), EvalContext.RVal);
			
			if (attr.type != TNative.Bool && attr.type != TNative.Bottom) {
				this.errors.add(
					s.getCond(),
					"condition does not result in a boolean value"
				);
			}
			
			Statement sthen = s.getThenClause();
			Statement selse = s.getElseClause();
			
			if (selse == null) {
				this.builder.ifz(lblEnd);
				this.compile(sthen);
			}
			else {
				String lblElse = Label.IfElse(labelBase);
				this.builder.ifz(lblElse);
				this.compile(sthen);
				this.builder.jump(lblEnd);
				this.builder.label(lblElse);
				this.compile(selse);
			}
			
			this.builder.label(lblEnd);
		}
		else if (stmt instanceof StmtWhile) {
			StmtWhile s = (StmtWhile) stmt;
			
			String labelBase = this.nextLabel();
			
			this.scope.loops.push(labelBase);
			
			String top = Label.WhileTop(labelBase);
			String bottom = Label.WhileBottom(labelBase);
			
			this.builder.label(top);
			
			Attr attr = this.exprCompiler.compile(s.getCond(), EvalContext.RVal);
			if (attr.type != TNative.Bool && attr.type != TNative.Bottom) {
				this.errors.add(
					s.getCond(),
					"condition does not result in a boolean value"
				);
			}
			
			this.builder.ifz(bottom);
			
			this.compile(s.getBody());
			
			this.builder.jump(top);
			this.builder.label(bottom);
			
			this.scope.loops.pop();
		}
		else if (stmt instanceof StmtContinue) {
			if (this.scope.loops.empty()) {
				this.errors.add(stmt, "'continue' is invalid outside of a while loop");
				return;
			}
			
			String labelBase = this.scope.loops.peek();
			this.builder.jump(Label.WhileTop(labelBase));
		}
		else if (stmt instanceof StmtExit) {
			if (this.scope.loops.empty()) {
				this.errors.add(stmt, "'exit' is invalid outside of a while loop");
				return;
			}
			
			String labelBase = this.scope.loops.peek();
			this.builder.jump(Label.WhileBottom(labelBase));
		}
		else if (stmt instanceof Expr) {
			this.exprCompiler.compile((Expr) stmt, EvalContext.Void);
		}
	}
	
	public static enum EvalContext {
		Void, RVal, LVal;
	}
	
	private DType resolveType(Type t) {
		if (t instanceof NameRef) {
			return this.scope.getType((NameRef) t);
		}
		else {
			return this.createType(null, t);
		}
	}
	
	private int createVars(Declarations decl) {
		int varspace = 0;
		
		if (decl != null) {
			for (VarDecl v : decl.getVars()) {
				DType t = this.resolveType(v.getType());
				
				for (NameRef name : v.getNames()) {
					DVar var = new DVar(name.getName(), t,
							this.scope.display(),
							this.scope.getCurrentVarOffset(t));
					this.scope.declare(name, var);
					varspace += var.type.size();
				}
			}
		}
		
		return varspace;
	}
	
	private void declareBuiltIns() {
		for (DType type : TNative.ALL) {
			this.scope.addBuiltIn(type.name, type);
		}
		
		for (Declared builtin : BuiltIns.ALL) {
			this.scope.addBuiltIn(builtin.name, builtin);
		}
		
		this.scope.addBuiltIn("false", new DConst("false", TNative.Bool, 0));
		this.scope.addBuiltIn("true", new DConst("true", TNative.Bool, 1));
		this.scope.addBuiltIn("maxint", new DConst("maxint", TNative.Int,
				TNative.Int.max));
	}
	
	private String nextLabel() {
		int idx = this.labelIndex;
		this.labelIndex += 1;
		return String.format("L%02d_%%s", idx);
	}
	
	private int nextProc() {
		int idx = this.procIndex;
		this.procIndex += 1;
		return idx;
	}
}
