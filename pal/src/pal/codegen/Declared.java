//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.codegen;

public abstract class Declared {
	public final String name;
	
	public Declared(String name) {
		this.name = name;
	}
	
	public abstract String kind();
}
