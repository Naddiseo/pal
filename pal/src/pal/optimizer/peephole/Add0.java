package pal.optimizer.peephole;

import pal.optimizer.Inst;
import pal.optimizer.Inst.Type;

public class Add0 extends Peephole
{	
	@Override
	public boolean optimizeDouble(Inst a, Inst b) {
		if ((a.type == Type.CONSTI) && ((Integer) a.args[0] == 0) && (b.type == Type.ADDI)) {
			// NOOP
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean optimizeTriple(Inst a, Inst b, Inst c) {
		return false;
	}
}
