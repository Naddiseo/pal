//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.optimizer.peephole;

import java.util.ArrayList;
import java.util.List;

import pal.optimizer.Inst;

public abstract class Peephole {
	List<Inst> newInstructions = null;
	List<Inst> oldInstructions = null;
	
	// This doesn't work if there are multiple adjacent comments
	// so I'm copying instructions for simplicity.
	public List<Inst> optimize(List<Inst> instructions) {
		this.newInstructions = new ArrayList<Inst>();
		this.oldInstructions = new ArrayList<Inst>();
		
		for (Inst inst: instructions) {
			if (inst.label != null || inst.type != null) {
				this.oldInstructions.add(inst);
			}
		}
		
		Inst a, b, c;
		Integer i = 2;
		
		if (this.oldInstructions.size() < 2) {
			return this.oldInstructions;
		}
		
		a = getNextInstruction(0);
		b = getNextInstruction(1);
		
		if (this.oldInstructions.size() == 2) {
			if (optimizeDouble(a, b)) {
				return this.newInstructions;
			}
			else {
				return this.oldInstructions;
			}
		}
		
		c = getNextInstruction(i);
		
		do {
			
			if (optimizeTriple(a, b, c)) {
				// Optimized using all three, so discard
				a = getNextInstruction(++i);
				b = getNextInstruction(++i);
				c = getNextInstruction(++i);
			}
			else {
				// Couldn't optimize the three, perhaps we can do two
				if (optimizeDouble(a, b)) {
					// discard a and b
					a = c;
					b = getNextInstruction(++i);
					c = getNextInstruction(++i);
				}
				else {
					// Nope, so add 'a' to the instructions
					this.newInstructions.add(a);
					a = b;
					b = c;
					c = getNextInstruction(++i);
				}
			}
			
		}
		while (i <= this.oldInstructions.size());
		
		// add any left over instructions
		if (a != null) {
			this.newInstructions.add(a);
			if (b != null) {
				this.newInstructions.add(b);
				if (c != null) {
					this.newInstructions.add(c);
				}
			}
		}
		
		return this.newInstructions;
	}
	
	private Inst getNextInstruction(int i) {
		if (i < this.oldInstructions.size()) {
			Inst ret = this.oldInstructions.get(i);
			// Skip comments
			if (ret.type == null && ret.label == null && ret.comment != null) {
				return getNextInstruction(i+1);
			}
			return ret;
		}
		return null;
	}
	
	/*
	 * returns true if it added an instruction to newInstructions
	 */
	public abstract boolean optimizeDouble(Inst a, Inst b);
	
	public abstract boolean optimizeTriple(Inst a, Inst b, Inst c);
}
