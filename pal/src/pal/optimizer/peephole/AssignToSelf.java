package pal.optimizer.peephole;

import pal.optimizer.Inst;
import pal.optimizer.Inst.Type;

public class AssignToSelf extends Peephole {
	
	@Override
	public boolean optimizeDouble(Inst a, Inst b) {
		return false;
	}
	
	@Override
	public boolean optimizeTriple(Inst a, Inst b, Inst c) {
		if ((a.type == Type.PUSHA) && (b.type == Type.PUSH)
				&& (c.type == Type.POPI)) {
			if (a.args[0].equals(b.args[0])) {
				// Self assignment, noop
				return true;
			}
		}
		return false;
	}
	
}
