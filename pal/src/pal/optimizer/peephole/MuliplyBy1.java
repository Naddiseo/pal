package pal.optimizer.peephole;

import pal.optimizer.Inst;
import pal.optimizer.Inst.Type;


public class MuliplyBy1 extends Peephole {
	
	@Override
	public boolean optimizeDouble(Inst a, Inst b) {
		if ((a.type == Type.CONSTI) && ((Integer) a.args[0] == 1)
				&& (b.type == Type.MULI)) {
			// NOOP
			return true;
		}
		return false;
	}
	
	@Override
	public boolean optimizeTriple(Inst a, Inst b, Inst c) {
		return false;
	}
}

