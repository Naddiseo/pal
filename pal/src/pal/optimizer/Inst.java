//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.optimizer;

import pal.codegen.ASCBuilderBase;

public class Inst {
	public String label;
	public String comment;
	public final Type type;
	public final Object[] args;
	
	public Inst(String label) {
		if ("".equals(label)) {
			label = null;
		}
		
		this.label = label;
		this.type = null;
		this.args = null;
		this.comment = null;
	}
	
	public Inst(Type type, Object... args) {
		this.type = type;
		this.args = args;
		this.label = null;
		this.comment = null;
	}
	
	@Override
	public String toString() {
		String ret = "";
		if ((this.type == null) && (this.label != null)) {
			ret = this.label;
		}
		else if (this.type != null) {
			
			ret = "\t" + this.type.toString();
			
			boolean first = true;
			
			for (Object arg : this.args) {
				if (first) {
					ret += " ";
					first = false;
				}
				else {
					ret += ", ";
				}
				
				ret += ASCBuilderBase.Stringify(arg);
			}
		}
		
		if (this.comment != null) {
			ret += " # " + this.safeComment();
		}
		
		return ret;
	}
	
	private String safeComment() {
		return this.comment.replace("\n", "\\n").replace("\r", "\\r");
	}
	
	public static enum Type {
		PUSH, PUSHI, PUSHA, POP, POPI, CONSTI, CONSTR, ADJUST, ALLOC, FREE, ADDI, ADDR, SUBI, SUBR, MULI, MULR, DIVI, DIVR, MOD, ITOR, RTOI, EQI, EQR, LTI, LTR, GTI, GTR, OR, AND, NOT, IFZ, IFNZ, GOTO, STOP, CALL, RET, READI, READR, READC, WRITEI, WRITER, WRITEC, TRACE, DUMP, DUP, SALLOC, IFERR;
		
		public final String name;
		
		private Type() {
			this.name = this.name().toLowerCase();
		}
		
		public static Type Parse(String str) {
			for (Type t : Type.values()) {
				if (t.name.toUpperCase().equals(str.toUpperCase())) {
					return t;
				}
			}
			
			// shouldn't get here
			System.err.println("Unknown instruction " + str);
			return null;
		}
		
		public Inst create(Object... args) {
			return new Inst(this, args);
		}
	}
}
