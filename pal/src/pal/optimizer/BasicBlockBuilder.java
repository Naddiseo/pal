//
// Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac
// CMPUT 415, Fall 2011
//
package pal.optimizer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import pal.codegen.ASCBuilderBase;
import pal.optimizer.Inst.Type;

public class BasicBlockBuilder extends ASCBuilderBase {
	private final PrintStream writer;
	private List<Inst> instructions = new ArrayList<Inst>();
	Map<String, Integer> labels = new HashMap<String, Integer>();
	private List<BasicBlock> blocks = new ArrayList<BasicBlock>();
	
	public BasicBlockBuilder(PrintStream fasc) {
		this.writer = fasc;
	}
	
	public BasicBlockBuilder(File fasc) throws FileNotFoundException {
		this.writer = new PrintStream(fasc);
	}
	
	public void optimize() {
		if (this.instructions.size() == 0) {
			return;
		}
		mergeLabels();
		createBlocks();
		removeDeadBlocks();
		
		optimizeBlocks();
		shortenLabels(); // must come last
	}
	
	private void optimizeBlocks() {
		for (BasicBlock block : this.blocks) {
			block.optimize();
		}
	}
	
	private void shortenLabels() {
		Integer lbl = 0, offset = -1;
		String newLabel, oldLabel;
		Map<String, String> changeMap = new HashMap<String, String>();
		
		for (BasicBlock block : this.blocks) {
			for (Inst inst : block.instructions) {
				if (inst.label != null) {
					changeMap.put(inst.label, "LBL_" + (lbl++).toString());
				}
			}
		}
		
		for (BasicBlock block : this.blocks) {
			for (Inst inst : block.instructions) {
				if (inst.label != null) {
					inst.comment = inst.label;
					inst.label = changeMap.get(inst.label);
					continue;
				}
				if (inst.type == null) {
					continue;
				}
				switch (inst.type) {
					case GOTO:
					case IFERR:
					case IFNZ:
					case IFZ:
						offset = 0;
						break;
					case CALL:
						offset = 1;
						break;
					default:
						offset = -1;
						break;
				}
				
				if (offset > -1) {
					oldLabel = (String) inst.args[offset];
					newLabel = changeMap.get(oldLabel);
					
					inst.comment = newLabel + " -> " + oldLabel;
					inst.args[offset] = newLabel;
				}
			}
		}
	}
	
	/*
	 * Removes blocks that can't be executed
	 */
	private void removeDeadBlocks() {
		List<BasicBlock> newBlocks = new ArrayList<BasicBlock>();
		for (int i = 0; i < this.blocks.size(); i++) {
			BasicBlock block = this.blocks.get(i);
			if (block.instructions.size() == 0) {
				continue;
			}
			Inst lastInst = block.instructions
					.get(block.instructions.size() - 1);
			if ((lastInst.type != null) && (lastInst.type == Type.GOTO)) {
				int j = i + 1;
				if ((this.blocks.size() - 1) > j) {
					BasicBlock nextBlock = this.blocks.get(j);
					while ((nextBlock.label == null)
							&& ((j + 1) < this.blocks.size())) {
						nextBlock = this.blocks.get(++j);
					}
					
					newBlocks.add(block);
					newBlocks.add(nextBlock);
					i = j;
					continue;
				}
			}
			
			newBlocks.add(block);
		}
		this.blocks = newBlocks;
	}
	
	private void mergeLabels() {
		Map<String, String> mergeMap = new HashMap<String, String>();
		
		Inst lastInst = null;
		
		/*
		 * Construct all pairs of labels that can be merged
		 */
		for (Inst inst : this.instructions) {
			if ((inst.label != null) && (lastInst != null)
					&& (lastInst.label != null)) {
				mergeMap.put(lastInst.label, inst.label);
			}
			lastInst = inst;
		}
		
		/*
		 * For each pair of labels, create a linked list, then traverse to the
		 * end of that list to find what the first entry will ultimately point
		 * to
		 */
		for (Map.Entry<String, String> entry : mergeMap.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			
			if (mergeMap.containsKey(value)) {
				String newKey = findListEnd(value, mergeMap);
				mergeMap.put(key, newKey);
			}
			
		}
		
		List<Inst> newInstructions = new ArrayList<Inst>();
		
		for (Inst inst : this.instructions) {
			String oldLabel = null;
			Integer offset = -1;
			if (inst.label != null) {
				Integer lineNo = newInstructions.size();
				if (mergeMap.containsKey(inst.label)) {
					// ignore this label
					continue;
				}
				if (lineNo != null) {
					this.labels.put(inst.label, lineNo);
				}
			}
			if (inst.type != null) {
				switch (inst.type) {
					case IFZ:
					case IFNZ:
					case IFERR:
					case GOTO:
						offset = 0;
						break;
					case CALL:
						offset = 1;
						break;
					default:
						offset = -1;
				}
				
				if (offset > -1) {
					oldLabel = (String) inst.args[offset];
					if (mergeMap.containsKey(oldLabel)) {
						inst.args[offset] = mergeMap.get(oldLabel);
					}
				}
			}
			
			newInstructions.add(inst);
		}
		
		this.instructions = newInstructions;
	}
	
	private String findListEnd(String key, Map<String, String> mergeMap) {
		
		while (mergeMap.containsKey(key)) {
			key = mergeMap.get(key);
		}
		
		return key;
	}
	
	private void createBlocks() {
		List<Integer> leaders = new ArrayList<Integer>();
		BasicBlock block = null;
		
		int i = 0;
		
		leaders.add(0);
		
		// get the leaders
		while (i < this.instructions.size()) {
			Inst inst = this.instructions.get(i);
			if (inst.type == null) {
				if (inst.label != null) {
					/*
					 * I think this is a label that never gets jumped to.
					 */
					leaders.add(this.labels.get(inst.label));
				}
				i++;
				continue;
			}
			switch (inst.type) {
				case IFZ:
				case IFNZ:
				case IFERR:
				case GOTO:
					leaders.add(this.labels.get(inst.args[0]));
					i++;
					if (i < this.instructions.size()) {
						leaders.add(i++);
					}
					break;
				case CALL:
					leaders.add(this.labels.get(inst.args[1]));
					i++;
					if (i < this.instructions.size()) {
						leaders.add(i++);
					}
					
					break;
				default:
					i++;
					break;
			}
		}
		
		// Get all the unique leaders and sort
		leaders = new ArrayList<Integer>(new HashSet<Integer>(leaders));
		
		for (Integer leader: leaders) {
			if (leader == null) {
				throw new IllegalArgumentException("null leader; check labels in stdlib");
			}
		}
		
		Collections.sort(leaders);
		
		i = 0;
		for (int leader : leaders) {
			block = new BasicBlock();
			Inst firstInst = this.instructions.get(i);
			if (firstInst.label != null) {
				block.label = firstInst.label;
			}
			for (Integer j = i; j < leader; j++) {
				block.instructions.add(this.instructions.get(j));
			}
			i = leader;
			this.blocks.add(block);
		}
		// Now add the final block
		block = new BasicBlock();
		for (int j = i; j < this.instructions.size(); j++) {
			block.instructions.add(this.instructions.get(j));
		}
		this.blocks.add(block);
	}
	
	public void save() {
		Integer bn = 0;
		// for (Inst inst : this.instructions) {
		for (BasicBlock block : this.blocks) {
			String blabel = (block.label == null ? bn.toString() : block.label);
			
			this.writer.println("# Block: " + blabel);
			for (Inst inst : block.instructions) {
				this.writer.println(inst.toString());
			}
			this.writer.println("# EndBlock: " + blabel + "\n");
			bn++;
		}
		// }
	}
	
	@Override
	public void label(String label) {
		this.instructions.add(new Inst(label));
	}
	
	@Override
	public void inst(String inst, Object... args) {
		Inst i;
		i = Inst.Type.Parse(inst).create(args);
		this.instructions.add(i);
	}
	
	@Override
	public void konst(Object value, String comment) {
		Inst i;
		
		if (value instanceof Integer) {
			i = Inst.Type.Parse("consti").create(value);
		}
		else if (value instanceof Float) {
			i = Inst.Type.Parse("constr").create(value);
		}
		else if (value instanceof Character) {
			i = Inst.Type.Parse("consti").create((int)(Character)value);
		}
		else if (value instanceof Byte) {
			i = Inst.Type.Parse("consti").create((int)(Byte)value);
		}
		else if (value instanceof String) {
			String str = (String) value;
			this.comment("\"" + str + "\"");
			for (byte c : str.getBytes()) {
				this.konst((int) c, ((Integer) (int) c).toString());
			}
			return;
		}
		else {
			throw new IllegalArgumentException("Unhandled constant type.");
		}
		
		i.comment = comment;
		this.instructions.add(i);
	}
	
	@Override
	public void comment(String c) {
		// Ignore comments by default
		Inst i = new Inst(null);
		i.comment = c;
		this.instructions.add(i);
	}
	
	public List<BasicBlock> getBlocks() {
		return this.blocks;
	}
}
