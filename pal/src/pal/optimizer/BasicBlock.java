//
//  Written by Kevin Barabash, Jonathan Clark, Richard Eames, Valentin Tiriac 
//  CMPUT 415, Fall 2011
//
package pal.optimizer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pal.optimizer.peephole.Add0;
import pal.optimizer.peephole.AssignToSelf;
import pal.optimizer.peephole.MuliplyBy1;
import pal.optimizer.peephole.Peephole;

public class BasicBlock {
	public String label;
	public List<Inst> instructions;
	
	private static final List<Peephole> peepingToms = Arrays.asList(
		new Add0(), new MuliplyBy1(), new AssignToSelf()
	);
	
	public BasicBlock() {
		this.label = null;
		this.instructions = new ArrayList<Inst>();
	}
	
	public BasicBlock(String label, List<Inst> instructions) {
		this.label = label;
		this.instructions = instructions;
	}
	
	public void optimize() {
		for (Peephole tom : peepingToms) {
			this.instructions = tom.optimize(this.instructions);
		}
	}
	
	@Override
	public String toString() {
		return "Block(" + (this.label == null ? "" : this.label) + ")\n"
				+ this.instructions.toString();
	}
}
