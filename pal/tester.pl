#!/usr/bin/perl
use 5.10.0;
use strict;
use warnings;
use Getopt::Long;
use Cwd;
use Data::Dumper;

local $" = "'";

my %expected = ();

my $low = 1;
my $high = 50;
my $executable = shift @ARGV;

my $local_path = getcwd();

die 'Need path to executable as first argument' unless $executable;

my $result = GetOptions(
	'low|l=i' => \$low,
	'high|h=i' => \$high,
) or die $!;

# construct the testing path
my $tester_path = $local_path . "/files/tests/";

for my $test_num ($low ... $high) {
	my $test_file = $tester_path . sprintf("test%02d.pal", $test_num);
	
	if (! -f $test_file) {
		warn("Could not find file '$test_file', skipping\n");
		next;
	}
	
	readTestFile($test_file, $test_num);
	
	#say "Running $test_file";
	open my $fp, "java -jar $executable -n $test_file 2>&1 |" or die $!;
		my $expected_errors = $expected{$test_num};
		my $expected_error_count = 0;
		while (my ($k, $v) = each %$expected_errors) {
			$expected_error_count += $v->{count};
		}
		my $success = 1;
		
		#say "Expecting $expected_error_count errors";
		
		while (<$fp>) {
			if (/Error on line (\d+).*?:\s*(.*)$/i) {
				my $lineno = $1;
				my $line_kw = $2;
								
				if (not $expected_errors->{$lineno}) {
					if ($expected_error_count <= 0) {
						say STDERR "An error was found on line $lineno of file $test_num, but was not expected";
						say STDERR "\tError found: $_";
					}
					
					$expected_error_count--;
					next;
				}
				
				my $line_err = $expected_errors->{$lineno}{keywords};
				my $matches = 0;
				for my $word (@$line_err) {
					if (index($line_kw, $word) >= 0) {
						$matches++;
					}
				}
				if ($matches / scalar(@$line_err) < 0.5) {
					#say "Failed line $lineno test expecting match '@$line_err' for '$line_kw'";
				}
				else {
					$expected_error_count--;
				}
			}
		}
		
		#say "There are $expected_error_count errors left";
		
		if ($expected_error_count == 0) {
			say STDERR "$test_num Success"
		}
		else {
			if ($expected_error_count > 0) {
				say STDERR "Still expecting $expected_error_count errors";
			}
			elsif ($expected_error_count < 0) {
				say STDERR "Got " . abs($expected_error_count) ." more errors than expected";
			}
			say STDERR "$test_num Failed";
		}
		
	close $fp; 
	
}

sub readTestFile {
	my ($file_path, $test_num) = @_;
	
	$expected{$test_num} = {};
	
	open my $fp, $file_path or die $!;
	
	while (<$fp>) {
		chomp;
		if (/#!\s*line\s+(\d+)\s*(?:x(\d+))?\s+(.*)$/i) {
			my $lineno = $1;
			my $expected = int ($2 // 1);
			my @keywords = split /\s+/, $3;

			$expected{$test_num}{$lineno} = {
				count => $expected,
				keywords => \@keywords
			};
		}
		elsif (/#!\s*expected\s+(\d+)/i) {
			$expected{$test_num}{expected} = {
				count => int($1 // 0),
				keywords => []
			}
		}
	}
	
	close $fp;
}


