package pal.ours;

import static org.junit.Assert.assertEquals;

import java.util.Queue;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class OurTests {
	
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		this.testContext = new PALTestContext();
	}
	
	public void printOutput(Queue<Object> other) {
		for (Object o : other) {
			System.err.println(o.toString());
		}
	}
	
	@Test
	public void test02() throws Exception {
		this.testContext.compileFile("ours/test02.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test03() throws Exception {
		this.testContext.compileFile("ours/test03.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test04() throws Exception {
		this.testContext.compileFile("ours/test04.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test05() throws Exception {
		this.testContext.compileFile("ours/test05.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test06() throws Exception {
		this.testContext.compileFile("ours/test06.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test07() throws Exception {
		this.testContext.compileFile("ours/test07.pal");
		assertEquals(2, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test08() throws Exception {
		this.testContext.compileFile("ours/test08.pal");
		assertEquals(6, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test09() throws Exception {
		this.testContext.executeFile("ours/test09.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Character('b'), this.testContext.outputQueue.poll());
	}
	
	@Test
	public void test10() throws Exception {
		this.testContext.executeFile("ours/test10.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Character('\u039c'),
				this.testContext.outputQueue.poll());
	}
	
	@Test
	public void test11() throws Exception {
		this.testContext.compileFile("ours/test11.pal");
		assertEquals(2, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test12() throws Exception {
		this.testContext.compileFile("ours/test12.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test13() throws Exception {
		this.testContext.compileFile("ours/test13.pal");
		assertEquals(2, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(2, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test14() throws Exception {
		this.testContext.compileFile("ours/test14.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test15() throws Exception {
		this.testContext.compileFile("ours/test15.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test16() throws Exception {
		this.testContext.compileFile("ours/test16.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(2, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test17() throws Exception {
		this.testContext.compileFile("ours/test17.pal");
		assertEquals(2, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test18() throws Exception {
		this.testContext.compileFile("ours/test18.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test19() throws Exception {
		this.testContext.compileFile("ours/test19.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test20() throws Exception {
		this.testContext.executeFile("ours/test20.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
		
		assertEquals(0f, this.testContext.outputQueue.poll());
		assertEquals(3.40282356E38f, this.testContext.outputQueue.poll());
		assertEquals(1.40129846E-45f, this.testContext.outputQueue.poll());
		assertEquals(1.17549435E-38f, this.testContext.outputQueue.poll());
	}
	
	@Test
	public void test21() throws Exception {
		this.testContext.compileFile("ours/test21.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test22() throws Exception {
		this.testContext.compileFile("ours/test22.pal");
		assertEquals(2, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test23() throws Exception {
		this.testContext.executeFile("ours/test23.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
		String chars = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt placerat odio sollicitudin facilisis. Etiam vitae massa gravida ante mattis sagittis. Nulla egestas nunc sit amet est condimentum vel mattis magna imperdiet. Donec scelerisque auctor ipsum, eget mollis mi massa nunc.";
		
		for (int i = 0; i < chars.length(); i++) {
			assertEquals(chars.charAt(i), this.testContext.outputQueue.poll());
		}
		
	}
	
	@Test
	public void test24() throws Exception {
		this.testContext.compileFile("ours/test24.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test25() throws Exception {
		this.testContext.compileFile("ours/test25.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test26() throws Exception {
		this.testContext.compileFile("ours/test26.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(6, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test27() throws Exception {
		this.testContext.compileFile("ours/test27.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test28() throws Exception {
		this.testContext.compileFile("ours/test28.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test29() throws Exception {
		this.testContext.compileFile("ours/test29.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test30() throws Exception {
		this.testContext.compileFile("ours/test30.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test31() throws Exception {
		this.testContext.compileFile("ours/test31.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test32() throws Exception {
		this.testContext.compileFile("ours/test32.pal");
		assertEquals(2, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test33() throws Exception {
		this.testContext.compileFile("ours/test33.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test34() throws Exception {
		this.testContext.executeFile("ours/test34.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
		
		assertEquals(-1, this.testContext.outputQueue.poll());
		assertEquals(-1, this.testContext.outputQueue.poll());
		assertEquals(1, this.testContext.outputQueue.poll());
		assertEquals(-1, this.testContext.outputQueue.poll());
		assertEquals(-1, this.testContext.outputQueue.poll());
		assertEquals(1, this.testContext.outputQueue.poll());
		assertEquals(0, this.testContext.outputQueue.poll());
	}
	
	@Test
	public void test35() throws Exception {
		this.testContext.compileFile("ours/test35.pal");
		assertEquals(1, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test36() throws Exception {
		this.testContext.compileFile("ours/test36.pal");
		assertEquals(2, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test37() throws Exception {
		this.testContext.compileFile("ours/test37.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test38() throws Exception {
		this.testContext.executeFile("ours/test38.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
		
		assertEquals(0, this.testContext.outputQueue.poll());
		assertEquals(-1, this.testContext.outputQueue.poll());
		assertEquals(99, this.testContext.outputQueue.poll());
	}
	
	@Test
	public void test39() throws Exception {
		this.testContext.executeFile("ours/test39.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
		
	}
	
	@Test
	public void test40() throws Exception {
		this.testContext.executeFile("ours/test40.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
		
		assertEquals((float)3.145449,  testContext.outputQueue.poll());
	}
	
	@Test
	public void test41() throws Exception {
		this.testContext.executeFile("ours/test41.pal");
		assertEquals(2, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(1, this.testContext.compileErrorReport.getErrorCount());
		
	}
	
	@Test
	public void test42() throws Exception {
		this.testContext.executeFile("ours/test42.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(4, this.testContext.compileErrorReport.getErrorCount());
		
	}
	
	@Test
	public void test43() throws Exception {
		this.testContext.executeFile("ours/test43.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
		String chars = "0123456789\n";
		for (int i = 0; i < chars.length(); i++) {
			assertEquals(chars.charAt(i), this.testContext.outputQueue.poll());
		}
		
	}
	
	@Test
	public void test44() throws Exception {
		this.testContext.inputQueue.offer(10);
		this.testContext.inputQueue.offer(13);
		
		this.testContext.executeFile("ours/test44.pal");
		assertEquals(0, this.testContext.parserErrorReport.getErrorCount());
		assertEquals(0, this.testContext.compileErrorReport.getErrorCount());
		
		printOutput(testContext.outputQueue);
		
		assertEquals(10,  testContext.outputQueue.poll());
		assertEquals(13,  testContext.outputQueue.poll());
	}
}
