package pal;

import junit.framework.Assert;

public class PALCustomAssert extends Assert {
	static final float ERROR_EPSILON = 0.0001f;
	static final float RANGE_EPSILON = 0.0001f;
	
	public static void assertRangeSimilar(float value, Object output) {
		Float value2 = (Float)output;
		
		if(value2 == null) {
			failNotSame("", new Float(0.0f), output);
		}
		
		float error = Math.abs(value2 - value);
		
		if(error > RANGE_EPSILON) {
			failNotEquals("", value, value2);
		}
	}
	
	public static void assertSimilar(float value, Object output) {
		Float value2 = (Float)output;
		
		if(value2 == null) {
			failNotSame("", new Float(0.0f), output);
		}
		
		float error = Math.abs((value2 - value) / value);
		
		if(error > ERROR_EPSILON) {
			failNotEquals("", value, value2);
		}
	}
}
