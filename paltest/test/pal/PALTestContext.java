package pal;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.Queue;

import pal.asc.Assembler;
import pal.asc.Program;
import pal.asc.Simulator;
import pal.codegen.ASCBuilder;
import pal.codegen.Compiler;
import pal.common.ErrorReport;
import pal.common.Parser;
import pal.optimizer.BasicBlockBuilder;

public class PALTestContext {
	private final Parser parser;
	private Assembler assembler;
	private Simulator simulator;
	
	private Parser.Result result;
	private String program;
	
	public ErrorReport parserErrorReport;
	public ErrorReport compileErrorReport;
	
	public Queue<Object> inputQueue;
	public Queue<Object> outputQueue;
	
	public PALTestContext() {
		inputQueue = new InputQueue();
		outputQueue = new LinkedList<Object>();
		
		parser = Parser.Instance;
		result = null;
		program = null;
		
		parserErrorReport = null;
		compileErrorReport = null;
	}
	
	public void parseFile(String resourceFilename) throws Exception {
		InputStream stream = null;
		
		try {
			stream = getClass().getResourceAsStream(resourceFilename);
			InputStreamReader reader = new InputStreamReader(stream);
			
			result = parser.parse(reader);
			parserErrorReport = result.report;
		}
		finally {
			if(stream != null) {
				stream.close();
			}
		}
		
		return;
	}
	
	public void compileFile(String resourceFilename) throws Exception {
		parseFile(resourceFilename);
		
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(byteStream);
		
		ASCBuilder builder = new BasicBlockBuilder(ps);
		Compiler compiler = new Compiler(builder, true);
		
		compileErrorReport = compiler.compile(result.program);
		
		program = byteStream.toString();
	}
	
	public void executeFile(String resourceFilename) throws Exception {
		compileFile(resourceFilename);
		
		// Strip carriage returns from the program.
		// (only affects Windows.)
		program = program.replaceAll("\r", "");
		
		//System.out.print(program);
		
		// TODO: Some kind of error maybe?
		if (compileErrorReport.getErrorCount() == 0 && parserErrorReport.getErrorCount() == 0) {
			assembler = new Assembler();
			Program prg = assembler.assemble(program);
			
			simulator = new Simulator();
			simulator.execute(prg, inputQueue, outputQueue);
		}
	}
	
	public class InputQueue extends LinkedList<Object> {
		private static final long serialVersionUID = 6385096921864226289L;
		
		@Override
		public boolean offer(Object obj) {
			if (obj instanceof String) {
				for (Character c : ((String)obj).toCharArray()) {
					this.offer(c);
				}
				return true;
			} else {
				return super.offer(obj);
			}
		}
		
	}
}
