package pal.asc;

public class InstructionFormat {
	private int integerConstant;
	private float realConstant;
	
	private Opcode operation;
	private int regflag;
	private int regnum;
	private int address;
	
	public Opcode getOperation() {
		return operation;
	}
	
	public void setOperation(Opcode operation) {
		this.operation = operation;
	}
	
	public int getRegFlag() {
		return regflag;
	}
	
	public void setRegFlag(int regflag) {
		this.regflag = regflag;
	}
	
	public int getRegNum() {
		return regnum;
	}
	
	public void setRegNum(int regnum) {
		this.regnum = regnum;
	}
	
	public int getAddress() {
		return address;
	}
	
	public void setAddress(int address) {
		this.address = address;
	}
	
	public int getIntegerConstant() {
		return integerConstant;
	}
	
	public void setIntegerConstant(int integerConstant) {
		this.integerConstant = integerConstant;
	}
	
	public float getRealConstant() {
		return realConstant;
	}
	
	public void setRealConstant(float realConstant) {
		this.realConstant = realConstant;
	}
}

