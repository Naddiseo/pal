package pal.asc;

import java.nio.ByteBuffer;
import java.util.Queue;

public class Simulator {
	private static final int ERROR_CYCLES = 5000000;
	private static final int MAXINT = 2147483647;
	private static final int MININT = (-MAXINT - 1);
	private static final int STACKSIZE = 32767;
	private static final int NOOFREGS = 16;
	private static final int KWDSIZE = 7;
	private static final boolean CONT = false;
	private static final int MAXADDR = 0xFFFFF;
	
	private static final int SADRMASK = 0xFFF00000;
	
	private final ByteBuffer stack = ByteBuffer.allocate(STACKSIZE * 4);
	private int sp;
	private int ic;
	private final int[] reg = new int[NOOFREGS];
	private int tracing;
	private Opcode action;
	private int rf;
	private int rn;
	private int aval;
	private int mfree;
	private int mlast;
	
	private int ncycles;
	
	int c;
	int	listflag = 0;
	
	public Simulator() {
		return;
	}
	
	int caddr() {
		int k;
		
		/* Calculate stack address */
		if(rf != 0) {
			k = reg[rn] + aval; 
		}
		else {
			k = aval;
		}
		
		if((k < 0) || (k >= STACKSIZE)) {
			rerror(new AscRuntimeAddressOutsideStackException(), k);
		}
		
		return k;
	}
	
	int caddi() {
		int k;
		
		/* Calculate stack address */
		
		if(sp < 0) {
			rerror(new AscRuntimeEmptyStackException(), null);
		}
		
		if(rf != 0) {
			k = reg[rn] + stack.getInt(sp * 4);
		}
		else {
			k = stack.getInt(sp * 4);
		}
		
		if((k < 0) || (k >= STACKSIZE)) {
			rerror(new AscRuntimeAddressOutsideStackException(), k);
		}
		
		return k;
	}
	
	void csph() {
		/* Validate stack pointer */
		if(sp >= mlast - 1) {
			rerror(new AscRuntimeStackOverflowException(), sp);
		}
	}
	
	void cspl() {
		/* Validate stack pointer */
		if(sp < 0) {
			rerror(new AscRuntimeStackUnderflowException(), sp);
		}
	}
	
	void cspa() {
		/* Validate stack pointer */
		if(sp < 1) {
			rerror(new AscRuntimeInsufficientArgsException(), null);
		}
	}

	boolean error;
	
	boolean addisubierr(int a, int b, int r) {
		if(a > 0 && b > 0 && !(r > 0)) {
			return true;
		}
		
		if(a < 0 && b < 0 && !(r < 0)) {
			return true;
		}
		
		return false;
	}
	
	public void execute(Program prg, Queue<Object> inputQueue, Queue<Object> outputQueue) {
		boolean infmode;
		int k, l, m, p, op;
		double auxdouble;
		float aux1_real;
		float aux2_real;
		Object nextInput;
		Integer integerInput;
		Float realInput;
		Character characterInput;
		
		sp = -1;	/* Stack initially empty */
		ic = 0;		/* Instruction counter initially zero */
		mfree = 0;	/* Free list for ALLOC / FREE */
		mlast = STACKSIZE;	/* Last used by ALLOC */
		tracing = 0;	/* Tracing turned off */
		
		// signal(SIGFPE, sigfpe);  ------ Java doesn't generate floating point exceptions.
		
		// Clear registers
		for(k = 0; k < NOOFREGS; ++k) {
			reg[k] = 0;
		}
		
		// Clear cycle count.
		ncycles = 0;
		
		while(true) {
			/* Main loop */
			
			++ncycles;
			if(ncycles > ERROR_CYCLES) {
				throw new AscRuntimeTooLongException();
			}
			
			InstructionFormat rni = prg.getCode()[ic]; /* Read next instruction */
			
			action = rni.getOperation();
			rf = rni.getRegFlag();
			rn = rni.getRegNum();
			aval = rni.getAddress();
			
			if(aval > MAXADDR) {
				aval |= SADRMASK;
			}
			
			if(tracing != 0) {
				tracing--;
				infmode = true;
			}
			else {
				infmode = false;
			}
			
			
			switch(action) {
			
			
			case PUSH:
				error = false;
				csph();
				
				stack.putInt((++sp) * 4, stack.getInt((k = caddr()) * 4));
				
				if(infmode) {
					trace("PUSH", stack.getInt(sp * 4), "from", k);
				}
				
				break;
				
			case PUSHI:
				error = false;
				
				stack.putInt(sp * 4, stack.getInt((k = caddi()) * 4));
				
				if(infmode) {
					trace("PUSHI", stack.getInt(sp * 4), "from", k);
				}
				
				break;
				
			case PUSHA:
				error = false;
				csph();
				
				stack.putInt((++sp) * 4, caddr());
				
				if(infmode) {
					trace("PUSHA", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case DUP:
				error = false;
				csph();
				cspl();
				
				stack.putInt((sp + 1) * 4, stack.getInt(sp * 4));
				sp++;
				
				if(infmode) {
					trace("DUP", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case POP:
				error = false;
				cspl();
				
				stack.putInt((k = caddr()) * 4, stack.getInt((sp--) * 4));
				
				if(infmode) {
					trace("POP", stack.getInt(k * 4), "to", k);
				}
				
				break;
				
			case POPI:
				error = false;
				cspl();
				
				l = stack.getInt((sp--) * 4);
				
				stack.putInt((k = caddi()) * 4, l);
				
				sp--;
				
				if(infmode) {
					trace("POPI", l, "to", k);
				}
				
				break;
			
			case CONSTI:
				error = false;
				csph();
				
				stack.putInt((++sp) * 4, prg.getCode()[aval].getIntegerConstant());
				
				if(infmode) {
					trace("CONSTI", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case CONSTR:
				error = false;
				csph();
				
				stack.putFloat((++sp) * 4, prg.getCode()[aval].getRealConstant());
				
				if(infmode) {
					tracer("CONSTR", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case ADJUST:
				error = false;
				sp += prg.getCode()[aval].getIntegerConstant();
				
				csph();
				
				if(infmode) {
					trace("ADJUST", sp, null, 0);
				}
				
				break;
				
			case ALLOC:
				error = false;
				
				if((k = prg.getCode()[aval].getIntegerConstant()) <= 0) {
					rerror(new AscRuntimeBadAllocException(), k);
				}
				
				for(l = mfree, m = 0; l != 0; l = stack.getInt((l + 1) * 4)) {
					if(stack.getInt(l * 4) >= k) {
						break;
					}
					
					m = l;
				}
				
				if(l != 0) {
					if(m == 0) {
						mfree = stack.getInt((l + 1) * 4);
					}
					else {
						stack.putInt((m + 1) * 4, stack.getInt((l + 1) * 4));
					}
				}
				else {
					if((l = (mlast -= k + 1)) <= sp + 1) {
						rerror(new AscRuntimeOutOfMemoryException(), null);
					}
					
					stack.putInt(l * 4, k);
				}
				
				stack.putInt((++sp) * 4, l + 1);
				
				if(infmode) {
					trace("ALLOC", k, "at", l + 1);
				}
				
				break;
				
			case SALLOC:
				error = false;
				
				if((k = stack.getInt(sp * 4)) <= 0) {
					rerror(new AscRuntimeBadAllocException(), k);
				}
				
				for(l = mfree, m = 0; l != 0; l = stack.getInt((l + 1) * 4)) {
					if(stack.getInt(l * 4) >= k) {
						break;
					}
					
					m = l;
				}
				
				if(l != 0) {
					if(m == 0) {
						mfree = stack.getInt((l + 1) * 4);
					}
					else {
						stack.putInt((m + 1) * 4, stack.getInt((l + 1) * 4));
					}
				}
				else {
					if((l = (mlast -= k+1)) <= sp + 1) {
						rerror(new AscRuntimeOutOfMemoryException(), null);
					}
					
					stack.putInt(l * 4, k);
				}
				
				stack.putInt(sp * 4, l + 1);
				
				if(infmode) {
					trace("SALLOC", k, "at", l + 1);
				}
				
				break;
				
			case FREE:
				error = false;
				cspl();
				
				p = stack.getInt((sp--) * 4) - 1;
				
				if((p <= sp) || (p > STACKSIZE - 2) || ((k = stack.getInt(p * 4)) < 1) ||
						(k >= STACKSIZE)) {
					rerror(new AscRuntimeIllegalFreeException(), k);
				}
				
				if(p == mlast) {
					mlast += k + 1;
				}
				else {
					for(l = mfree, m = 0; l != 0; l = stack.getInt((l + 1) * 4)) {
						if(stack.getInt(l * 4) >= k) {
							break;
						}
						
						m = l;
					}
					
					if(m == 0) {
						stack.putInt((p + 1) * 4, mfree);
						mfree = p;
					}
					else {
						stack.putInt((m + 1) * 4, p);
						stack.putInt((p + 1) * 4, l);
					}
				}
				
				if(infmode) {
					trace("FREE", k, "at", p);
				}
				
				break;
				
			case ADDI:
				cspa();
				
				op = stack.getInt((sp - 1) * 4);
				
				stack.putInt((sp - 1) * 4, stack.getInt(sp * 4) + stack.getInt((sp - 1) * 4));
				
				error = addisubierr(stack.getInt(sp * 4), op, stack.getInt((sp - 1) * 4));
				
				sp--;
				
				if(infmode) {
					trace("ADDI", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case ADDR:
				error = false;
				cspa();
				
				aux1_real = stack.getFloat(sp * 4);
				aux2_real = stack.getFloat((sp - 1) * 4);
				aux1_real += aux2_real;
				
				stack.putFloat((--sp) * 4, aux1_real);
				
				if(infmode) {
					tracer("ADDR", stack.getFloat(sp * 4), null, 0);
				}
				
				break;
				
			case SUBI:
				cspa();
				
				op = stack.getInt((sp - 1) * 4);
				
				stack.putInt((sp - 1) * 4, stack.getInt((sp - 1) * 4) - stack.getInt(sp * 4));
				
				error = addisubierr(-stack.getInt(sp * 4), op, stack.getInt((sp - 1) * 4));
				
				sp--;
				
				if(infmode) {
					trace("SUBI", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case SUBR:
				error = false;
				cspa();
				
				aux1_real = stack.getFloat((sp - 1) * 4);
				aux2_real = stack.getFloat(sp * 4);
				
				aux1_real -= aux2_real;
				
				stack.putFloat((--sp) * 4, aux1_real);
				
				if(infmode) {
					tracer("SUBR", stack.getFloat(sp * 4), null, 0);
				}
				
				break;
				
			case MULI:
				cspa();
				auxdouble = (double)stack.getInt((sp - 1) * 4) * (double)stack.getInt(sp * 4);
				
				stack.putInt((sp - 1) * 4, stack.getInt((sp - 1) * 4) * stack.getInt(sp * 4));
				
				error = (auxdouble != stack.getInt((sp - 1) * 4));
				
				sp--;
				
				if(infmode) {
					trace("MULI", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case MULR:
				error = false;
				cspa();
				
				aux1_real = stack.getFloat((sp - 1) * 4);
				aux2_real = stack.getFloat(sp * 4);
				
				aux1_real *= aux2_real;
				
				stack.putFloat((--sp) * 4, aux1_real);
				
				if(infmode) {
					tracer("MULR", stack.getFloat(sp * 4), null, 0);
				}
				break;
				
			case DIVI:
				error = ((stack.getInt(sp * 4) == 0) ||
						(stack.getInt((sp - 1) * 4) == MININT && stack.getInt(sp * 4) == -1));
				cspa();
				
				if(!error) {
					stack.putInt((sp - 1) * 4, stack.getInt((sp - 1) * 4) / stack.getInt(sp * 4));
				}
				
				sp--;
				
				if(infmode) {
					trace("DIVI", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case DIVR:
				error = false;
				cspa();
				
				aux1_real = stack.getFloat((sp - 1) * 4);
				aux2_real = stack.getFloat(sp * 4);
				
				aux1_real /= aux2_real;
				
				stack.putFloat((--sp) * 4, aux1_real);
				
				if(infmode) {
					tracer("DIVR", stack.getFloat(sp * 4), null, 0);
				}
				
				break;
				
			case MOD:
				error = ((stack.getInt(sp * 4) == 0) ||
						(stack.getInt((sp - 1) * 4) == MININT && stack.getInt(sp * 4) == -1));
				cspa();
				
				if(!error) {
					stack.putInt((sp - 1) * 4, stack.getInt((sp - 1) * 4) % stack.getInt(sp * 4));
				}
				
				sp--;
				
				if(infmode) {
					trace("MOD", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case ITOR:
				error = false;
				cspl();
				
				aux1_real = stack.getInt(sp * 4);
				stack.putFloat(sp * 4, aux1_real);
				
				if(infmode) {
					tracer("ITOR", stack.getFloat(sp * 4), null, 0);
				}
				
				break;
				
			case RTOI:
				aux1_real = stack.getInt(sp * 4);
				
				error = (aux1_real > (double)MAXINT) ||
						(aux1_real < (double)MININT);
				
				cspl();
				
				stack.putInt(sp * 4, (int)aux1_real);
				
				if(infmode) {
					trace("RTOI", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case EQI:
				error = false;
				cspa();
				
				stack.putInt((sp - 1) * 4, (stack.getInt((sp - 1) * 4) == stack.getInt(sp * 4)) ? 1 : 0);
				
				sp--;
				
				if(infmode) {
					trace("EQI", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case EQR:
				error = false;
				cspa();
				
				aux1_real = stack.getFloat((sp - 1) * 4);
				aux2_real = stack.getFloat(sp * 4);
				
				stack.putInt((--sp) * 4, (aux1_real == aux2_real) ? 1 : 0);
				
				if(infmode) {
					trace("EQR", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case LTI:
				error = false;
				cspa();
				
				stack.putInt((sp - 1) * 4, (stack.getInt((sp - 1) * 4) < stack.getInt(sp * 4)) ? 1 : 0);
				
				sp--;
				
				if(infmode) {
					trace("LTI", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case LTR:
				error = false;
				cspa();
				
				aux1_real = stack.getFloat((sp - 1) * 4);
				aux2_real = stack.getFloat(sp * 4);
				
				stack.putInt((--sp) * 4, (aux1_real < aux2_real) ? 1 : 0);
				
				if(infmode) {
					trace("LTR", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case GTI:
				error = false;
				cspa();
				
				stack.putInt((sp - 1) * 4, (stack.getInt((sp - 1) * 4) > stack.getInt(sp * 4)) ? 1 : 0);
				
				sp--;
				
				if(infmode) {
					trace("GTI", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case GTR:
				error = false;
				cspa();
				
				aux1_real = stack.getFloat((sp - 1) * 4);
				aux2_real = stack.getFloat(sp * 4);
				
				stack.putInt((--sp) * 4, (aux1_real > aux2_real) ? 1 : 0);
				
				if(infmode) {
					trace("GTR", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case OR:
				error = false;
				cspa();
				
				stack.putInt((sp - 1) * 4, (stack.getInt((sp - 1) * 4) != 0 || stack.getInt(sp * 4) != 0) ? 1 : 0);
				
				sp--;
				
				if(infmode) {
					trace("OR", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case AND:
				error = false;
				cspa();
				
				stack.putInt((sp - 1) * 4, (stack.getInt((sp - 1) * 4) != 0 && stack.getInt(sp * 4) != 0) ? 1 : 0);
				
				sp--;
				
				if(infmode) {
					trace("AND", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case NOT:
				error = false;
				cspa();
				
				stack.putInt(sp * 4, (stack.getInt(sp * 4) != 0) ? 0 : 1);
				
				if(infmode) {
					trace("NOT", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
			case IFZ:
				error = false;
				cspl();
				
				if(stack.getInt((sp--) * 4) == 0) {
					ic = aval - 1;
				}
				
				if(infmode) {
					trace("IFZ", stack.getInt((sp + 1) * 4), "to", aval);
				}
				
				break;
				
			case IFNZ:
				error = false;
				cspl();
				
				if(stack.getInt((sp--) * 4) != 0) {
					ic = aval - 1;
				}
				
				if(infmode) {
					trace("IFNZ", stack.getInt((sp + 1) * 4), "to", aval);
				}
				
				break;
				
			case GOTO:
				error = false;
				ic = aval - 1;
				
				if(infmode) {
					trace("GOTO", 0, "to", aval);
				}
				
				break;
				
			case STOP:
				return;
				
			case CALL:
				error = false;
				csph();
				
				stack.putInt((++sp) * 4, ic);
				
				csph();
				
				stack.putInt((++sp) * 4, reg[rn]);
				
				ic = aval - 1;
				
				reg[rn] = sp + 1;
				
				if(infmode) {
					trace("CALL", rn, "to", aval);
				}
				
				break;
				
			case RET:
				error = false;
				cspa();
				
				reg[rn] = stack.getInt((sp--) * 4);
				
				ic = stack.getInt((sp--) * 4);
				
				if(infmode) {
					trace("RET", rn, "to", ic + 1);
				}
				
				break;
				
			case READI:
				csph();
				
				nextInput = inputQueue.poll();
				
				if(nextInput instanceof Integer) {
					integerInput = (Integer)nextInput;
					stack.putInt((++sp) * 4, integerInput);
					error = false;
				}
				else if (nextInput instanceof Long) {
					integerInput = ((Long)nextInput).intValue();
					stack.putInt((++sp) * 4, integerInput);
					error = false;
				}
				else if (nextInput instanceof String) {
					integerInput = Integer.valueOf((String) nextInput);
					stack.putInt((++sp) * 4, integerInput);
					error = false;
				}
				else {
					error = true;
					stack.putInt((++sp) * 4, 0);
				}
				
				if(infmode) {
					trace("READI", stack.getInt(sp * 4), null, 0);
				}
				
				break;
			
			case READR:
				csph();
				
				nextInput = inputQueue.poll();
				
				if(nextInput instanceof Float) {
					realInput = (Float)nextInput;
					stack.putFloat((++sp) * 4, realInput);
					error = false;
				}
				else if (nextInput instanceof Double) {
					realInput = ((Double)nextInput).floatValue();
					stack.putFloat((++sp) * 4, realInput);
					error = false;
				}
				else if (nextInput instanceof String) {
					realInput = Float.valueOf((String) nextInput);
					stack.putFloat((++sp) * 4, realInput);
					error = false;
				}
				else {
					error = true;
					stack.putFloat((++sp) * 4, 0.0f);
				}
				
				if(infmode) {
					tracer("READF", stack.getFloat(sp * 4), null, 0);
				}
				
				break;
				
			case READC:
				csph();
				
				nextInput = inputQueue.poll();
				
				if(nextInput instanceof Character) {
					characterInput = (Character)nextInput;
					stack.putInt((++sp) * 4, characterInput);
					error = false;
				}
				else {
					error = true;
					stack.putInt((++sp) * 4, 0);
				}
				
				if(infmode) {
					trace("READC", stack.getInt(sp * 4), null, 0);
				}
				
				break;
				
				
			case WRITEI:
				error = false;
				cspl();
				
				outputQueue.offer(new Integer(stack.getInt((sp--) * 4)));
				
				if(infmode) {
					trace("\nWRITEI", stack.getInt((sp + 1) * 4), null, 0);
				}
				
				break;
				
			case WRITER:
				error = false;
				cspl();
				
				outputQueue.offer(new Float(stack.getFloat((sp--) * 4)));
				
				if(infmode) {
					tracer("\nWRITER", stack.getFloat((sp + 1) * 4), null, 0);
				}
				
				break;
				
			case WRITEC:
				error = false;
				cspl();
				
				outputQueue.offer(new Character((char)stack.getInt((sp--) * 4)));
				
				if(infmode) {
					tracer("\nWRITEC", stack.getInt((sp + 1) * 4), null, 0);
				}
				
				break;
				
				/* Debugging aids */
				
			case TRACE:
				error = false;
				tracing = prg.getCode()[aval].getIntegerConstant();
				break;
				
			case DUMP:
				error = false;
				dumpstack(CONT);
				break;
				
			case IFERR:
				if(error) {
					ic = aval - 1;
				}
				
				if(infmode) {
					trace("IFERR", error ? 1 : 0, "to", aval);
				}
				
				error = false;
				break;
				
			default:
				rerror(new AscRuntimeIllegalInstructionException(), null);
			}
			
			ic++;
		}
				
	}

	void dumpstack(boolean act) {
		System.out.print("\n\n                    STACK CONTENTS\n");
		System.out.print(    "                    ==============\n\n");
		System.out.print("Location     Integer        Octal       Floating     Char\n");
		
		for(int k = (act ? ((sp > 19) ? (sp - 20) : 0) : 0); k <= sp; ++k) {
			int k_index = k * 4;
			
			int aux2integer = stack.getInt(k_index);
			float aux2real = stack.getFloat(k_index);
			byte aux1pattern_ca = stack.get(k_index);
			byte aux1pattern_cb = stack.get(k_index + 1);
			byte aux1pattern_cc = stack.get(k_index + 2);
			byte aux1pattern_cd = stack.get(k_index + 3);
			
			System.out.printf("%8d %11d  %11o %14e     %c%c%c%c\n",
					k, aux2integer, aux2integer, aux2real,
					((aux1pattern_ca > 31) && (aux1pattern_ca < 127)) ? aux1pattern_ca : '-',
					((aux1pattern_cb > 31) && (aux1pattern_cb < 127)) ? aux1pattern_cb : '-',
					((aux1pattern_cc > 31) && (aux1pattern_cc < 127)) ? aux1pattern_cc : '-',
					((aux1pattern_cd > 31) && (aux1pattern_cd < 127)) ? aux1pattern_cd : '-');
		}
	}
	
	void pfs(String s, int n) {
		System.out.print(s);
		for(int i = s.length(); i < n; ++i) {
			System.out.print(' ');
		}
	}

	void trace(String s1, int n1, String s2, float n2) {
		pfs(s1, KWDSIZE);
		
		System.out.printf("(%1d o'%1o') ", n1, n1);
		if(s2 != null) {
			pfs(s2, 5);
			System.out.printf("(%1d o'%1o') ", n2, n2);
		}
		
		System.out.printf(" sp = (%1d o'%1o') ic = (%1d o'%1o')", sp, sp, ic, ic);
		System.out.println();
	}
	
	void tracer(String s1, float n1, String s2, float n2) {
		pfs(s1, KWDSIZE);
		
		System.out.printf("(%g o'%1o') ", n1, n1);
		if(s2 != null) {
			pfs(s2, 5);
			System.out.printf("(%g o'%1o') ", n2, n2);
		}
		
		System.out.printf(" sp = (%1d o'%1o') ic = (%1d o'%1o')", sp, sp, ic, ic);
		System.out.println();
	}

	void rerror(RuntimeException e, Integer q) {		
		throw e;
	}
}
