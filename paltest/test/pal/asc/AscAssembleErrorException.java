package pal.asc;

public class AscAssembleErrorException extends Exception {
	private static final long serialVersionUID = 1L;
	
	private final int errors;
	
	public AscAssembleErrorException(int errors) {
		this.errors = errors;
	}
	
	public int getErrorCount() {
		return errors;
	}
}
