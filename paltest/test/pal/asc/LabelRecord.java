package pal.asc;

import java.util.ArrayList;
import java.util.List;

public class LabelRecord {
	final String name;
	int address;
	boolean defined = false;
	List<InstructionFormat> references = new ArrayList<InstructionFormat>();
	
	public LabelRecord(String name, int address, boolean defined) {
		this.name = name;
		this.address = address;
		this.defined = defined;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAddress() {
		return address;
	}
	
	public void setAddress(int address) {
		this.address = address;
	}
	
	public boolean getDefined() {
		return defined;
	}
	
	public void setDefined(boolean defined) {
		this.defined = defined;
	}
	
	public List<InstructionFormat> getReferences() {
		return references;
	}
}
