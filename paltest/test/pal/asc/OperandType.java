package pal.asc;

public enum OperandType {
	ADDRESS,
	REGISTER,
	RABSENT,
	INT,
	FLOAT,
	LABEL,
	ABSENT
}
