package pal.asc;

public class Instruction {
	final String keyword;
	final OperandType operand1;
	final OperandType operand2;
	final Opcode opcode;
	
	public Instruction(String keyword, OperandType operand1, OperandType operand2, Opcode opcode) {
		this.keyword = keyword;
		this.operand1 = operand1;
		this.operand2 = operand2;
		this.opcode = opcode;
	}
}
