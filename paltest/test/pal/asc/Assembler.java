package pal.asc;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Assembler {
	private static final int MAXINT = 2147483647;
	private static final int MININT = (-MAXINT - 1);
	private static final int CODESIZE = 32767;
	private static final int NOOFREGS = 16;
	private static final int KWDSIZE = 7;
	private static final boolean CONT = false;
	private static final boolean ABORT = true;
	private static final int MAXADDR = 0xFFFFF;
	private static final int LABLENGTH = 16;
	private static final int ADDRMASK = 0x1FFFFF;
	private static final Map<String, Instruction> instructions = new HashMap<String, Instruction>();

	private int errcnt;
	private int org;
	private int curline;
	private int endconsts;
	private Map<String, LabelRecord> labels = new HashMap<String, LabelRecord>();
	
	int c;
	Program prg;
	
	static {
		instructions.put("PUSH", new Instruction("PUSH", OperandType.ADDRESS, OperandType.ABSENT, Opcode.PUSH));
		instructions.put("PUSHI", new Instruction("PUSHI", OperandType.RABSENT, OperandType.ABSENT, Opcode.PUSHI));
		instructions.put("PUSHA", new Instruction("PUSHA",OperandType.ADDRESS, OperandType.ABSENT, Opcode.PUSHA));
		instructions.put("POP", new Instruction("POP", OperandType.ADDRESS, OperandType.ABSENT, Opcode.POP));
		instructions.put("POPI", new Instruction("POPI", OperandType.RABSENT, OperandType.ABSENT, Opcode.POPI));
		instructions.put("CONSTI", new Instruction("CONSTI", OperandType.INT, OperandType.ABSENT, Opcode.CONSTI));
		instructions.put("CONSTR", new Instruction("CONSTR", OperandType.FLOAT, OperandType.ABSENT, Opcode.CONSTR));
		instructions.put("ADJUST", new Instruction("ADJUST", OperandType.INT, OperandType.ABSENT, Opcode.ADJUST));
		instructions.put("ALLOC", new Instruction("ALLOC", OperandType.INT, OperandType.ABSENT, Opcode.ALLOC));
		instructions.put("FREE", new Instruction("FREE", OperandType.ABSENT, OperandType.ABSENT, Opcode.FREE));
		instructions.put("ADDI", new Instruction("ADDI", OperandType.ABSENT, OperandType.ABSENT, Opcode.ADDI));
		instructions.put("ADDR", new Instruction("ADDR", OperandType.ABSENT, OperandType.ABSENT, Opcode.ADDR));
		instructions.put("SUBI", new Instruction("SUBI", OperandType.ABSENT, OperandType.ABSENT, Opcode.SUBI));
		instructions.put("SUBR", new Instruction("SUBR", OperandType.ABSENT, OperandType.ABSENT, Opcode.SUBR));
		instructions.put("MULI", new Instruction("MULI", OperandType.ABSENT, OperandType.ABSENT, Opcode.MULI));
		instructions.put("MULR", new Instruction("MULR", OperandType.ABSENT, OperandType.ABSENT, Opcode.MULR));
		instructions.put("DIVI", new Instruction("DIVI", OperandType.ABSENT, OperandType.ABSENT, Opcode.DIVI));
		instructions.put("DIVR", new Instruction("DIVR", OperandType.ABSENT, OperandType.ABSENT, Opcode.DIVR));
		instructions.put("MOD", new Instruction("MOD", OperandType.ABSENT, OperandType.ABSENT, Opcode.MOD));
		instructions.put("ITOR", new Instruction("ITOR", OperandType.ABSENT, OperandType.ABSENT, Opcode.ITOR));
		instructions.put("RTOI", new Instruction("RTOI",	OperandType.ABSENT, OperandType.ABSENT, Opcode.RTOI));
		instructions.put("EQI", new Instruction("EQI", OperandType.ABSENT, OperandType.ABSENT, Opcode.EQI));
		instructions.put("EQR", new Instruction("EQR", OperandType.ABSENT, OperandType.ABSENT, Opcode.EQR));
		instructions.put("LTI", new Instruction("LTI", OperandType.ABSENT, OperandType.ABSENT, Opcode.LTI));
		instructions.put("LTR", new Instruction("LTR", OperandType.ABSENT, OperandType.ABSENT, Opcode.LTR));
		instructions.put("GTI", new Instruction("GTI", OperandType.ABSENT, OperandType.ABSENT, Opcode.GTI));
		instructions.put("GTR", new Instruction("GTR", OperandType.ABSENT, OperandType.ABSENT, Opcode.GTR));
		instructions.put("OR", new Instruction("OR", OperandType.ABSENT, OperandType.ABSENT, Opcode.OR));
		instructions.put("AND", new Instruction("AND", OperandType.ABSENT, OperandType.ABSENT, Opcode.AND));
		instructions.put("NOT", new Instruction("NOT", OperandType.ABSENT, OperandType.ABSENT, Opcode.NOT));
		instructions.put("IFZ", new Instruction("IFZ", OperandType.LABEL, OperandType.ABSENT, Opcode.IFZ));
		instructions.put("IFNZ", new Instruction("IFNZ", OperandType.LABEL, OperandType.ABSENT, Opcode.IFNZ));
		instructions.put("GOTO", new Instruction("GOTO", OperandType.LABEL, OperandType.ABSENT, Opcode.GOTO));
		instructions.put("STOP", new Instruction("STOP", OperandType.ABSENT, OperandType.ABSENT, Opcode.STOP));
		instructions.put("CALL", new Instruction("CALL", OperandType.REGISTER, OperandType.LABEL, Opcode.CALL));
		instructions.put("RET", new Instruction("RET", OperandType.REGISTER, OperandType.ABSENT, Opcode.RET));
		instructions.put("READI", new Instruction("READI", OperandType.ABSENT, OperandType.ABSENT, Opcode.READI));
		instructions.put("READR", new Instruction("READR", OperandType.ABSENT, OperandType.ABSENT, Opcode.READR));
		instructions.put("READC", new Instruction("READC", OperandType.ABSENT, OperandType.ABSENT, Opcode.READC));
		instructions.put("WRITEI", new Instruction("WRITEI", OperandType.ABSENT, OperandType.ABSENT, Opcode.WRITEI));
		instructions.put("WRITER", new Instruction("WRITER", OperandType.ABSENT, OperandType.ABSENT, Opcode.WRITER));
		instructions.put("WRITEC", new Instruction("WRITEC", OperandType.ABSENT, OperandType.ABSENT, Opcode.WRITEC));
		instructions.put("TRACE", new Instruction("TRACE", OperandType.INT, OperandType.ABSENT, Opcode.TRACE));
		instructions.put("DUMP", new Instruction("DUMP", OperandType.ABSENT, OperandType.ABSENT, Opcode.DUMP));
		instructions.put("DUP", new Instruction("DUP", OperandType.ABSENT, OperandType.ABSENT, Opcode.DUP));
		instructions.put("SALLOC", new Instruction("SALLOC", OperandType.ABSENT, OperandType.ABSENT, Opcode.SALLOC));
		instructions.put("IFERR", new Instruction("IFERR", OperandType.LABEL, OperandType.ABSENT, Opcode.IFERR));
	}
	
	public Program assemble(String program) throws AscAssembleErrorException, IOException {
		InputStream stream = new ByteArrayInputStream(program.getBytes());
		return assemble(stream);
	}
	
	public Program assemble(InputStream stream) throws AscAssembleErrorException, IOException {
		InputStreamReader reader = new InputStreamReader(stream);
		return assemble(reader);
	}
	
	public Program assemble(InputStreamReader reader) throws AscAssembleErrorException, IOException {
		prg = new Program();
		internalAssemble(reader);
		
		if(errcnt != 0) {
			throw new AscAssembleErrorException(errcnt);
		}
		
		return prg;
	}
	
	private String getlabel(InputStreamReader getc) throws IOException {
		char[] labname = new char[LABLENGTH];
		
		labname[0] = (char)c;
		for(int k = 0; ((c = getc.read()) != ' ') && (c != '\t') &&
				(c != '\n') && (c != '#') && (c != '!'); ) {
			
			if((++k) < LABLENGTH) {
				labname[k] = (char)c;
			}
		}
		
		return (new String(labname)).trim().toUpperCase();
	}
	
	LabelRecord findlabel(String name) {
		if(labels.containsKey(name)) {
			return labels.get(name);
		}
		else {
			return null;
		}
	}
	
	boolean deflabel(InputStreamReader getc) throws IOException {
		String lname = getlabel(getc); /* Read label */
		
		LabelRecord labelRecord = findlabel(lname);
		
		if(labelRecord != null) {
			if(labelRecord.getDefined()) {
				/* Label already defined */
				cerror("Duplicate label", CONT, getc);
				return false;
			}
			
			for(InstructionFormat fmt : labelRecord.getReferences()) {
				fmt.setAddress(org);
			}
			
			/* Flag label as defined */
			labelRecord.setDefined(true);
			labelRecord.setAddress(org);
		}
		else {
			labels.put(lname, new LabelRecord(lname, org, true));
		}
		
		return true;
	}
	
	void skiptoeol(InputStreamReader getc) throws IOException {
		while((c != '\n') && (c != -1)) {
			c = getc.read();
		}
	}
	
	void skipspaces(InputStreamReader getc) throws IOException {
		while((c == ' ') || (c == '\t')) {
			c = getc.read();
		}
	}
	
	Instruction mnemonic(InputStreamReader getc) throws IOException {
		char[] mnem = new char[KWDSIZE];
		
		mnem[0] = (char)c;
		for(int k = 0; ((c = getc.read()) != ' ') && (c != '\t') &&
				(c != '\n') && (c != '#') && (c != '!'); ) {
			if((++k) < KWDSIZE - 1) {
				mnem[k] = (char)c;
			}
			else {
				cerror("Instruction mnemonic too long", CONT, getc);
				return null;
			}
		}
		
		String mnemonic = new String(mnem).trim().toUpperCase();
		
		/* Find the mnemonic in the dictionary */
		if(instructions.containsKey(mnemonic)) {
			return instructions.get(mnemonic);
		}
		else {
			cerror("Illegal instruction", CONT, getc);
			return null;
		}
	}
	
	boolean notdigit() {
		return !Character.isDigit(c);
	}
	
	Integer getint(InputStreamReader getc) throws IOException {
		int n = 0;
		boolean f = (c == '-') ? false : true;
		if((c == '-') || (c=='+')) {
			c = getc.read();
			skipspaces(getc);
		}
		
		if(notdigit()) {
			cerror("Number expected", CONT, getc);
			return null;
		}
		
		for(n = 0; !notdigit(); ) {
			n = n * 10 - (c - '0');
			if((n > 0) || (f && (n == MININT))) {
				cerror("Integer constant overflow", CONT, getc);
				return null;
			}
			
			c = getc.read();
		}
		
		return f ? -n : n;
	}
	
	Double getflt(InputStreamReader getc) throws IOException {
		boolean f = (c == '-') ? false : true;
		
		if((c == '-') || (c == '+')) {
			c = getc.read();
			skipspaces(getc);
		}
		
		if(notdigit()) {
			cerror("Number expected", CONT, getc);
			return null;
		}
		
		StringBuilder ff = new StringBuilder();
		ff.append((char)c);
		
		while((c = getc.read()) == '+' || c == '-' || c == '.' || c == 'e' || c == 'E' ||
				Character.isDigit(c)) {
			ff.append((char)c);
		}
		
		double v = Double.parseDouble(ff.toString());
		
		if(!f) {
			v = -v;
		}
		
		return v;
	}
	
	boolean operand(OperandType otype, InstructionFormat cinstr, InputStreamReader getc) throws IOException {
		Integer n;
		String lname;
		Double u;
		
		switch(otype) {
		case ABSENT:
			return true;
			
		case ADDRESS:
			if(notdigit() && (c != '[') && (c != '+') && (c != '-')) {
				cerror("Bad address format", CONT, getc);
				return false;
			}
						
			if(c == '[') {
				n = 0;
			}
			else {
				n = getint(getc);
				if(n == null) {
					return false;
				}
			}
			
			if((n > MAXADDR) || (n < -MAXADDR - 1)) {
				cerror("Address constant out of range", CONT, getc);
				return false;
			}
			
			cinstr.setAddress(n & ADDRMASK);
			
			skipspaces(getc);
			
			if(c == '[') {
				c = getc.read();
				cinstr.setRegFlag(1);
				skipspaces(getc);
				
				if(notdigit()) {
					cerror("[] format bad", CONT, getc);
					return false;
				}
				
				for(n = 0; !notdigit(); ) {
					n = n * 10 + (c - '0');
					if(n >= NOOFREGS) {
						cerror("[] register number too big", CONT, getc);
						return false;
					}
					
					c = getc.read();
				}
				
				cinstr.setRegNum(n);
				
				skipspaces(getc);
				
				if(c != ']') {
					cerror("No matching ']'", CONT, getc);
					return false;
				}
				
				c = getc.read();
			}
			
			return true;
			
		case REGISTER:
		case RABSENT:
			
			if(notdigit()) {
				if(otype == OperandType.RABSENT) {
					return true;
				}
				
				cerror("Register number expected", CONT, getc);
				return false;
			}
			
			for(n = 0; !notdigit(); ) {
				n = n * 10 + (c - '0');
				if(n >= NOOFREGS) {
					cerror("Register number too big", CONT, getc);
					return false;
				}
				
				c = getc.read();
			}
			
			cinstr.setRegNum(n);
			cinstr.setRegFlag(1);
			
			return true;
			
		case INT:
			
			n = getint(getc);
			if(n == null) {
				return false;
			}
			
			if(endconsts <= org) {
				cerror("Program too long", ABORT, getc);
				return false;
			}
			
			InstructionFormat intConstInstr = new InstructionFormat();
			intConstInstr.setIntegerConstant(n);
			
			prg.getCode()[--endconsts] = intConstInstr;
			cinstr.setAddress(endconsts);
			
			return true;
			
		case FLOAT:
			
			u = getflt(getc);
			if(u == null) {
				return false;
			}
			
			if(endconsts <= org) {
				cerror("Program too long", ABORT, getc);
				return false;
			}
			
			InstructionFormat realConstInstr = new InstructionFormat();
			realConstInstr.setRealConstant(u.floatValue());
			
			prg.getCode()[--endconsts] = realConstInstr;
			cinstr.setAddress(endconsts);
			
			return true;
			
		case LABEL:
			if((c == '\n') || (c == '#') || (c == '!')) {
				cerror("Label expected\n", CONT, getc);
				return false;
			}
			
			lname = getlabel(getc);
			
			LabelRecord lfmt = findlabel(lname);
			if(lfmt != null) {
				if(lfmt.getDefined()) {
					/* Label defined */
					cinstr.setAddress(lfmt.getAddress());
				}
				else {
					/* Label undefined */
					/* Append new reference */
					lfmt.getReferences().add(cinstr);
				}
			}
			else {
				/* Label not in dictionary */
				lfmt = new LabelRecord(lname, 0, false);
				lfmt.getReferences().add(cinstr);
				labels.put(lname, lfmt);
			}
			
			return true;
			
		default:
			return false;
		}
	}
	
	void debug(InputStreamReader getc) throws IOException {
		c = getc.read();
		
		skipspaces(getc);
		
		InstructionFormat cinstr = new InstructionFormat();
		Instruction instr;
		
		switch(Character.toUpperCase(c)) {
		case 'T':
			
			/* Trace */
			if((c = getc.read()) != '=') {
				cerror("Invalid trace syntax", CONT, getc);
				return;
			}
			
			c = getc.read();
			
			instr = instructions.get("TRACE");
			break;
			
		case 'D':
			
			/* Dump */
			instr = instructions.get("DUMP");
			break;
			
		default:
			cerror("Illegal debug instruction", CONT, getc);
			return;
		}
		
		if(!operand(instr.operand1, cinstr, getc)) {
			return;
		}
		
		skiptoeol(getc);
		if(org >= endconsts) {
			cerror("Program too big", ABORT, getc);
		}
		
		cinstr.setOperation(instr.opcode);
		prg.getCode()[org++] = cinstr;
	}

	void cerror(String message, boolean code, InputStreamReader getc) throws IOException {
		errcnt++;
		
		System.err.printf("Assembly cerror (line %6d)  -- %s --\n", curline, message);
		skiptoeol(getc);
		if(code == ABORT) {
			System.err.println("Assembly aborted");
			throw new RuntimeException();
		}
	}
	
	void internalAssemble(InputStreamReader getc) throws IOException {
		org = 0; /* Program location counter */
		curline = 0;
		
		endconsts = CODESIZE;
		
		/* Initialize label reference list */
		
		while((c = getc.read()) != -1) {
			/* Read next instruction */
			
			curline++;
			
			if(c == '#') {
				/* A comment line */
				skiptoeol(getc);
				continue;
			}
			
			if(c == '!') {
				/* Debugging directive */
				debug(getc);
				continue;
			}
			
			if(c == '\n') {
				continue; /* Empty line */
			}
			
			if((c != ' ') && (c != '\t')) {
				/* Label */
				
				if(deflabel(getc)) {
					continue;
				}
			}
			
			skipspaces(getc);
			
			if(c == '#') {
				skiptoeol(getc);
				continue;
			}
			
			if(c == '!') {
				debug(getc);
				continue;
			}
			
			if(c == '\n') {
				continue;
			}
			
			/* Process instruction mnemonic */
			
			Instruction instr = mnemonic(getc);
			
			if(instr == null) {
				continue;
			}
			
			skipspaces(getc);
			
			InstructionFormat cinstr = new InstructionFormat();
			cinstr.setOperation(instr.opcode);
			
			if(!operand(instr.operand1, cinstr, getc)) {
				continue;
			}
			
			skipspaces(getc);
			if(c == ',') {
				c = getc.read();
				skipspaces(getc);
			}
			
			if(!operand(instr.operand2, cinstr, getc)) {
				continue;
			}
			
			skipspaces(getc);
			
			if(org >= endconsts) {
				cerror("Program too big", ABORT, getc);
			}
			
			prg.getCode()[org++] = cinstr;
			
			if(c == '!') {
				debug(getc);
				continue;
			}
			
			if(c == '#') {
				skiptoeol(getc);
				continue;
			}
			
			if(c != '\n') {
				cerror("Too many operands", CONT, getc);
			}
		}
		
		/* Check for undefined labels */
		
		for(LabelRecord l : labels.values()) {
			if(!l.getDefined()) {
				cerror("Undefined labels", CONT, getc);
				System.err.println(l.getName());
			}
		}
	}
}
