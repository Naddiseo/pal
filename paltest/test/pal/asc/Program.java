package pal.asc;

public class Program {
	public static final int CODESIZE = 32767;
	
	private InstructionFormat[] code = new InstructionFormat[CODESIZE];
	
	public InstructionFormat[] getCode() {
		return code;
	}
}
