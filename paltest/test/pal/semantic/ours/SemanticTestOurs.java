package pal.semantic.ours;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class SemanticTestOurs {
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void testIllegalRecord() throws Exception {
		testContext.compileFile("semantic/ours/illegalrecord.pal");
		
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testIllegalRecordAssignment() throws Exception {
		testContext.compileFile("semantic/ours/illegalrecordassignment.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testRecordScope() throws Exception {
		testContext.compileFile("semantic/ours/recordscope.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testFunctionAsProcedure() throws Exception {
		testContext.compileFile("semantic/ours/functionasprocedure.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testExternalRvAssignment() throws Exception {
		testContext.compileFile("semantic/ours/externalrvassignment.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
}
