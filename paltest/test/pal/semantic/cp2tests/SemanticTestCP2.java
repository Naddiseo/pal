package pal.semantic.cp2tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class SemanticTestCP2 {
	
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void test_g01_4() throws Exception {
		testContext.compileFile("semantic/cp2tests/g01_4.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(5, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_g01_5() throws Exception {
		testContext.compileFile("semantic/cp2tests/g01_5.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(3, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_g01_8() throws Exception {
		testContext.compileFile("semantic/cp2tests/g01_8.pal");
		assertEquals(1, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_g02_5() throws Exception {
		testContext.compileFile("semantic/cp2tests/g02_5.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(8, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_g02_7() throws Exception {
		testContext.compileFile("semantic/cp2tests/g02_7.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(5, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_g02_9() throws Exception {
		testContext.compileFile("semantic/cp2tests/g02_9.pal");
		assertEquals(3, testContext.parserErrorReport.getErrorCount());
		assertEquals(6, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_g03_2() throws Exception {
		testContext.compileFile("semantic/cp2tests/g03_2.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(5, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_g03_8() throws Exception {
		testContext.compileFile("semantic/cp2tests/g03_8.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_g04_4() throws Exception {
		testContext.compileFile("semantic/cp2tests/g04_4.pal");
		assertEquals(1, testContext.parserErrorReport.getErrorCount());
		assertEquals(6, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_g04_6() throws Exception {
		testContext.compileFile("semantic/cp2tests/g04_6.pal");
		assertEquals(1, testContext.parserErrorReport.getErrorCount());
		assertEquals(6, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_g04_9() throws Exception {
		testContext.compileFile("semantic/cp2tests/g04_9.pal");
		assertEquals(10, testContext.parserErrorReport.getErrorCount());
		assertEquals(10, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_nt01() throws Exception {
		testContext.compileFile("semantic/cp2tests/nt01.pal");
		assertEquals(3, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_nt04() throws Exception {
		testContext.compileFile("semantic/cp2tests/nt04.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_nt21() throws Exception {
		testContext.compileFile("semantic/cp2tests/nt21.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_old_g01_2() throws Exception {
		testContext.compileFile("semantic/cp2tests/old-g01-2.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(3, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_old_g01_3() throws Exception {
		testContext.compileFile("semantic/cp2tests/old-g01-3.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(3, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_old_g01_6() throws Exception {
		testContext.compileFile("semantic/cp2tests/old-g01-6.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(4, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_old_g02_3() throws Exception {
		testContext.compileFile("semantic/cp2tests/old-g02-3.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(2, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_old_g02_7() throws Exception {
		testContext.compileFile("semantic/cp2tests/old-g02-7.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(3, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_old_g03_2() throws Exception {
		testContext.compileFile("semantic/cp2tests/old-g03-2.pal");
		assertEquals(1, testContext.parserErrorReport.getErrorCount());
		assertEquals(4, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_old_g03_6() throws Exception {
		testContext.compileFile("semantic/cp2tests/old-g03-6.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(3, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_old_g04_4() throws Exception {
		testContext.compileFile("semantic/cp2tests/old-g04-4.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(3, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_old_g05_3() throws Exception {
		testContext.compileFile("semantic/cp2tests/old-g05-3.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(3, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se01() throws Exception {
		testContext.compileFile("semantic/cp2tests/se01.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(3, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se02() throws Exception {
		testContext.compileFile("semantic/cp2tests/se02.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se03() throws Exception {
		testContext.compileFile("semantic/cp2tests/se03.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se04() throws Exception {
		testContext.compileFile("semantic/cp2tests/se04.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(17, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se05() throws Exception {
		testContext.compileFile("semantic/cp2tests/se05.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(4, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se06() throws Exception {
		testContext.compileFile("semantic/cp2tests/se06.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(7, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se07() throws Exception {
		testContext.compileFile("semantic/cp2tests/se07.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(6, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se08() throws Exception {
		testContext.compileFile("semantic/cp2tests/se08.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(6, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se09() throws Exception {
		testContext.compileFile("semantic/cp2tests/se09.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(6, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se10() throws Exception {
		testContext.compileFile("semantic/cp2tests/se10.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(5, testContext.compileErrorReport.getErrorCount());
	}
	
	
	@Test
	public void test_se11() throws Exception {
		testContext.compileFile("semantic/cp2tests/se11.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(2, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se12() throws Exception {
		testContext.compileFile("semantic/cp2tests/se12.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(15, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se13() throws Exception {
		testContext.compileFile("semantic/cp2tests/se13.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(5, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se14() throws Exception {
		testContext.compileFile("semantic/cp2tests/se14.pal");
		assertEquals(1, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se15() throws Exception {
		testContext.compileFile("semantic/cp2tests/se15.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(5, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se16() throws Exception {
		testContext.compileFile("semantic/cp2tests/se16.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(2, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_se17() throws Exception {
		testContext.compileFile("semantic/cp2tests/se17.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(3, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void test_sy01() throws Exception {
		testContext.parseFile("semantic/cp2tests/sy01.pal");
		assertEquals(8, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test_sy02() throws Exception {
		testContext.parseFile("semantic/cp2tests/sy02.pal");
		assertEquals(8, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test_sy03() throws Exception {
		testContext.parseFile("semantic/cp2tests/sy03.pal");
		assertEquals(7, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test_sy04() throws Exception {
		testContext.parseFile("semantic/cp2tests/sy04.pal");
		assertEquals(7, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test_sy05() throws Exception {
		testContext.parseFile("semantic/cp2tests/sy05.pal");
		assertEquals(2, testContext.parserErrorReport.getErrorCount());
	}
	
}
