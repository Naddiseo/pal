package pal.syntactic.cp1tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class SyntacticTestCP1 {
	
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void test00() throws Exception {
		testContext.parseFile("syntactic/cp1tests/00.pal");
		assertEquals(1, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test01() throws Exception {
		testContext.parseFile("syntactic/cp1tests/01.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test02() throws Exception {
		testContext.parseFile("syntactic/cp1tests/02.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test03() throws Exception {
		testContext.parseFile("syntactic/cp1tests/03.pal");
		assertEquals(3, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test04() throws Exception {
		testContext.parseFile("syntactic/cp1tests/04.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test05() throws Exception {
		testContext.parseFile("syntactic/cp1tests/05.pal");
		assertEquals(5, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test06() throws Exception {
		testContext.parseFile("syntactic/cp1tests/06.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test07() throws Exception {
		testContext.parseFile("syntactic/cp1tests/07.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test08() throws Exception {
		testContext.parseFile("syntactic/cp1tests/08.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test09() throws Exception {
		testContext.parseFile("syntactic/cp1tests/09.pal");
		assertEquals(2, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test10() throws Exception {
		testContext.parseFile("syntactic/cp1tests/10.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test11() throws Exception {
		testContext.parseFile("syntactic/cp1tests/11.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test12() throws Exception {
		testContext.parseFile("syntactic/cp1tests/12.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test13() throws Exception {
		testContext.parseFile("syntactic/cp1tests/13.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test14() throws Exception {
		testContext.parseFile("syntactic/cp1tests/14.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test15() throws Exception {
		testContext.parseFile("syntactic/cp1tests/15.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test16() throws Exception {
		testContext.parseFile("syntactic/cp1tests/16.pal");
		assertEquals(1, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test17() throws Exception {
		testContext.parseFile("syntactic/cp1tests/17.pal");
		assertEquals(6, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test18() throws Exception {
		testContext.parseFile("syntactic/cp1tests/18.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test19() throws Exception {
		testContext.parseFile("syntactic/cp1tests/19.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test20() throws Exception {
		testContext.parseFile("syntactic/cp1tests/20.pal");
		assertEquals(3, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test21() throws Exception {
		testContext.parseFile("syntactic/cp1tests/21.pal");
		assertEquals(7, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test22() throws Exception {
		testContext.parseFile("syntactic/cp1tests/22.pal");
		assertEquals(16, testContext.parserErrorReport.getErrorCount());
	}
	
	@Test
	public void test23() throws Exception {
		testContext.parseFile("syntactic/cp1tests/23.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
	
	@Test
	public void test24() throws Exception {
		testContext.parseFile("syntactic/cp1tests/24.pal");
		assertFalse(testContext.parserErrorReport.getErrors().isEmpty());
	}
}
