package pal.syntactic.ours;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class SyntacticTestOurs {
	
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void emptyProgram() throws Exception {
		testContext.parseFile("syntactic/ours/emptyprogram.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
	}
}
