package pal.codegen.ours;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;
import static pal.PALCustomAssert.assertSimilar;

public class RecordTest {	
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void testMemberAssign() throws Exception {
		testContext.executeFile("codegen/ours/record/memberassign.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(128, testContext.outputQueue.poll());
		assertEquals(256, testContext.outputQueue.poll());
		assertEquals(512, testContext.outputQueue.poll());
	}
	
	@Test
	public void testCopyAssign() throws Exception {
		testContext.executeFile("codegen/ours/record/copyassign.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(128, testContext.outputQueue.poll());
		assertEquals(256, testContext.outputQueue.poll());
		assertEquals(512, testContext.outputQueue.poll());
		
		assertEquals(128, testContext.outputQueue.poll());
		assertEquals(256, testContext.outputQueue.poll());
		assertEquals(512, testContext.outputQueue.poll());
	}
	
	@Test
	public void testVectorLib() throws Exception {
		testContext.executeFile("codegen/ours/record/vectorlib.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(0.0f, testContext.outputQueue.poll());
		
		assertSimilar(0.0f, testContext.outputQueue.poll());
		assertSimilar(0.0f, testContext.outputQueue.poll());
		assertSimilar(1.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testNestedRecords() throws Exception {
		testContext.executeFile("codegen/ours/record/nestedrecords.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
		assertEquals('s', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals(128, testContext.outputQueue.poll());
		
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
		assertEquals('s', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals(128, testContext.outputQueue.poll());
		
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
		assertEquals('s', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals(512, testContext.outputQueue.poll());
		
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
		assertEquals('s', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals(1024, testContext.outputQueue.poll());
	}
}
