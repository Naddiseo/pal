package pal.codegen.ours;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Queue;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class IOTest {
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	
	public void printOutput(Queue<Object> other) {
		for (Object o : other) {
			System.err.println(o.toString());
		}
	}
	public void assertString(String s, Queue<Object> other) {
		for (int i = 0; i < s.length(); i++) {
			assertEquals(s.charAt(i), other.poll());
		}
	}
	
	@Test
	public void testWritelnEmpty() throws Exception {
		testContext.executeFile("codegen/ours/io/writelnempty.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('\n', testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testWriteThreeArgs() throws Exception {
		testContext.executeFile("codegen/ours/io/writethreeargs.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(32, testContext.outputQueue.poll());
		assertEquals(453, testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testWritelnThreeArgs() throws Exception {
		testContext.executeFile("codegen/ours/io/writelnthreeargs.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(32, testContext.outputQueue.poll());
		assertEquals(453, testContext.outputQueue.poll());
		assertEquals('\n', testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testWriteLiteralString() throws Exception {
		testContext.executeFile("codegen/ours/io/writeliteralstring.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
		assertEquals('s', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testWritelnLiteralString() throws Exception {
		testContext.executeFile("codegen/ours/io/writelnliteralstring.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
		assertEquals('s', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('\n', testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testWriteVarString() throws Exception {
		testContext.executeFile("codegen/ours/io/writevarstring.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
		assertEquals('s', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testWritelnVarString() throws Exception {
		testContext.executeFile("codegen/ours/io/writelnvarstring.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
		assertEquals('s', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('\n', testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testWriteVarInteger() throws Exception {
		testContext.executeFile("codegen/ours/io/writevarinteger.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(128, testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testWritelnVarInteger() throws Exception {
		testContext.executeFile("codegen/ours/io/writelnvarinteger.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(128, testContext.outputQueue.poll());
		assertEquals('\n', testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testWriteVarReal() throws Exception {
		testContext.executeFile("codegen/ours/io/writevarreal.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(128.0f, testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testWritelnVarReal() throws Exception {
		testContext.executeFile("codegen/ours/io/writelnvarreal.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(128.0f, testContext.outputQueue.poll());
		assertEquals('\n', testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testReadIntoVars() throws Exception {
		String instr = "hellohello";
		testContext.inputQueue.offer(99);
		testContext.inputQueue.offer('c');
		testContext.inputQueue.offer(instr);
		testContext.inputQueue.offer(1.5f);
		
		testContext.executeFile("codegen/ours/io/readintovars.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(99, testContext.outputQueue.poll());
		assertEquals('c', testContext.outputQueue.poll());
		assertString(instr, testContext.outputQueue);
		assertEquals(1.5f, testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testReadIntOverflow() throws Exception {
		testContext.inputQueue.offer(4294967295L);
		
		testContext.executeFile("codegen/ours/io/readintoverflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(-1, testContext.outputQueue.poll());
	}
	
	@Test
	public void testReadIntBad1() throws Exception {
		
		testContext.inputQueue.offer("asdf");
		
		testContext.executeFile("codegen/ours/io/readintoverflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertString("Run-time error", testContext.outputQueue);
	}
	
	@Test
	public void testReadIntoStructures() throws Exception {
		Integer i = 0;
		Character c = 'a';
		Float r = 1.5f;
		for (int j = 0; j < 10; j++) {
			for (int k = 0; k < 10; k++) {
				testContext.inputQueue.offer(i);
				testContext.inputQueue.offer(c);
				testContext.inputQueue.offer(r);
				
				i++;
				c++;
				r += 1.5f;
			}
			i = 0;
			c = 'a';
			r = 1.5f;
		}
		
		testContext.executeFile("codegen/ours/io/readintostructures.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		i = 0;
		c = 'a';
		r = 1.5f;
		for (int j = 0; j < 10; j++) {
			for (int k = 0; k < 10; k++) {
				assertEquals(i, testContext.outputQueue.poll());
				assertEquals(c, testContext.outputQueue.poll());
				assertEquals(r, testContext.outputQueue.poll());
				
				i++;
				c++;
				r += 1.5f;
			}
			i = 0;
			c = 'a';
			r = 1.5f;
		}
	}
	
	@Test
	public void testReadIntoRecord1() throws Exception {
		testContext.inputQueue.offer("Bobby Bobb");
		testContext.inputQueue.offer(22);
		testContext.inputQueue.offer(1.5f);
		testContext.inputQueue.offer(75f);
		
		testContext.executeFile("codegen/ours/io/readintorecord.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertString("What is your name?\n", testContext.outputQueue);
		assertString("What is your age?\n", testContext.outputQueue);
		assertString("What is your height? (in meters)\n", testContext.outputQueue);
		assertString("What is your weight? (in kg)\n", testContext.outputQueue);
		
		assertString("Hello, Bobby Bobb\n", testContext.outputQueue);
		
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testReadIntoRecord2() throws Exception {
		testContext.inputQueue.offer("Bobby Bobb");
		testContext.inputQueue.offer(15);
		testContext.inputQueue.offer(1.0f);
		testContext.inputQueue.offer(45f);
		
		testContext.executeFile("codegen/ours/io/readintorecord.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertString("What is your name?\n", testContext.outputQueue);
		assertString("What is your age?\n", testContext.outputQueue);
		assertString("What is your height? (in meters)\n", testContext.outputQueue);
		assertString("What is your weight? (in kg)\n", testContext.outputQueue);
		
		assertString("Hello, Bobby Bobb\n", testContext.outputQueue);
		assertString("You're young!\n", testContext.outputQueue);
		assertString("You're short!\n", testContext.outputQueue);
		assertString("You're light!\n", testContext.outputQueue);
		
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testReadIntoRecord3() throws Exception {
		testContext.inputQueue.offer("Bobby Bobb");
		testContext.inputQueue.offer(35);
		testContext.inputQueue.offer(2.0);
		testContext.inputQueue.offer(100.5);
		
		testContext.executeFile("codegen/ours/io/readintorecord.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertString("What is your name?\n", testContext.outputQueue);
		assertString("What is your age?\n", testContext.outputQueue);
		assertString("What is your height? (in meters)\n", testContext.outputQueue);
		assertString("What is your weight? (in kg)\n", testContext.outputQueue);
		
		assertString("Hello, Bobby Bobb\n", testContext.outputQueue);
		assertString("You're old!\n", testContext.outputQueue);
		assertString("You're tall!\n", testContext.outputQueue);
		assertString("You're fat!\n", testContext.outputQueue);
		
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	
	@Test
	public void testReadlnIntoRecord1() throws Exception {
		testContext.inputQueue.offer("Bobby\n");
		testContext.inputQueue.offer(22);
		testContext.inputQueue.offer(1.5f);
		testContext.inputQueue.offer(75f);
		
		testContext.executeFile("codegen/ours/io/readlnintorecord.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		printOutput(testContext.outputQueue);
		
		assertString("What is your name?\n", testContext.outputQueue);
		assertString("What is your age?\n", testContext.outputQueue);
		assertString("What is your height? (in meters)\n", testContext.outputQueue);
		assertString("What is your weight? (in kg)\n", testContext.outputQueue);
		
		assertString("Hello, Bobby\n", testContext.outputQueue);
		
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testReadlnIntoRecord2() throws Exception {
		testContext.inputQueue.offer("Bobby\n");
		testContext.inputQueue.offer(15);
		testContext.inputQueue.offer(1.0f);
		testContext.inputQueue.offer(45f);
		
		testContext.executeFile("codegen/ours/io/readlnintorecord.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertString("What is your name?\n", testContext.outputQueue);
		assertString("What is your age?\n", testContext.outputQueue);
		assertString("What is your height? (in meters)\n", testContext.outputQueue);
		assertString("What is your weight? (in kg)\n", testContext.outputQueue);
		
		assertString("Hello, Bobby\n", testContext.outputQueue);
		assertString("You're young!\n", testContext.outputQueue);
		assertString("You're short!\n", testContext.outputQueue);
		assertString("You're light!\n", testContext.outputQueue);
		
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testReadlnIntoRecord3() throws Exception {
		testContext.inputQueue.offer("Bobby\n");
		testContext.inputQueue.offer(35);
		testContext.inputQueue.offer(2.0);
		testContext.inputQueue.offer(100.5);

		
		testContext.executeFile("codegen/ours/io/readlnintorecord.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		

		assertString("What is your name?\n", testContext.outputQueue);
		assertString("What is your age?\n", testContext.outputQueue);
		assertString("What is your height? (in meters)\n", testContext.outputQueue);
		assertString("What is your weight? (in kg)\n", testContext.outputQueue);
		
		assertString("Hello, Bobby\n", testContext.outputQueue);
		assertString("You're old!\n", testContext.outputQueue);
		assertString("You're tall!\n", testContext.outputQueue);
		assertString("You're fat!\n", testContext.outputQueue);
		
		assertTrue(testContext.outputQueue.isEmpty());
	}
}
