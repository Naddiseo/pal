package pal.codegen.ours;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class EnumerationTest {
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void testOrd() throws Exception {
		testContext.executeFile("codegen/ours/enumeration/ord.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(0, testContext.outputQueue.poll());
		assertEquals(2, testContext.outputQueue.poll());
		assertEquals(6, testContext.outputQueue.poll());
	}
	
	@Test
	public void testSucc() throws Exception {
		testContext.executeFile("codegen/ours/enumeration/succ.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(3, testContext.outputQueue.poll());
	}
	
	@Test
	public void testPred() throws Exception {
		testContext.executeFile("codegen/ours/enumeration/pred.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(5, testContext.outputQueue.poll());
	}
	
	@Test
	public void testPredUnderflow() throws Exception {
		testContext.executeFile("codegen/ours/enumeration/predunderflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testSuccOverflow() throws Exception {
		testContext.executeFile("codegen/ours/enumeration/succoverflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testOrdVar() throws Exception {
		testContext.executeFile("codegen/ours/enumeration/ordvar.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(0, testContext.outputQueue.poll());
		assertEquals(2, testContext.outputQueue.poll());
		assertEquals(6, testContext.outputQueue.poll());
	}
	
	@Test
	public void testSuccVar() throws Exception {
		testContext.executeFile("codegen/ours/enumeration/succvar.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(3, testContext.outputQueue.poll());
	}
	
	@Test
	public void testPredVar() throws Exception {
		testContext.executeFile("codegen/ours/enumeration/predvar.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(5, testContext.outputQueue.poll());
	}
	
	@Test
	public void testPredVarUnderflow() throws Exception {
		testContext.executeFile("codegen/ours/enumeration/predvarunderflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('R', testContext.outputQueue.poll());
		assertEquals('u', testContext.outputQueue.poll());
		assertEquals('n', testContext.outputQueue.poll());
		assertEquals('-', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('i', testContext.outputQueue.poll());
		assertEquals('m', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
	}
	
	@Test
	public void testSuccVarOverflow() throws Exception {
		testContext.executeFile("codegen/ours/enumeration/succvaroverflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('R', testContext.outputQueue.poll());
		assertEquals('u', testContext.outputQueue.poll());
		assertEquals('n', testContext.outputQueue.poll());
		assertEquals('-', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('i', testContext.outputQueue.poll());
		assertEquals('m', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
	}
}