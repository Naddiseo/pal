package pal.codegen.ours;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class IntegerTest {
	
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void testLiteral() throws Exception {
		testContext.executeFile("codegen/ours/integer/literal.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(128), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVar() throws Exception {
		testContext.executeFile("codegen/ours/integer/var.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(128), testContext.outputQueue.poll());
	}
	
	@Test
	public void testInput() throws Exception {
		testContext.inputQueue.offer(new Integer(128));
		
		testContext.executeFile("codegen/ours/integer/input.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(128), testContext.outputQueue.poll());
	}
	
	@Test
	public void testConst() throws Exception {
		testContext.executeFile("codegen/ours/integer/const.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(128), testContext.outputQueue.poll());
	}
	
	@Test
	public void testLiteralSum() throws Exception {
		testContext.executeFile("codegen/ours/integer/literalsum.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(3), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarSum() throws Exception {
		testContext.executeFile("codegen/ours/integer/varsum.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(3), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarLiteralSum() throws Exception {
		testContext.executeFile("codegen/ours/integer/varliteralsum.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(3), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarNegation() throws Exception {
		testContext.executeFile("codegen/ours/integer/varnegation.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(-128), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarDifference() throws Exception {
		testContext.executeFile("codegen/ours/integer/vardifference.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(1), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarProduct() throws Exception {
		testContext.executeFile("codegen/ours/integer/varproduct.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(2), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarQuotient() throws Exception {
		testContext.executeFile("codegen/ours/integer/varquotient.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(3), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarRemainder() throws Exception {
		testContext.executeFile("codegen/ours/integer/varremainder.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(1), testContext.outputQueue.poll());
	}
	
	@Test
	public void testReturnValue() throws Exception {
		testContext.executeFile("codegen/ours/integer/returnvalue.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(128), testContext.outputQueue.poll());
	}
	
	@Test
	public void testOrd() throws Exception {
		testContext.executeFile("codegen/ours/integer/ord.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(0, testContext.outputQueue.poll());
		assertEquals(2, testContext.outputQueue.poll());
		assertEquals(6, testContext.outputQueue.poll());
	}
	
	@Test
	public void testSucc() throws Exception {
		testContext.executeFile("codegen/ours/integer/succ.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(3, testContext.outputQueue.poll());
	}
	
	@Test
	public void testPred() throws Exception {
		testContext.executeFile("codegen/ours/integer/pred.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(5, testContext.outputQueue.poll());
	}
	
	@Test
	public void testPredUnderflow() throws Exception {
		testContext.executeFile("codegen/ours/integer/predunderflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testSuccOverflow() throws Exception {
		testContext.executeFile("codegen/ours/integer/succoverflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testOrdVar() throws Exception {
		testContext.executeFile("codegen/ours/integer/ordvar.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(0, testContext.outputQueue.poll());
		assertEquals(2, testContext.outputQueue.poll());
		assertEquals(6, testContext.outputQueue.poll());
	}
	
	@Test
	public void testSuccVar() throws Exception {
		testContext.executeFile("codegen/ours/integer/succvar.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(3, testContext.outputQueue.poll());
	}
	
	@Test
	public void testPredVar() throws Exception {
		testContext.executeFile("codegen/ours/integer/predvar.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(5, testContext.outputQueue.poll());
	}
	
	@Test
	public void testPredVarUnderflow() throws Exception {
		testContext.executeFile("codegen/ours/integer/predvarunderflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('R', testContext.outputQueue.poll());
		assertEquals('u', testContext.outputQueue.poll());
		assertEquals('n', testContext.outputQueue.poll());
		assertEquals('-', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('i', testContext.outputQueue.poll());
		assertEquals('m', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
	}
	
	@Test
	public void testSuccVarOverflow() throws Exception {
		testContext.executeFile("codegen/ours/integer/succvaroverflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('R', testContext.outputQueue.poll());
		assertEquals('u', testContext.outputQueue.poll());
		assertEquals('n', testContext.outputQueue.poll());
		assertEquals('-', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('i', testContext.outputQueue.poll());
		assertEquals('m', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
	}
	
	@Test
	public void testSqr() throws Exception {
		testContext.executeFile("codegen/ours/integer/sqr.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(0, testContext.outputQueue.poll());
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(25, testContext.outputQueue.poll());
		assertEquals(100, testContext.outputQueue.poll());
		assertEquals(10000, testContext.outputQueue.poll());
	}
	
	@Test
	public void testAbs() throws Exception {
		testContext.executeFile("codegen/ours/integer/abs.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(1, testContext.outputQueue.poll());
	}
	
	@Test
	public void testComparisons() throws Exception {
		testContext.executeFile("codegen/ours/integer/comparisons.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(2, testContext.outputQueue.poll());
		assertEquals(3, testContext.outputQueue.poll());
		assertEquals(7, testContext.outputQueue.poll());
		assertEquals(9, testContext.outputQueue.poll());
		assertEquals(11, testContext.outputQueue.poll());
		assertEquals(12, testContext.outputQueue.poll());
		assertEquals(14, testContext.outputQueue.poll());
		assertEquals(16, testContext.outputQueue.poll());
		assertEquals(18, testContext.outputQueue.poll());
	}
}
