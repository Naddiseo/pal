package pal.codegen.ours;

import static org.junit.Assert.assertEquals;
import static pal.PALCustomAssert.assertRangeSimilar;
import static pal.PALCustomAssert.assertSimilar;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class RealTest {
	PALTestContext testContext;
		
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}

	@Test
	public void testLiteral() throws Exception {
		testContext.executeFile("codegen/ours/real/literal.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(128.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testVar() throws Exception {
		testContext.executeFile("codegen/ours/real/var.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(128.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testInput() throws Exception {
		testContext.inputQueue.offer(new Float(128));
		
		testContext.executeFile("codegen/ours/real/input.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(128.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testConst() throws Exception {
		testContext.executeFile("codegen/ours/real/const.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(128.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testLiteralSum() throws Exception {
		testContext.executeFile("codegen/ours/real/literalsum.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(3.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarSum() throws Exception {
		testContext.executeFile("codegen/ours/real/varsum.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(3.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarLiteralSum() throws Exception {
		testContext.executeFile("codegen/ours/real/varliteralsum.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(3.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarNegation() throws Exception {
		testContext.executeFile("codegen/ours/real/varnegation.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(-128.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarDifference() throws Exception {
		testContext.executeFile("codegen/ours/real/vardifference.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(1.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarProduct() throws Exception {
		testContext.executeFile("codegen/ours/real/varproduct.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(2.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarQuotient() throws Exception {
		testContext.executeFile("codegen/ours/real/varquotient.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(3.0f, testContext.outputQueue.poll());
	}

	@Test
	public void testVarLn() throws Exception {
		testContext.executeFile("codegen/ours/real/varln.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(0.0f, testContext.outputQueue.poll());
		assertSimilar(1.0f, testContext.outputQueue.poll());
		assertSimilar(11.78350207f, testContext.outputQueue.poll());
		assertSimilar(-85.8305267f, testContext.outputQueue.poll());
		assertSimilar(3.648663f, testContext.outputQueue.poll());
		assertSimilar(29.188934f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarAbs() throws Exception {
		testContext.executeFile("codegen/ours/real/varabs.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(0, testContext.outputQueue.poll());
		assertEquals(1, testContext.outputQueue.poll());
		assertSimilar(1.0f, testContext.outputQueue.poll());
		assertSimilar(0.0f, testContext.outputQueue.poll());
		assertSimilar(1.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarSin() throws Exception {
		testContext.executeFile("codegen/ours/real/varsin.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertRangeSimilar((float)Math.sin(0.0), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(3.141592654), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(-3.141592654), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(6.283185308), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(-6.283185308), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(9.424777962), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(-9.424777962), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(0.785398163), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(1.57079633), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(4.71238898), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(Math.sqrt(2.0)), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarExp() throws Exception {
		testContext.executeFile("codegen/ours/real/varexp.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar((float)1.0, testContext.outputQueue.poll());
		assertSimilar((float)Math.E, testContext.outputQueue.poll());
		assertSimilar((float)Math.exp(2.0), testContext.outputQueue.poll());
		assertSimilar((float)Math.exp(0.5), testContext.outputQueue.poll());
		assertSimilar((float)Math.exp(-1.0), testContext.outputQueue.poll());
		assertSimilar((float)Math.exp(-2.0), testContext.outputQueue.poll());
		assertSimilar((float)Math.exp(Math.PI), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarSqrt() throws Exception {
		testContext.executeFile("codegen/ours/real/varsqrt.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(5.0f, testContext.outputQueue.poll());
		assertSimilar((float)Math.sqrt(2.0), testContext.outputQueue.poll());
		assertSimilar(10.0f, testContext.outputQueue.poll());
		assertSimilar(100.0f, testContext.outputQueue.poll());
		assertSimilar(4.0e15f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testReturnValue() throws Exception {
		testContext.executeFile("codegen/ours/real/returnvalue.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(128.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testSqrt() throws Exception {
		testContext.executeFile("codegen/ours/real/sqrt.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(5.0f, testContext.outputQueue.poll());
		assertSimilar((float)Math.sqrt(2.0), testContext.outputQueue.poll());
		assertSimilar(10.0f, testContext.outputQueue.poll());
		assertSimilar(100.0f, testContext.outputQueue.poll());
		assertSimilar(4.0e15f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testNegSqrt() throws Exception {
		testContext.executeFile("codegen/ours/real/negsqrt.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		// Negative square root should be illegal
		assertEquals('R', testContext.outputQueue.poll());
		assertEquals('u', testContext.outputQueue.poll());
		assertEquals('n', testContext.outputQueue.poll());
		assertEquals('-', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('i', testContext.outputQueue.poll());
		assertEquals('m', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
	}
	
	@Test
	public void testSqr() throws Exception {
		testContext.executeFile("codegen/ours/real/sqr.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(25.0f, testContext.outputQueue.poll());
		assertSimilar(2.0f, testContext.outputQueue.poll());
		assertSimilar(100.0f, testContext.outputQueue.poll());
		assertSimilar(10000.0f, testContext.outputQueue.poll());
		assertSimilar(1.6e31f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testAbs() throws Exception {
		testContext.executeFile("codegen/ours/real/abs.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(1.0f, testContext.outputQueue.poll());
		assertSimilar(1.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testSin() throws Exception {
		testContext.executeFile("codegen/ours/real/sin.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertRangeSimilar((float)Math.sin(0.0), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(3.141592654), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(-3.141592654), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(6.283185308), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(-6.283185308), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(9.424777962), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(-9.424777962), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(0.785398163), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(1.57079633), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(4.71238898), testContext.outputQueue.poll());
		assertRangeSimilar((float)Math.sin(Math.sqrt(2.0)), testContext.outputQueue.poll());
	}
	
	@Test
	public void testExp() throws Exception {
		testContext.executeFile("codegen/ours/real/exp.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(1.0f, testContext.outputQueue.poll());
		assertSimilar((float)Math.E, testContext.outputQueue.poll());
		assertSimilar((float)Math.exp(2.0), testContext.outputQueue.poll());
		assertSimilar((float)Math.exp(0.5), testContext.outputQueue.poll());
		assertSimilar((float)Math.exp(-1.0), testContext.outputQueue.poll());
		assertSimilar((float)Math.exp(-2.0), testContext.outputQueue.poll());
		assertSimilar((float)Math.exp(Math.PI), testContext.outputQueue.poll());
	}
	
	@Test
	public void testLn() throws Exception {
		testContext.executeFile("codegen/ours/real/ln.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertSimilar(0.0f, testContext.outputQueue.poll());
		assertSimilar(1.0f, testContext.outputQueue.poll());
		assertSimilar(17.0f, testContext.outputQueue.poll());
	}
	
	@Test
	public void testNegLn() throws Exception {
		testContext.executeFile("codegen/ours/real/negln.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		// We do not have imaginary numbers
		assertEquals('R', testContext.outputQueue.poll());
		assertEquals('u', testContext.outputQueue.poll());
		assertEquals('n', testContext.outputQueue.poll());
		assertEquals('-', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('i', testContext.outputQueue.poll());
		assertEquals('m', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
	}
	
	@Test
	public void testZeroLn() throws Exception {
		testContext.executeFile("codegen/ours/real/zeroln.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());

		// ln(<= 0) is illegal
		assertEquals('R', testContext.outputQueue.poll());
		assertEquals('u', testContext.outputQueue.poll());
		assertEquals('n', testContext.outputQueue.poll());
		assertEquals('-', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('i', testContext.outputQueue.poll());
		assertEquals('m', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
	}
	
	@Test
	public void testComparisons() throws Exception {
		testContext.executeFile("codegen/ours/real/comparisons.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(2, testContext.outputQueue.poll());
		assertEquals(3, testContext.outputQueue.poll());
		assertEquals(12, testContext.outputQueue.poll());
		assertEquals(14, testContext.outputQueue.poll());
		assertEquals(16, testContext.outputQueue.poll());
		assertEquals(18, testContext.outputQueue.poll());
	}
}
