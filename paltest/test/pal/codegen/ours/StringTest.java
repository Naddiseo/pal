package pal.codegen.ours;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class StringTest {
	
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void testLiteral() throws Exception {
		testContext.executeFile("codegen/ours/string/literal.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Character('T'), testContext.outputQueue.poll());
		assertEquals(new Character('e'), testContext.outputQueue.poll());
		assertEquals(new Character('s'), testContext.outputQueue.poll());
		assertEquals(new Character('t'), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVar() throws Exception {
		testContext.executeFile("codegen/ours/string/var.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Character('T'), testContext.outputQueue.poll());
		assertEquals(new Character('e'), testContext.outputQueue.poll());
		assertEquals(new Character('s'), testContext.outputQueue.poll());
		assertEquals(new Character('t'), testContext.outputQueue.poll());
	}
	
}
