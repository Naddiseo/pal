package pal.codegen.ours;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class ArrayTest {
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void testMemberAssign() throws Exception {
		testContext.executeFile("codegen/ours/array/memberassign.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(128, testContext.outputQueue.poll());
		assertEquals(256, testContext.outputQueue.poll());
	}
	
	@Test
	public void testMemberAssignVarIndex() throws Exception {
		testContext.executeFile("codegen/ours/array/memberassignvarindex.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(128, testContext.outputQueue.poll());
		assertEquals(256, testContext.outputQueue.poll());
	}
	
	@Test
	public void testCopyAssign() throws Exception {
		testContext.executeFile("codegen/ours/array/copyassign.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(128, testContext.outputQueue.poll());
		assertEquals(256, testContext.outputQueue.poll());
		
		assertEquals(128, testContext.outputQueue.poll());
		assertEquals(256, testContext.outputQueue.poll());
	}
	
	@Test
	public void testDaysOfWeek() throws Exception {
		testContext.executeFile("codegen/ours/array/daysofweek.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testBoundsUnderflow() throws Exception {
		testContext.executeFile("codegen/ours/array/boundsunderflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('R', testContext.outputQueue.poll());
		assertEquals('u', testContext.outputQueue.poll());
		assertEquals('n', testContext.outputQueue.poll());
		assertEquals('-', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('i', testContext.outputQueue.poll());
		assertEquals('m', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
	}
	
	@Test
	public void testBoundsOverflow() throws Exception {
		testContext.executeFile("codegen/ours/array/boundsoverflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals('R', testContext.outputQueue.poll());
		assertEquals('u', testContext.outputQueue.poll());
		assertEquals('n', testContext.outputQueue.poll());
		assertEquals('-', testContext.outputQueue.poll());
		assertEquals('t', testContext.outputQueue.poll());
		assertEquals('i', testContext.outputQueue.poll());
		assertEquals('m', testContext.outputQueue.poll());
		assertEquals('e', testContext.outputQueue.poll());
	}
	
	@Test
	public void testConstBoundsUnderflow() throws Exception {
		testContext.executeFile("codegen/ours/array/constboundsunderflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testConstBoundsOverflow() throws Exception {
		testContext.executeFile("codegen/ours/array/constboundsoverflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testLiteralBoundsUnderflow() throws Exception {
		testContext.executeFile("codegen/ours/array/literalboundsunderflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testLiteralBoundsOverflow() throws Exception {
		testContext.executeFile("codegen/ours/array/literalboundsoverflow.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(1, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testQuicksort() throws Exception {
		testContext.inputQueue.offer(45);
		testContext.inputQueue.offer(897);
		testContext.inputQueue.offer(9833);
		testContext.inputQueue.offer(897);
		testContext.inputQueue.offer(83745);
		testContext.inputQueue.offer(8);
		testContext.inputQueue.offer(92);
		testContext.inputQueue.offer(17523455);
		testContext.inputQueue.offer(123);
		testContext.inputQueue.offer(9778177);
		
		testContext.executeFile("codegen/ours/array/quicksort.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(8, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(45, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(92, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(123, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(897, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(897, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(9833, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(83745, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(9778177, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(17523455, testContext.outputQueue.poll());
		assertEquals('\n', testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testHeapsort() throws Exception {
		testContext.inputQueue.offer(45);
		testContext.inputQueue.offer(897);
		testContext.inputQueue.offer(9833);
		testContext.inputQueue.offer(897);
		testContext.inputQueue.offer(83745);
		testContext.inputQueue.offer(8);
		testContext.inputQueue.offer(92);
		testContext.inputQueue.offer(17523455);
		testContext.inputQueue.offer(123);
		testContext.inputQueue.offer(9778177);
		
		testContext.executeFile("codegen/ours/array/heapsort.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(8, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(45, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(92, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(123, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(897, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(897, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(9833, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(83745, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(9778177, testContext.outputQueue.poll());
		assertEquals(' ', testContext.outputQueue.poll());
		assertEquals(17523455, testContext.outputQueue.poll());
		assertEquals('\n', testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testExpressionIndexing() throws Exception {
		testContext.executeFile("codegen/ours/array/expressionindexing.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(99, testContext.outputQueue.poll());
		assertEquals(-1, testContext.outputQueue.poll());
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(-1, testContext.outputQueue.poll());
		assertEquals(99, testContext.outputQueue.poll());
		assertEquals(99, testContext.outputQueue.poll());
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(-1, testContext.outputQueue.poll());
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(-1, testContext.outputQueue.poll());
		assertEquals(-1, testContext.outputQueue.poll());
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(1, testContext.outputQueue.poll());
	}
	
}
