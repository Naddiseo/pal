package pal.codegen.ours;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class BooleanTest {
	
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void testOr() throws Exception {
		testContext.executeFile("codegen/ours/boole/or.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(2, testContext.outputQueue.poll());
		assertEquals(3, testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}

	@Test
	public void testAnd() throws Exception {
		testContext.executeFile("codegen/ours/boole/and.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testNot() throws Exception {
		testContext.executeFile("codegen/ours/boole/not.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(1, testContext.outputQueue.poll());
		assertEquals(4, testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
}
