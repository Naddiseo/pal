package pal.codegen.ours;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class FlowTest {
	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void testEmptyProgram() throws Exception {
		testContext.executeFile("codegen/ours/flow/emptyprogram.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testLongProcName() throws Exception {
		testContext.executeFile("codegen/ours/flow/longprocname.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testBasicLoop() throws Exception {
		testContext.executeFile("codegen/ours/flow/basicloop.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		for(int i = 0; i < 10; ++i) {
			assertEquals(new Integer(i), testContext.outputQueue.poll());
		}
	}
	
	@Test
	public void testSelectionStatementStressTest() throws Exception {
		testContext.executeFile("codegen/ours/flow/selstmtstress.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(0, testContext.outputQueue.poll());
		assertEquals(2, testContext.outputQueue.poll());
		assertEquals(5, testContext.outputQueue.poll());
		assertEquals(6, testContext.outputQueue.poll());
		assertEquals(10, testContext.outputQueue.poll());
		assertEquals(13, testContext.outputQueue.poll());
		assertEquals(18, testContext.outputQueue.poll());
		assertEquals(22, testContext.outputQueue.poll());
		assertEquals(26, testContext.outputQueue.poll());
		assertEquals(29, testContext.outputQueue.poll());
		assertEquals(30, testContext.outputQueue.poll());
		assertEquals(33, testContext.outputQueue.poll());
		assertEquals(37, testContext.outputQueue.poll());
		assertEquals(41, testContext.outputQueue.poll());
		assertEquals(42, testContext.outputQueue.poll());
		assertEquals(50, testContext.outputQueue.poll());
		assertEquals(54, testContext.outputQueue.poll());
		assertEquals(61, testContext.outputQueue.poll());
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testIterationStatementStressTest() throws Exception {
		testContext.executeFile("codegen/ours/flow/itrstmtstress.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		for(int i = 0; i < 10; ++i) {
			assertEquals(i, testContext.outputQueue.poll());
		}
		
		for(int i = 0; i < 10; ++i) {
			assertEquals(i, testContext.outputQueue.poll());
		}
		
		for(int i = 0; i < 10000; ++i) {
			assertEquals(i, testContext.outputQueue.poll());
		}
		
		assertTrue(testContext.outputQueue.isEmpty());
	}
	
	@Test
	public void testGlobalVar() throws Exception {
		testContext.inputQueue.offer(new Integer(8));
		
		testContext.executeFile("codegen/ours/flow/globalvar.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(9), testContext.outputQueue.poll());
	}
	
	@Test
	public void testVarParam() throws Exception {
		testContext.inputQueue.offer(new Integer(8));
		
		testContext.executeFile("codegen/ours/flow/varparam.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(9), testContext.outputQueue.poll());
	}
	
	@Test
	public void testRecursion() throws Exception {
		testContext.executeFile("codegen/ours/flow/recursion.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		for(int i = 10; i > -1; --i) {
			assertEquals(new Integer(i), testContext.outputQueue.poll());
		}
	}
	
	@Test
	public void testNesting() throws Exception {
		testContext.inputQueue.add(new Integer(5));
		testContext.executeFile("codegen/ours/flow/nesting.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(120), testContext.outputQueue.poll());
	}
	
	@Test
	public void testNestedVars() throws Exception {
		testContext.executeFile("codegen/ours/flow/nestedvars.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		for(int i = 0; i < 10000; ++i) {
			assertEquals(i, testContext.outputQueue.poll());
			assertEquals('\n', testContext.outputQueue.poll());
		}
	}
	
	@Test
	public void testFactorial() throws Exception {
		testContext.inputQueue.add(new Integer(5));
		testContext.executeFile("codegen/ours/flow/factorial.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(120), testContext.outputQueue.poll());
	}
	
	@Test
	public void testAssignment2() throws Exception {
		testContext.inputQueue.add(new Integer(0xD318)); // 1101001100011000
		testContext.executeFile("codegen/ours/flow/asn2.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(new Integer(0xD318), testContext.outputQueue.poll());
		assertEquals(new Integer(7), testContext.outputQueue.poll());
		assertEquals(new Integer(9), testContext.outputQueue.poll());
	}
	
	@Test
	public void testReturnValue() throws Exception {
		testContext.executeFile("codegen/ours/flow/returnvalue.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(7, testContext.outputQueue.poll());
	}
	
	@Test
	public void testArrayByVar() throws Exception {
		testContext.executeFile("codegen/ours/flow/arraybyvar.pal");
		
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		String chars1 = "0123456789\n";
		String chars2 = "9876543210\n";
		
		for (int i = 0; i < chars1.length(); i++) {
			assertEquals(chars1.charAt(i), testContext.outputQueue.poll());
		}
		
		for (int i = 0; i < chars2.length(); i++) {
			assertEquals(chars2.charAt(i), testContext.outputQueue.poll());
		}
	}
}
