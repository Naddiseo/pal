package pal.codegen.cp3tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import pal.PALTestContext;

public class CodegenTestCP3 {

	PALTestContext testContext;
	
	@Before
	public void setUp() {
		testContext = new PALTestContext();
	}
	
	@Test
	public void testFfr() throws Exception {
		testContext.compileFile("codegen/cp3tests/ffr.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testTheOK() throws Exception {
		testContext.compileFile("codegen/cp3tests/theOK.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testNested() throws Exception {
		testContext.inputQueue.offer(96);
		testContext.inputQueue.offer('\n');
		
		
		
		testContext.compileFile("codegen/cp3tests/nested.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
	}
	
	@Test
	public void testBubble() throws Exception {
		testContext.inputQueue.offer(45);
		testContext.inputQueue.offer(897);
		testContext.inputQueue.offer(9833);
		testContext.inputQueue.offer(897);
		testContext.inputQueue.offer(83745);
		testContext.inputQueue.offer(8);
		testContext.inputQueue.offer(92);
		testContext.inputQueue.offer(17523455);
		testContext.inputQueue.offer(123);
		testContext.inputQueue.offer(9778177);
		
		testContext.executeFile("codegen/cp3tests/bubble.pal");
		assertEquals(0, testContext.parserErrorReport.getErrorCount());
		assertEquals(0, testContext.compileErrorReport.getErrorCount());
		
		assertEquals(8, testContext.outputQueue.poll());
		assertEquals(45, testContext.outputQueue.poll());
		assertEquals(92, testContext.outputQueue.poll());
		assertEquals(123, testContext.outputQueue.poll());
		assertEquals(897, testContext.outputQueue.poll());
		assertEquals(897, testContext.outputQueue.poll());
		assertEquals(9833, testContext.outputQueue.poll());
		assertEquals(83745, testContext.outputQueue.poll());
		assertEquals(9778177, testContext.outputQueue.poll());
		assertEquals(17523455, testContext.outputQueue.poll());
	}
}
