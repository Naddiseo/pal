// This program exercises all the basic arithmetic operations, comparisons, and
// string subscripting.
// It shoul output "Program executed successfully" if there were no errors
// Otherwise it will print any failed assertions

program p(input, output);

const
	z = 'qwert';
	i = 'treww';
	r = 1 + 2;
	b = 1 mod 2;
	c = true and false;
	d = true or false;
	e = 3 < 2;
	f = not false;
	eps = 0.001;

type string5 = array[1..5] of char;
var	q : integer;
	y : real;
	a : real;
	n : integer;
	k : integer;
	o : boolean;
	l : (big,small);
	g : array[1..10] of char;
	x : real;
	asserts : integer;
	errs : integer;
	xs : string5;
	ys : string5;
	cs : char;
	ds : char;

procedure assertb( b : boolean; e : boolean);
	begin
		asserts := asserts + 1;
		if b <> e then
			begin
			errs := errs + 1;
			writeln('Boolean assertion #', asserts, ' failed.');
			if b then
				writeln('Expected false but got true')
			else
				writeln('Expected true but got false');	
			end;
	end;
procedure assertc( c : char; e : char);
	begin
		asserts := asserts + 1;
		if c <> e then
			begin
			errs := errs + 1;
			writeln('Char assertion #', asserts, ' failed.');
			writeln('Expected ', e, ' but got ',c);
			end;
	end;
procedure asserti( n : integer; e : integer);
	begin
		asserts := asserts + 1;
		if n <> e then
			begin
			errs := errs + 1;
			writeln('Integer assertion #', asserts, ' failed.');
			writeln('Expected ', e, ' but got ',n);
			end;
	end;
procedure assertr( a : real; e : real; tol : real);
	begin
		asserts := asserts + 1;
		if (a > e) and (a - tol > e) or (e > a) and (e - tol > a) then
			begin
			errs := errs + 1;
			writeln('Real assertion #', asserts, ' failed.');
			writeln('Expected ', e, ' but got ', a);
			end;
	end;
procedure assertre( a: real; e : real);
	begin
		assertr(a,e,eps);
	end;
procedure assertst( c : string5; e : string5);
	begin
		asserts := asserts + 1;
		if c <> e then
			begin
			errs := errs + 1;
			writeln('String assertion #', asserts, ' failed.');
			writeln('Expected ', e, ' but got ',c);
			end;
	end;
procedure strref(var s : string5);
	begin
		assertb(s = z,true);
		assertst(s,z);
		s[1] := 'y';
	end;
procedure assertdone();
	begin
		if errs > 0 then
			writeln(errs,' errors encountered.')
		else
			writeln('Program executed succesfully.');
	end;

begin
asserts := 0;
errs := 0;
n := 3;
k := 6;
x := 314.2315421235;
y := 4.67;
a := n;
q := 4;

assertr(a,3,0);
assertb(odd(n),true);
assertb(odd(2),false);

asserti(ord(small),1);

asserti(round(x),314);
asserti(round(y),5);

asserti(trunc(x),314);
asserti(trunc(y),4);


assertr(exp(q),54.598,eps);
assertr(exp(y),106.697,eps);
assertr(ln(q),1.386,eps);
assertr(ln(y),1.541,eps);
assertr(sin(q),-0.757,eps);
assertr(sin(y),-1,eps);
assertr(sqrt(q),2,eps);
assertr(sqrt(y),2.161,eps);



asserti(abs(q),4);
asserti(abs(-q),4);
assertr(abs(y),4.67,eps);
asserti(sqr(q),16);
assertr(sqr(y),21.808,eps);

asserti(n mod k,3);
asserti(k div n,2);



{ 24 }	assertr(0.0 + 1.1,1.1,eps);
	assertr(1 + 1.1,2.1,eps);
	assertr(1.1 + 1,2.1,eps);
	asserti(1 + 2,3);

//	1 real var
	assertr(0.0 + y,4.67,eps);
	assertr(1 + y,5.67,eps);
{ 30 }	assertre(y + 0.0,4.67);
	assertre(y + 1,5.67);

//	1 int var
	assertre(0.0 + n,3);
	assertre(1 + n,4);
	assertre(n + 0.0,3);
	assertre(n + 1,4);

//	2 var
	assertre(x + y,318.9015);
	assertre(y + n,7.67);
	assertre(n + y,7.67);
	asserti(n + k,9);

	assertre(0.0 / 1.1,0);
	assertre(1 / 1.1,0.90909);
	assertre(1.1 / 1,1.1);
	assertre(1 / 2,0.5);

//	1 real var
	assertre(0.0 / y,0);
	assertre(1 / y,0.214);
	assertre(y / 1.0,4.67);
	assertre(y / 2,2.335);

//	1 int var
	assertre(0.0 / n,0);
	assertre(1 / n,0.333);
	assertre(n / 2.0,1.5);
	assertre(n / 2,1.5);

//	2 var
	assertre(x / y,67.287);
	assertre(y / n,1.556);
	assertre(n / y,0.642);

	assertre(n / k,0.5);

	assertre(0.0 * 1.1,0);
	assertre(2 * 1.1,2.2);
	assertre(1.1 * 3,3.3);
	asserti(3 * 2,6);

//	1 real var
	assertre(0.0 * y,0);
	assertre(2 * y,9.34);
	assertre(y * 3.0,14.01);
	assertre(y * 2,9.34);

//	1 int var
	assertre(3.1 * n,9.3);
	asserti(2 * n,6);
	assertre(n * 2.1,6.3);
	asserti(n * 3,9);

//	2 var
	assertr(x * y,1467.4,0.1);
	assertre(y * n,14.01);
	assertre(n * y,14.01);
	asserti(n * k,18);

	assertre(0.0 - 1.1,-1.1);
	assertre(1 - 1.1,-0.1);
	assertre(1.1 - 1,0.1);
	asserti(1 - 2,-1);

//	1 real var
	assertre(0.0 - y,-4.67);
	assertre(1 - y,-3.67);
	assertre(y - 2.0,2.67);
	assertre(y - 1,3.67);

//	1 int var
	assertre(1.0 - n,-2);
	asserti(2 - n,-1);
	assertre(n - 4.0,-1);
	asserti(n - 1,2);

assertb(1.1 > 2.2,false);
assertb(2.2 > 1.1, true);
assertb(1.1 < 2.2,true);
assertb(2.2 < 1.1,false);
assertb(1.1 = 2.2,false);
assertb(1.1 <> 2.2,true);
assertb(1.1 = 1.1,true);

assertb(1 > 2,false);
assertb(2 > 1, true);
assertb(1 < 2,true);
assertb(2 < 1,false);
assertb(1 = 2,false);
assertb(1 <> 2,true);
assertb(1 = 1,true);

assertb(1 > 2.2,false);
assertb(2 > 1.1, true);
assertb(1 < 2.2,true);
assertb(2 < 1.1,false);
assertb(1 = 2.2,false);
assertb(1 <> 2.2,true);
assertb(1 = 1.0,true);

assertb(1.1 > 2,false);
assertb(2.2 > 1, true);
assertb(1.1 < 2,true);
assertb(2.2 < 1,false);
assertb(1.1 = 2,false);
assertb(1.1 <> 2,true);
assertb(1.0 = 1,true);

x := 1.0;

assertb(x > 2.2,false);
assertb(2.2 > x, true);
assertb(x < 2.2,true);
assertb(2.2 < x,false);
assertb(x = 2.2,false);
assertb(x <> 2.2,true);
assertb(x = x,true);

y := 2.2;

assertb(x > y,false);
assertb(y > x, true);
assertb(x < y,true);
assertb(y < x,false);
assertb(x = y,false);
assertb(x <> y,true);
assertb(x = x,true);

assertb(1 > y,false);
assertb(2 > x, true);
assertb(1 < y,true);
assertb(2 < x,false);
assertb(1 = y,false);
assertb(1 <> y,true);
assertb(1 = x,true);

assertb(x > 2,false);
assertb(y > 1, true);
assertb(x < 2,true);
assertb(y < 1,false);
assertb(x = 2,false);
assertb(x <> 2,true);
assertb(x = 1,true);

n := 1;

assertb(n > 2,false);
assertb(2 > n, true);
assertb(n < 2,true);
assertb(2 < n,false);
assertb(n = 2,false);
assertb(n <> 2,true);
assertb(n = 1,true);

k := 2;
assertb(n > y,false);
assertb(k > x, true);
assertb(n < y,true);
assertb(k < x,false);
assertb(n = y,false);
assertb(n <> y,true);
assertb(n = x,true);

assertb(x > k,false);
assertb(y > n, true);
assertb(x < k,true);
assertb(y < n,false);
assertb(x = k,false);
assertb(x <> k,true);
assertb(x = n,true);

///////////////////////////////////////////////////////////////////////////
// STRING COMPARISONS AND SUBSCRIPTING FOLLOW /////////////////////////////
///////////////////////////////////////////////////////////////////////////

cs := 'j';
assertb(cs = 'j',true);
assertb(cs = 'n',false);
assertb(cs <> 'n',true);
assertb(cs <> 'j',false);
assertb(cs > 'k',false);
assertb(cs < 'k',true);
assertb('k' > cs,true);
assertb('k' < cs,false);

ds := 'k';

assertb(cs = ds,false);
assertb(cs <> ds,true);
assertb(cs = cs,true);
assertb(cs <> cs,false);
assertb(cs > ds,false);
assertb(cs < ds,true);
assertb(ds > cs,true);
assertb(ds < cs,false);

assertc(cs,'j');

xs := z;
ys := 'tyuio';
assertb(xs = z,true);
assertb(xs = ys,false);
assertb(xs <> z,false);
assertb(xs <> ys,true);

assertst(ys,'tyuio');
ys[1] := 'r';
assertst(ys,'ryuio');
ys[2] := cs;
assertst(ys,'rjuio');

assertst(xs,'qwert');
assertc(ys[1],'r');
assertc(ys[5],'o');

assertc(z[1],'q');
assertc(z[5],'t');

assertb(xs < ys,true);
assertb(xs > ys,false);
strref(xs);
assertst(xs,'ywert');

xs := z;
assertb(xs = z,true);
assertb(xs <> z,false);
assertb('aaaaa' = 'aaaab',false);
assertb('aaaaa' <> 'aaaab',true);
assertb(z = i,false);
assertb(z <> i, true);

assertdone();
end.
